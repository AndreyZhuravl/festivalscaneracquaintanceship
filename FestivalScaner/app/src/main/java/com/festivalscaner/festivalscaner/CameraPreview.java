package com.festivalscaner.festivalscaner;
import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;

import com.festivalscaner.festivalscaner.radarmodel.Navigator;

import java.io.IOException;
import java.util.List;
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback{

    SurfaceHolder mHolder;
    Camera mCamera;

    public CameraPreview(Context context) {
        super(context);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        mCamera = getCameraInstance();
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException exception) {
            mCamera.release();
            mCamera = null;
            // TODO: add more exception handling logic here
        }
    }

    public static Camera getCameraInstance() {
        int cameraId = 0;
        Camera c = null;
        try {
            c = Camera.open(cameraId);
            Camera.Parameters parameters = c.getParameters();
            Navigator.CAMERA_ANGLE_OF_VISION = (int) parameters.getVerticalViewAngle();
            c.setParameters(parameters);
        } catch (Exception e) {
        }
        return c; // returns null if camera is unavailable
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        // Because the CameraDevice object is not a shared resource, it's very
        // important to release it when the activity is paused.
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }


    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        Camera.Parameters parameters = mCamera.getParameters();

        List<Size> sizes = parameters.getSupportedPreviewSizes();
        Size optimalSize = getOptimalPreviewSize(sizes, w, h);
        parameters.setPreviewSize(optimalSize.width, optimalSize.height);

        mCamera.setParameters(parameters);
        mCamera.startPreview();
    }


//	private SurfaceHolder mHolder;
//	private Camera mCamera;
//	protected final Paint rectanglePaint = new Paint();
//	private byte[] mCallBackBuffer;
//	private SurfaceHolder mHolder2;
//	private int mWidthP;
//	private int mHeightP;
//	private int mWidth;
//	private int mHeight;
//	int[] previewPixels;
//
//	public CameraPreview(Context context, Camera camera) {
//		super(context);
//
//		mCamera = camera;
//
//		// Install a SurfaceHolder.Callback so we get notified when the
//		// underlying surface is created and destroyed.
//		mHolder = getHolder();
//		mWidthP = mCamera.getParameters().getPictureSize().width;
//		mHeightP = mCamera.getParameters().getPictureSize().height;
//		previewPixels = new int[mWidthP * mHeightP];
//		// deprecated setting, but required on Android versions prior to 3.0
//		// mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//		// mHolder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
//		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//		mHolder.addCallback(this);
//		rectanglePaint.setARGB(0, 0, 0, 0);
//		rectanglePaint.setStyle(Paint.Style.FILL);
//		rectanglePaint.setStrokeWidth(9);
//
//	}
//
//	@Override
//	public void surfaceCreated(SurfaceHolder holder) {
//		try {
//			mCamera.setPreviewDisplay(holder);
//			mCamera.startPreview();
//		} catch (IOException e) {
//		}
//	}
//
//	@Override
//	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		int desiredWidth = -2;
//		int desiredHeight = -2;
//
//		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
//		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
//
//		// Measure Width
//		if (widthMode == MeasureSpec.EXACTLY) {
//			// Must be this size
//			mWidth = widthSize;
//		} else if (widthMode == MeasureSpec.AT_MOST) {
//			// Can't be bigger than...
//			mWidth = Math.min(desiredWidth, widthSize);
//		} else {
//			// Be whatever you want
//			mWidth = desiredWidth;
//		}
//
//		// Measure Height
//		if (heightMode == MeasureSpec.EXACTLY) {
//			// Must be this size
//			mHeight = heightSize;
//		} else if (heightMode == MeasureSpec.AT_MOST) {
//			// Can't be bigger than...
//			mHeight = Math.min(desiredHeight, heightSize);
//		} else {
//			// Be whatever you want
//			mHeight = desiredHeight;
//		}
//
//		// MUST CALL THIS
//		setMeasuredDimension(mWidth, mHeight);
//	}
//
//	@Override
//	public void surfaceDestroyed(SurfaceHolder holder) {
//		mCamera.stopPreview();
//		mCamera.release();
//	}
//
//	@Override
//	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
//
//		if (mHolder.getSurface() == null) {
//			return;
//		}
//
//		try {
//			stopPreview();
//		} catch (Exception e) {
//		}
//		try {
//			startPreview();
//		} catch (Exception e) {
//		}
//	}
//
//	public void startPreview() {
//		try {
//			mCamera.setPreviewDisplay(mHolder);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		mCamera.startPreview();
//	}
//
//	public void stopPreview() {
//		mCamera.stopPreview();
//	}
//
//	Paint paint = new Paint();
//
//	@Override
//	public void onPreviewFrame(byte[] data, Camera camera) {
//
//	}

}
