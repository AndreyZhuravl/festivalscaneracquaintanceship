package com.festivalscaner.festivalscaner;

public interface IApplicationScopeManager {

	public static final int STATE_REDY = 1;
	public static final int STATE_NOT_REDY = 0;

	public int getState();

	public void prepareOnWorkerThreadIfNeeded(OnPrepareProgressListener listener);

	public static interface OnPrepareProgressListener {
		public void onProgressUpdate(IApplicationScopeManager sender, int progress, int overal, String message);
	}

	public static class SimpleOnPrepareProgerssListener implements OnPrepareProgressListener {
		@Override
		public void onProgressUpdate(IApplicationScopeManager sender, int progress, int overal, String message) {
		}
	}
}
