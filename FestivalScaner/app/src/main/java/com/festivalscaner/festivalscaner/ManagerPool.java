package com.festivalscaner.festivalscaner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class ManagerPool {

	private static final ManagerPool sInstance = new ManagerPool();
	private final Map<Integer, IApplicationScopeManager> mApplicationManagerMap;

	private ManagerPool() {
		mApplicationManagerMap = new HashMap<Integer, IApplicationScopeManager>();
	}

	public static ManagerPool getInstance() {
		return sInstance;
	}

	public void clear() {
		mApplicationManagerMap.clear();
	}

	public void addManager(int managerId, IApplicationScopeManager manager) {
		mApplicationManagerMap.put(managerId, manager);
	}

	public void addManagerIfNoPresence(int managerId, IApplicationScopeManager manager) {
		if (mApplicationManagerMap.get(managerId) == null) {
			mApplicationManagerMap.put(managerId, manager);
		}
	}
	
	public <T extends IApplicationScopeManager> T getManager(int id){
		return (T) mApplicationManagerMap.get(id);
	}

	public Set<Integer> getManagerKeys() {
		return mApplicationManagerMap.keySet();
	}
}
