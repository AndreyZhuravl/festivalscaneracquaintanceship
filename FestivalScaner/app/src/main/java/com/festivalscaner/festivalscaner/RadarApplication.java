package com.festivalscaner.festivalscaner;

import android.app.Application;
import android.util.Log;

import com.festivalscaner.festivalscaner.network.INetworkManager;
import com.festivalscaner.festivalscaner.network.NetworkManagerDefault;


public class RadarApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        ManagerPool managerPool = ManagerPool.getInstance();
        managerPool.addManager(INetworkManager.ID,new NetworkManagerDefault());
    }


}
