package com.festivalscaner.festivalscaner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

import com.festivalscaner.festivalscaner.radarmodel.IRadarModel;
import com.festivalscaner.festivalscaner.radarmodel.RadarModel;
import com.festivalscaner.festivalscaner.viewcontroller.RadarView;

/**
 * Created by andrey on 20.06.2015.
 */
public class RadarSurfaceView extends SurfaceView implements OnTouchListener, SurfaceHolder.Callback {
    IRadarModel radarModel;
    Context mContext;
    private int mWidth;
    private int mHeight;

    private DrawThread thread;

    public RadarSurfaceView(Context context, IRadarModel radarModel) {
        super(context);
        this.radarModel = radarModel;
        this.radarModel.setSurfaceView(this);
        mContext = context;
        setOnTouchListener(this);
        thread = new DrawThread(this);
        getHolder().addCallback(this);
        setZOrderOnTop(true);
        getHolder().setFormat(PixelFormat.RGBA_8888);
        setFocusable(true);

    }

    public void registerModelSensorListener() {
        radarModel.registerSensorListeners();
    }

    public void unregisterModelSensorListeners() {
        radarModel.unregisterSensorListeners();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = -2;
        int desiredHeight = -2;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        // Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            // Must be this size
            mWidth = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            // Can't be bigger than...
            mWidth = Math.min(desiredWidth, widthSize);
        } else {
            // Be whatever you want
            mWidth = desiredWidth;
        }

        // Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            // Must be this size
            mHeight = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            // Can't be bigger than...
            mHeight = Math.min(desiredHeight, heightSize);
        } else {
            // Be whatever you want
            mHeight = desiredHeight;
        }
        radarModel.setWidthHeight(mWidth, mHeight);
        // MUST CALL THIS
        setMeasuredDimension(mWidth, mHeight);
    }


    public void draw(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY);
        radarModel.updateRadar();
        radarModel.onDraw(canvas);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return radarModel.onTouch(event);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        thread.setRunning(false);
        while (retry){
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    class DrawThread extends Thread{
        private boolean running = false;
        private SurfaceHolder surfaceHolder;
        private RadarSurfaceView radarView;

        public DrawThread(RadarSurfaceView radarView) {
            this.radarView = radarView;
            this.surfaceHolder = radarView.getHolder();
        }

        public void setRunning(boolean running) {
            this.running = running;
        }


        @Override
        public void run() {
            //super.run();
            Canvas canvas = null;
            while (running){
                //Canvas canvas = null;
                try{
                    canvas = surfaceHolder.lockCanvas();

                    synchronized (surfaceHolder){
                        radarView.draw(canvas);
                    }
                }finally {
                    if(canvas !=null){
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }

                try {
                    Thread.sleep(17);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

