package com.festivalscaner.festivalscaner.fsm;

import android.graphics.Canvas;
import android.location.Location;
import android.view.MotionEvent;

import com.festivalscaner.festivalscaner.network.CommandErrorInfo;
import com.festivalscaner.festivalscaner.network.Nearby;
import com.festivalscaner.festivalscaner.radarmodel.IRadarModel;
import com.festivalscaner.festivalscaner.radarmodel.UserData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrey on 02.06.2015.
 */
public class BusinessObject {
    IRadarModel model;
public BusinessObject(IRadarModel model){
    this.model = model;
}
    public Object getObject(){
        return model;
    }
}
