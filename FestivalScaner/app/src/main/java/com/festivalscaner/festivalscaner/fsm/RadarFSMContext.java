
package com.festivalscaner.festivalscaner.fsm;

import com.festivalscaner.festivalscaner.radarmodel.IRadarModel;
import com.stateforge.statemachine.context.AbstractContext;

public class RadarFSMContext
    extends AbstractContext<RadarFSMNewStateMachineState, RadarFSMContext>
{
    private BusinessObject _myBusinessObject;

    /**
     * Context constructor
     */
    public RadarFSMContext(BusinessObject myBusinessObject) {
        super();
        _myBusinessObject = myBusinessObject;
        setName("RadarFSMContext");
        setInitialState(RadarFSMSSobelEffectState.getInstance());
    }

    public BusinessObject getBusinessObject() {
        return _myBusinessObject;
    }

    /**
     * Enter the initial state
     */
    public void enterInitialState() {
        com.stateforge.statemachine.algorithm.StateOperation.walkTreeEntry(this, RadarFSMNewStateMachineState.getInstance(), RadarFSMSSobelEffectState.getInstance());
    }

    /**
     * Leave the current state
     */
    public void leaveCurrentState() {
        com.stateforge.statemachine.algorithm.StateOperation.walkTreeExit(this, this.getStateCurrent(), RadarFSMNewStateMachineState.getInstance());
    }

    /**
     * Event TrESWaitAnswer
     */
    public void TrESWaitAnswer() {
        getStateCurrent().TrESWaitAnswer(this);
    }

    /**
     * Event TrESAnimationSSobel
     */
    public void TrESAnimationSSobel() {
        getStateCurrent().TrESAnimationSSobel(this);
    }

    /**
     * Event TrECloseUserBarVisible
     */
    public void TrECloseUserBarVisible() {
        getStateCurrent().TrECloseUserBarVisible(this);
    }

    /**
     * Event TrESMissingYourInvite
     */
    public void TrESMissingYourInvite() {
        getStateCurrent().TrESMissingYourInvite(this);
    }

    /**
     * Event TrEDetailedUser
     */
    public void TrEDetailedUser() {
        getStateCurrent().TrEDetailedUser(this);
    }

    /**
     * Event TrEToForbidTwoMeeting
     */
    public void TrEToForbidTwoMeeting() {
        getStateCurrent().TrEToForbidTwoMeeting(this);
    }

    /**
     * Event TrERejectInvitation
     */
    public void TrERejectInvitation() {
        getStateCurrent().TrERejectInvitation(this);
    }

    /**
     * Event TrEToWeMeet
     */
    public void TrEToWeMeet() {
        getStateCurrent().TrEToWeMeet(this);
    }

    /**
     * Event TrESheAgreedMeet
     */
    public void TrESheAgreedMeet() {
        getStateCurrent().TrESheAgreedMeet(this);
    }

    /**
     * Event TrEYouAreInvited
     */
    public void TrEYouAreInvited() {
        getStateCurrent().TrEYouAreInvited(this);
    }

    /**
     * Event TrESEmptyRadar
     */
    public void TrESEmptyRadar() {
        getStateCurrent().TrESEmptyRadar(this);
        ((IRadarModel) getBusinessObject().getObject()).nextStackAction();
    }

    /**
     * Event TrEUpdateUsersShowListFromTopUser
     */
    public void TrEUpdateUsersShowListFromTopUser() {
        getStateCurrent().TrEUpdateUsersShowListFromTopUser(this);
    }

    /**
     * Event TrEUpdateUsersShowList
     */
    public void TrEUpdateUsersShowList() {
        getStateCurrent().TrEUpdateUsersShowList(this);
    }

    /**
     * Event TrEToYouAgreedMeet
     */
    public void TrEToYouAgreedMeet() {
        getStateCurrent().TrEToYouAgreedMeet(this);
    }

    /**
     * Event TrE1500MillisecEnd
     */
    public void TrE1500MillisecEnd() {
        getStateCurrent().TrE1500MillisecEnd(this);
    }

    /**
     * Event TrESPause
     */
    public void TrESPause() {
        getStateCurrent().TrESPause(this);
    }

    /**
     * Event TrESheRejectMeeting
     */
    public void TrESheRejectMeeting() {
        getStateCurrent().TrESheRejectMeeting(this);
    }

    /**
     * Event TrESWaitSJustPic
     */
    public void TrESWaitSJustPic() {
        getStateCurrent().TrESWaitSJustPic(this);
    }

    /**
     * Event TrESProfile
     */
    public void TrESProfile() {
        getStateCurrent().TrESProfile(this);
    }

    /**
     * Event TrESRadarSAnimation
     */
    public void TrESRadarSAnimation() {
        getStateCurrent().TrESRadarSAnimation(this);
    }

    /**
     * Event TrESActiveSRadar
     */
    public void TrESActiveSRadar() {
        getStateCurrent().TrESActiveSRadar(this);
    }

    /**
     * Event EStart
     */
    public void EStart() {
        getStateCurrent().EStart(this);
    }

    /**
     * Event EExit
     */
    public void EExit() {
        getStateCurrent().EExit(this);
    }

    /**
     * Event TrELetMeet
     */
    public void TrELetMeet() {
        getStateCurrent().TrELetMeet(this);
    }

    /**
     * Event TrEToRejected
     */
    public void TrEToRejected() {
        getStateCurrent().TrEToRejected(this);
    }

    /**
     * Event TrESWaitSAgreed
     */
    public void TrESWaitSAgreed() {
        getStateCurrent().TrESWaitSAgreed(this);
    }

    /**
     * Event TrEBackToRadar
     */
    public void TrEBackToRadar() {
        getStateCurrent().TrEBackToRadar(this);
    }

    /**
     * Event TrEEndSobel
     */
    public void TrEEndSobel() {
        getStateCurrent().TrEEndSobel(this);
    }
}
