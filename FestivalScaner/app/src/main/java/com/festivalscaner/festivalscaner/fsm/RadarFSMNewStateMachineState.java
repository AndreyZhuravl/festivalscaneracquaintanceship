
package com.festivalscaner.festivalscaner.fsm;

import com.stateforge.statemachine.state.AbstractState;

public class RadarFSMNewStateMachineState
    extends AbstractState<RadarFSMContext, RadarFSMNewStateMachineState>
{
    private final static RadarFSMNewStateMachineState instance = new RadarFSMNewStateMachineState();

    /**
     * Protected Constructor
     */
    protected RadarFSMNewStateMachineState() {
        setName("NewStateMachine");
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESWaitAnswer
     */
    public void TrESWaitAnswer(RadarFSMContext context) {
    }

    /**
     * Event id: TrESAnimationSSobel
     */
    public void TrESAnimationSSobel(RadarFSMContext context) {
    }

    /**
     * Event id: TrECloseUserBarVisible
     */
    public void TrECloseUserBarVisible(RadarFSMContext context) {
    }

    /**
     * Event id: TrESMissingYourInvite
     */
    public void TrESMissingYourInvite(RadarFSMContext context) {
    }

    /**
     * Event id: TrEDetailedUser
     */
    public void TrEDetailedUser(RadarFSMContext context) {
    }

    /**
     * Event id: TrEToForbidTwoMeeting
     */
    public void TrEToForbidTwoMeeting(RadarFSMContext context) {
    }

    /**
     * Event id: TrERejectInvitation
     */
    public void TrERejectInvitation(RadarFSMContext context) {
    }

    /**
     * Event id: TrEToWeMeet
     */
    public void TrEToWeMeet(RadarFSMContext context) {
    }

    /**
     * Event id: TrESheAgreedMeet
     */
    public void TrESheAgreedMeet(RadarFSMContext context) {
    }

    /**
     * Event id: TrEYouAreInvited
     */
    public void TrEYouAreInvited(RadarFSMContext context) {
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
    }

    /**
     * Event id: TrEUpdateUsersShowListFromTopUser
     */
    public void TrEUpdateUsersShowListFromTopUser(RadarFSMContext context) {
    }

    /**
     * Event id: TrEUpdateUsersShowList
     */
    public void TrEUpdateUsersShowList(RadarFSMContext context) {
    }

    /**
     * Event id: TrEToYouAgreedMeet
     */
    public void TrEToYouAgreedMeet(RadarFSMContext context) {
    }

    /**
     * Event id: TrE1500MillisecEnd
     */
    public void TrE1500MillisecEnd(RadarFSMContext context) {
    }

    /**
     * Event id: TrESPause
     */
    public void TrESPause(RadarFSMContext context) {
    }

    /**
     * Event id: TrESheRejectMeeting
     */
    public void TrESheRejectMeeting(RadarFSMContext context) {
    }

    /**
     * Event id: TrESWaitSJustPic
     */
    public void TrESWaitSJustPic(RadarFSMContext context) {
    }

    /**
     * Event id: TrESProfile
     */
    public void TrESProfile(RadarFSMContext context) {
    }

    /**
     * Event id: TrESRadarSAnimation
     */
    public void TrESRadarSAnimation(RadarFSMContext context) {
    }

    /**
     * Event id: TrESActiveSRadar
     */
    public void TrESActiveSRadar(RadarFSMContext context) {
    }

    /**
     * Event id: EStart
     */
    public void EStart(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from NewStateMachine to SActive triggered by EStart
        // The next state is within the context RadarFSMContext
        context.setTransitionName("EStart");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }

    /**
     * Event id: EExit
     */
    public void EExit(RadarFSMContext context) {
    }

    /**
     * Event id: TrELetMeet
     */
    public void TrELetMeet(RadarFSMContext context) {
    }

    /**
     * Event id: TrEToRejected
     */
    public void TrEToRejected(RadarFSMContext context) {
    }

    /**
     * Event id: TrESWaitSAgreed
     */
    public void TrESWaitSAgreed(RadarFSMContext context) {
    }

    /**
     * Event id: TrEBackToRadar
     */
    public void TrEBackToRadar(RadarFSMContext context) {
    }

    /**
     * Event id: TrEEndSobel
     */
    public void TrEEndSobel(RadarFSMContext context) {
    }
}
