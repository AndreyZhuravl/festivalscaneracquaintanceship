
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSActiveState
    extends RadarFSMNewStateMachineState
{
    private final static RadarFSMSActiveState instance = new RadarFSMSActiveState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSActiveState() {
        setName("SActive");
        setStateParent(RadarFSMNewStateMachineState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESActiveSRadar
     */
    public void TrESActiveSRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SActive to SRadar triggered by TrESActiveSRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESActiveSRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }

    /**
     * Event id: EExit
     */
    public void EExit(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SActive to SPauseState triggered by EExit
        // The next state is within the context RadarFSMContext
        context.setTransitionName("EExit");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSPauseStateState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSPauseStateState.getInstance());
        return ;
    }
}
