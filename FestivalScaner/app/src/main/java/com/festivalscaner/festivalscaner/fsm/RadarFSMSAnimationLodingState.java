
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSAnimationLodingState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSAnimationLodingState instance = new RadarFSMSAnimationLodingState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSAnimationLodingState() {
        setName("SAnimationLoding");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESAnimationSSobel
     */
    public void TrESAnimationSSobel(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SAnimationLoding to SSobelEffect triggered by TrESAnimationSSobel
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESAnimationSSobel");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }
}
