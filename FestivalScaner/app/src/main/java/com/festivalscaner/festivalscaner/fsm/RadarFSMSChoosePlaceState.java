
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSChoosePlaceState
    extends RadarFSMSActiveState
{
    private final static RadarFSMSChoosePlaceState instance = new RadarFSMSChoosePlaceState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSChoosePlaceState() {
        setName("SChoosePlace");
        setStateParent(RadarFSMSActiveState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrEBackToRadar
     */
    public void TrEBackToRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SChoosePlace to SRadar triggered by TrEBackToRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEBackToRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }

    /**
     * Event id: TrESWaitAnswer
     */
    public void TrESWaitAnswer(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SChoosePlace to SWaitAnswer triggered by TrESWaitAnswer
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESWaitAnswer");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSWaitAnswerState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSWaitAnswerState.getInstance());
        return ;
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SChoosePlace to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }
}
