
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSEmptyRadarState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSEmptyRadarState instance = new RadarFSMSEmptyRadarState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSEmptyRadarState() {
        setName("SEmptyRadar");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrEDetailedUser
     */
    public void TrEDetailedUser(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SLetMeet triggered by TrEDetailedUser
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEDetailedUser");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSLetMeetState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSLetMeetState.getInstance());
        return ;
    }

    /**
     * Event id: TrEUpdateUsersShowList
     */
    public void TrEUpdateUsersShowList(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SAnimationLoding triggered by TrEUpdateUsersShowList
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEUpdateUsersShowList");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }

    /**
     * Event id: TrESheAgreedMeet
     */
    public void TrESheAgreedMeet(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SSheAgreedMeet triggered by TrESheAgreedMeet
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESheAgreedMeet");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSheAgreedMeetState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSheAgreedMeetState.getInstance());
        return ;
    }

    /**
     * Event id: TrEYouAreInvited
     */
    public void TrEYouAreInvited(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SYouAreInvited triggered by TrEYouAreInvited
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEYouAreInvited");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSYouAreInvitedState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSYouAreInvitedState.getInstance());
        return ;
    }

    /**
     * Event id: TrESWaitAnswer
     */
    public void TrESWaitAnswer(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SWaitAnswer triggered by TrESWaitAnswer
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESWaitAnswer");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSWaitAnswerState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSWaitAnswerState.getInstance());
        return ;
    }

    /**
     * Event id: TrESheRejectMeeting
     */
    public void TrESheRejectMeeting(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SSheRejectMeeting triggered by TrESheRejectMeeting
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESheRejectMeeting");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSheRejectMeetingState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSheRejectMeetingState.getInstance());
        return ;
    }

    /**
     * Event id: TrEToForbidTwoMeeting
     */
    public void TrEToForbidTwoMeeting(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SForbidTwoMeeting triggered by TrEToForbidTwoMeeting
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEToForbidTwoMeeting");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSForbidTwoMeetingState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSForbidTwoMeetingState.getInstance());
        return ;
    }

    /**
     * Event id: TrEToRejected
     */
    public void TrEToRejected(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SYouRejectMeeting triggered by TrEToRejected
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEToRejected");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSYouRejectMeetingState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSYouRejectMeetingState.getInstance());
        return ;
    }

    /**
     * Event id: TrEToWeMeet
     */
    public void TrEToWeMeet(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SWeMeet triggered by TrEToWeMeet
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEToWeMeet");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSWeMeetState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSWeMeetState.getInstance());
        return ;
    }

    /**
     * Event id: TrERejectInvitation
     */
    public void TrERejectInvitation(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SYouRejectInvitation triggered by TrERejectInvitation
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrERejectInvitation");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSYouRejectInvitationState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSYouRejectInvitationState.getInstance());
        return ;
    }

    /**
     * Event id: TrESMissingYourInvite
     */
    public void TrESMissingYourInvite(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SEmptyRadar to SMissingYourInvite triggered by TrESMissingYourInvite
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESMissingYourInvite");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSMissingYourInviteState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSMissingYourInviteState.getInstance());
        return ;
    }
}
