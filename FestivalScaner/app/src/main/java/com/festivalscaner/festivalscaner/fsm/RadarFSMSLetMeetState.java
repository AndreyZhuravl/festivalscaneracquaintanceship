
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSLetMeetState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSLetMeetState instance = new RadarFSMSLetMeetState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSLetMeetState() {
        setName("SLetMeet");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SLetMeet to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }

    /**
     * Event id: TrEUpdateUsersShowList
     */
    public void TrEUpdateUsersShowList(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SLetMeet to SAnimationLoding triggered by TrEUpdateUsersShowList
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEUpdateUsersShowList");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }

    /**
     * Event id: TrELetMeet
     */
    public void TrELetMeet(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SLetMeet to SChoosePlace triggered by TrELetMeet
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrELetMeet");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSChoosePlaceState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSChoosePlaceState.getInstance());
        return ;
    }

    /**
     * Event id: TrEToForbidTwoMeeting
     */
    public void TrEToForbidTwoMeeting(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SLetMeet to SForbidTwoMeeting triggered by TrEToForbidTwoMeeting
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEToForbidTwoMeeting");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSForbidTwoMeetingState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSForbidTwoMeetingState.getInstance());
        return ;
    }

    /**
     * Event id: TrESProfile
     */
    public void TrESProfile(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SLetMeet to SProfile triggered by TrESProfile
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESProfile");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSProfileState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSProfileState.getInstance());
        return ;
    }
}
