
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSMissingYourInviteState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSMissingYourInviteState instance = new RadarFSMSMissingYourInviteState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSMissingYourInviteState() {
        setName("SMissingYourInvite");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SMissingYourInvite to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }
}
