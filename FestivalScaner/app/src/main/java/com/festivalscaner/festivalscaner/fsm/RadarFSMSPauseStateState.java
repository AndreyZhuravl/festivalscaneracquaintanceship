
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSPauseStateState
    extends RadarFSMNewStateMachineState
{
    private final static RadarFSMSPauseStateState instance = new RadarFSMSPauseStateState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSPauseStateState() {
        setName("SPauseState");
        setStateParent(RadarFSMNewStateMachineState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESActiveSRadar
     */
    public void TrESActiveSRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SPauseState to SActive triggered by TrESActiveSRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESActiveSRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }
}
