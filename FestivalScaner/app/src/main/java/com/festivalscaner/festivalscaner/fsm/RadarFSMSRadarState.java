
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSRadarState
    extends RadarFSMSActiveState
{
    private final static RadarFSMSRadarState instance = new RadarFSMSRadarState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSRadarState() {
        setName("SRadar");
        setStateParent(RadarFSMSActiveState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESRadarSAnimation
     */
    public void TrESRadarSAnimation(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SRadar to SAnimationLoding triggered by TrESRadarSAnimation
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESRadarSAnimation");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSobelEffectState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSobelEffectState.getInstance());
        return ;
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SRadar to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }
}
