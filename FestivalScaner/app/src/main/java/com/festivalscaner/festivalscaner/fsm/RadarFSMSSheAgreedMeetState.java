
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSSheAgreedMeetState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSSheAgreedMeetState instance = new RadarFSMSSheAgreedMeetState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSSheAgreedMeetState() {
        setName("SSheAgreedMeet");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSheAgreedMeet to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }

    /**
     * Event id: TrEToWeMeet
     */
    public void TrEToWeMeet(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSheAgreedMeet to SWeMeet triggered by TrEToWeMeet
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEToWeMeet");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSWeMeetState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSWeMeetState.getInstance());
        return ;
    }

    /**
     * Event id: TrEToRejected
     */
    public void TrEToRejected(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSheAgreedMeet to SYouRejectMeeting triggered by TrEToRejected
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEToRejected");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSYouRejectMeetingState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSYouRejectMeetingState.getInstance());
        return ;
    }

    /**
     * Event id: TrESProfile
     */
    public void TrESProfile(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSheAgreedMeet to SProfile triggered by TrESProfile
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESProfile");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSProfileState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSProfileState.getInstance());
        return ;
    }
}
