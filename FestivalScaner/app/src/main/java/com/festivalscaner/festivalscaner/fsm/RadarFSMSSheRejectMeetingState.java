
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSSheRejectMeetingState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSSheRejectMeetingState instance = new RadarFSMSSheRejectMeetingState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSSheRejectMeetingState() {
        setName("SSheRejectMeeting");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSheRejectMeeting to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }
}
