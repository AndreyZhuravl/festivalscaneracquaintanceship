
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSSlidingState
    extends RadarFSMSAnimationLodingState
{
    private final static RadarFSMSSlidingState instance = new RadarFSMSSlidingState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSSlidingState() {
        setName("SSliding");
        setStateParent(RadarFSMSAnimationLodingState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrE1500MillisecEnd
     */
    public void TrE1500MillisecEnd(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSliding to SEmptyRadar triggered by TrE1500MillisecEnd
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrE1500MillisecEnd");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }
}
