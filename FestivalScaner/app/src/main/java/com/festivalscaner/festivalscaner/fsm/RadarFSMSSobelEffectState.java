
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSSobelEffectState
    extends RadarFSMSAnimationLodingState
{
    private final static RadarFSMSSobelEffectState instance = new RadarFSMSSobelEffectState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSSobelEffectState() {
        setName("SSobelEffect");
        setStateParent(RadarFSMSAnimationLodingState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrEEndSobel
     */
    public void TrEEndSobel(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SSobelEffect to SSliding triggered by TrEEndSobel
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrEEndSobel");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSSlidingState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSSlidingState.getInstance());
        return ;
    }
}
