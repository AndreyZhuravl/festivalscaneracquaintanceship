
package com.festivalscaner.festivalscaner.fsm;


public class RadarFSMSWaitAnswerState
    extends RadarFSMSRadarState
{
    private final static RadarFSMSWaitAnswerState instance = new RadarFSMSWaitAnswerState();

    /**
     * Protected Constructor
     */
    protected RadarFSMSWaitAnswerState() {
        setName("SWaitAnswer");
        setStateParent(RadarFSMSRadarState.getInstance());
    }

    /**
     * Get the State Instance
     */
    public static RadarFSMNewStateMachineState getInstance() {
        return instance;
    }

    /**
     * onEntry
     */
    @Override
    public void onEntry(RadarFSMContext context) {
        context.getObserver().onEntry(context.getName(), this.getName());
    }

    /**
     * onExit
     */
    @Override
    public void onExit(RadarFSMContext context) {
        context.getObserver().onExit(context.getName(), this.getName());
    }

    /**
     * Event id: TrERejectInvitation
     */
    public void TrERejectInvitation(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SWaitAnswer to SYouRejectInvitation triggered by TrERejectInvitation
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrERejectInvitation");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSYouRejectInvitationState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSYouRejectInvitationState.getInstance());
        return ;
    }

    /**
     * Event id: TrESEmptyRadar
     */
    public void TrESEmptyRadar(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SWaitAnswer to SEmptyRadar triggered by TrESEmptyRadar
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESEmptyRadar");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSEmptyRadarState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSEmptyRadarState.getInstance());
        return ;
    }

    /**
     * Event id: TrESProfile
     */
    public void TrESProfile(RadarFSMContext context) {
        BusinessObject myBusinessObject = context.getBusinessObject();
        // Transition from SWaitAnswer to SProfile triggered by TrESProfile
        // The next state is within the context RadarFSMContext
        context.setTransitionName("TrESProfile");
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionBegin(context, RadarFSMSProfileState.getInstance());
        com.stateforge.statemachine.algorithm.StateOperation.processTransitionEnd(context, RadarFSMSProfileState.getInstance());
        return ;
    }
}
