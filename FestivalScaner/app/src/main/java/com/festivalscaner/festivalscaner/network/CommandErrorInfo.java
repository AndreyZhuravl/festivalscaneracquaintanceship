package com.festivalscaner.festivalscaner.network;

public class CommandErrorInfo {
	public final int errorCode;
	public String errorInfo;
	public Exception errorException;
	
	
	public CommandErrorInfo(int code){
		super();
		this.errorCode = code;
	}
	
	public CommandErrorInfo(int code, String info){
		super();
		this.errorCode = code;
		this.errorInfo = info;
	}
	
	public CommandErrorInfo(int code, String info, Exception exception){
		super();
		this.errorCode = code;
		this.errorInfo = info;
		this.errorException = exception;
	}
	
	public String getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
	public Exception getErrorException() {
		return errorException;
	}
	public void setErrorException(Exception errorException) {
		this.errorException = errorException;
	}
	public int getErrorCode() {
		return errorCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + errorCode;
		result = prime * result + ((errorException == null) ? 0 : errorException.hashCode());
		result = prime * result + ((errorInfo == null) ? 0 : errorInfo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommandErrorInfo other = (CommandErrorInfo) obj;
		if (errorCode != other.errorCode)
			return false;
		if (errorException == null) {
			if (other.errorException != null)
				return false;
		} else if (!errorException.equals(other.errorException))
			return false;
		if (errorInfo == null) {
			if (other.errorInfo != null)
				return false;
		} else if (!errorInfo.equals(other.errorInfo))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CommandErrorInfo [errorCode=" + errorCode + ", errorInfo=" + errorInfo + ", errorException="
				+ errorException + "]";
	}
	

}
