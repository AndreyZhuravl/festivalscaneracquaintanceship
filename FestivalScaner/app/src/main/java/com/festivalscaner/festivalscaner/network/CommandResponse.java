package com.festivalscaner.festivalscaner.network;


public class CommandResponse<T> {

	public T response;
	public boolean cancelled;
	public CommandErrorInfo errorInfo;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((errorInfo == null) ? 0 : errorInfo.hashCode());
		result = prime * result + ((response == null) ? 0 : response.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommandResponse other = (CommandResponse) obj;
		if (cancelled != other.cancelled)
			return false;
		if (errorInfo == null) {
			if (other.errorInfo != null)
				return false;
		} else if (!errorInfo.equals(other.errorInfo))
			return false;
		if (response == null) {
			if (other.response != null)
				return false;
		} else if (!response.equals(other.response))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommandResponse [response=" + response + ", errorInfo=" + errorInfo + "]";
	}

}
