package com.festivalscaner.festivalscaner.network;

import android.graphics.drawable.BitmapDrawable;

import com.festivalscaner.festivalscaner.IApplicationScopeManager;
import com.festivalscaner.festivalscaner.R;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by andrey on 28.05.2015.
 */
public interface INetworkManager extends IApplicationScopeManager {
    public static final int ID = R.id.network_manager_id;

    public void getNearbyUsersAsync(UUID sessionId, int usersInBatch, int batches, Map<String,Float> currentLocation);
    public void setNearbyUsersListener(IOnNearbyUsersListener listener);

    public void onLetMeetAsync(UUID sessionId, UUID userId, UUID meetingPlaceID);
    public void onRejectMeetingAsync(final UUID ssesionId, final UUID sessionId, boolean isMeetingNotInvitation);
    public void onYouAgreedToMeetAsync(final UUID sessionId, final UUID userId, final UUID meetingPlaceID);
    public void onGetPlacesIdAsync(final UUID sessionId);
    public void onChangeAppointmentAsync(final UUID sessionId, final UUID appointmentId, final String status);
    public void onAppointmentsAsync(final UUID sessionId, final UUID userId, final UUID meetingPlaceID);
    public void onPause();


    public static interface IOnNearbyUsersListener {

        public void onNearbyUsersStart();

        public void onNearbyUsersSuccess(List<List<Nearby>> nearbyList);

        public void onNearbyUsersError(CommandErrorInfo errorInfo);

        ////////////////////////////////////////////////////////////////////////////
        public void onLetMeetStart();

        public void onLetMeetSuccess(Map<String, String> apointmentResult);

        public void onLetMeetError(CommandErrorInfo errorInfo);

        ////////////////////////////////////////////////////////////////////////////
        public void onRejectMeetingStart();

        public void onRejectMeetingSuccess(UUID mRejectMeetingRecpose);

        public void onRejectMeetingError(CommandErrorInfo errorInfo);


        ////////////////////////////////////////////////////////////////////////////
        public void onYouAgreedToMeetStart();

        public void onYouAgreedToMeetSuccess();

        public void onYouAgreedToMeetError(CommandErrorInfo errorInfo);

        ////////////////////////////////////////////////////////////////////////////
        public void onGetPlacesIdStart();

        public void onGetPlacesIdSuccess(Map<String, Map<String, String>>  placesIdMap);

        public void onGetPlacesIdError(CommandErrorInfo errorInfo);

        public void onRejectInvitationSuccess(UUID userId);

        ////////////////////////////////////////////////////////////////////////////
        public void onChangeAppointmentStart();

        public void onChangeAppointmentSuccess(Map<String, String> appointment);

        public void onChangeAppointmentError(CommandErrorInfo errorInfo);

        ////////////////////////////////////////////////////////////////////////////
        public void onAppointmentsStart();

        public void onAppointmentsSuccess(List<Map<String, String>> appointment);

        public void onAppointmentsError(CommandErrorInfo errorInfo);
    }
}
