package com.festivalscaner.festivalscaner.network;

import android.os.ParcelUuid;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by andrey on 12.06.2015.
 */
public class JsonParser {
    public static final String PARAM_targets = "targets";
    public static final String PARAM_userId = "userId";
    public static final String PARAM_name = "name";
    public static final String PARAM_birthday = "birthday";
    public static final String PARAM_status = "status";
    public static final String PARAM_appointment = "appointment";
    public static final String PARAM_appointments = "appointments";
    public static final String PARAM_appointmentId = "appointmentId";
    public static final String PARAM_photo = "photo";
    public static final String PARAM_preview = "preview";
    public static final String PARAM_location = "location";
    public static final String PARAM_latitude = "latitude";
    public static final String PARAM_longitude = "longitude";


    public static final String PARAM_places = "places";
    public static final String PARAM_placeId = "placeId";
    public static final String PARAM_map = "map";
    public static final String PARAM_NW = "NW";
    public static final String PARAM_SE = "SE";
    public static final String PARAM_limits = "limits";
    public static final String PARAM_min = "min";
    public static final String PARAM_max = "max";


    public static final String PARAM_made = "made";
    public static final String PARAM_accepted = "accepted";
    public static final String PARAM_declined = "declined";
    public static final String PARAM_timeout = "timeout";
    public static final String PARAM_kept = "kept";
    public static final String PARAM_broken = "broken";
    public static final String PARAM_area = "area";
    public static final String PARAM_NWLATITUDE = "NWlatitude";
    public static final String PARAM_NWLONGITUDE = "NWlongitude";
    public static final String PARAM_SELATITUDE = "SElatitude";
    public static final String PARAM_SELONGITUDE = "SElongitude";
    public static final String MAP_COORD = "mapCoord";
    public static final String PARAM_error = "error";
    public static final String MAP_LIMITS = "limits";


    public static void parseJsonNearby(JSONObject jsonNearby, List<List<Nearby>> lastQueryResultNearbyUserList) throws JSONException {
        JSONArray targets = jsonNearby.getJSONArray(PARAM_targets);
        for (int i = 0; i < targets.length(); i++) {
            lastQueryResultNearbyUserList.add(parseBatches(targets.getJSONArray(i)));
        }
        Log.d("444", "parseJsonNearby = " + targets.toString());
    }

    private static List<Nearby> parseBatches(JSONArray batchesJsonArray) {
        List<Nearby> batch = new ArrayList<Nearby>();
        for (int i = 0; i < batchesJsonArray.length(); i++) {
            try {
                Nearby user = parseUserNearby(batchesJsonArray.getJSONObject(i));
                if (user != null) {
                    batch.add(user);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return batch;
    }

    private static Nearby parseUserNearby(JSONObject userJsonObject) {
        Nearby user = null;
        try {
            String id = userJsonObject.getString(PARAM_userId);
            String name = userJsonObject.getString(PARAM_name);
            String age = getUserAge(userJsonObject);
            String status = userJsonObject.getString(PARAM_status);

            String photo = userJsonObject.getJSONObject(PARAM_photo).getString(PARAM_preview);
            String photoMedium = null;
            String photoLarge = null;
            try {
                photoMedium = userJsonObject.getJSONObject(PARAM_photo).getString("previewMedium");
            } catch (JSONException e) {
            }
            try {
                photoLarge = userJsonObject.getJSONObject(PARAM_photo).getString("previewLarge");
            } catch (JSONException e) {
            }

            String appointment = null;
            try {
                appointment = userJsonObject.getString("appointmentId");
            } catch (JSONException e) {
            }
            float latitude = (float) userJsonObject.getJSONObject(PARAM_location).getDouble(PARAM_latitude);
            float longitude = (float) userJsonObject.getJSONObject(PARAM_location).getDouble(PARAM_longitude);
            user = generateNearby(id, name, age, photo, photoMedium, photoLarge, latitude, longitude, status, appointment);
            Log.d("JSON111", "========================");
            Log.d("JSON111", "name = " + name);
            Log.d("JSON111", "status = " + status);
            Log.d("JSON111", "id = " + id);
        } catch (JSONException e) {
            user = null;
        }
        return user;
    }

    private static String getUserAge(JSONObject userJsonObject) {
        String age = "";
        try {
            String birthday = userJsonObject.getString(PARAM_birthday);
            Date timestamp = new SimpleDateFormat("yyyy-MM-dd'T'h:m:ssZ").parse(birthday);
            GregorianCalendar cal = new GregorianCalendar();
            age = "" + getDateDiffInYears(cal.getTime(), timestamp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return age;
    }

    public static int getDateDiffInYears(Date date1, Date date2) {
        long diffInMillies = date1.getTime() - date2.getTime();
        return (int) (TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) / 365);
    }

    private static Nearby generateNearby(String id, String name, String birthday, String urlPhoto, String urlPhotoMedium, String urlPhotoLarge, float locationLat, float locationLong, String status, String appointmentId) {
        ParcelUuid userId = new ParcelUuid(UUID.fromString(id));
        HashMap<String, String> photo = new HashMap<String, String>();
        photo.put(Nearby.PHOTO_PREVIEW_KEY, urlPhoto);
        if (urlPhotoMedium != null) {
            photo.put(Nearby.PHOTO_PREVIEW_MEDIUM_KEY, urlPhotoMedium);
        }
        if (urlPhotoLarge != null) {
            photo.put(Nearby.PHOTO_PREVIEW_LARGE_KEY, urlPhotoLarge);
        }
        HashMap<String, Float> location = new HashMap<String, Float>();
        location.put(Nearby.LOCATION_LATITUDE_KEY, new Float(locationLat));
        location.put(Nearby.LOCATION_LONGITUDE_KEY, new Float(locationLong));
        ParcelUuid appId = null;
        if (appointmentId != null)
            appId = new ParcelUuid(UUID.fromString(appointmentId));
        return new Nearby(userId, name, birthday, photo, location, status, appId);
    }


    public static Map<String, String> parseJsonLetMeet(JSONObject json) throws JSONException {
        Map<String, String> map = new HashMap<String, String>();
        Log.d("444", "json = " + json);
        JSONObject appointment = json.getJSONObject(PARAM_appointment);
        String appointmentId = appointment.getString(PARAM_appointmentId);
        String status = appointment.getString(PARAM_status);
        String userId = appointment.getString(PARAM_userId);
        String placeId = appointment.getString(PARAM_placeId);
        map.put(PARAM_appointmentId, appointmentId);
        map.put(PARAM_status, status);
        map.put(PARAM_userId, userId);
        map.put(PARAM_placeId, placeId);
        return map;
    }

    public static String parseJsonLetMeetError(JSONObject json) throws JSONException {
        return json.getJSONObject(PARAM_error).getString("code");
    }

    public static Map<String, Map<String, String>> parseJsonPlacesId(JSONObject json) throws JSONException {
        Map<String, Map<String, String>> mapPlaces = new HashMap<String, Map<String, String>>();
        JSONObject area = json.getJSONObject(PARAM_area);
        JSONArray places = area.getJSONArray(PARAM_places);
        for (int i = 0; i < places.length(); i++) {
            String placeId = places.getJSONObject(i).getString(PARAM_placeId);
            String name = places.getJSONObject(i).getString(PARAM_name);
            float latitude = (float) places.getJSONObject(i).getJSONObject(PARAM_location).getDouble(PARAM_latitude);
            float longitude = (float) places.getJSONObject(i).getJSONObject(PARAM_location).getDouble(PARAM_longitude);
            Map<String, String> place = new HashMap<String, String>();
            place.put(PARAM_placeId, placeId);
            place.put(PARAM_latitude, Float.toString(latitude));
            place.put(PARAM_longitude, Float.toString(longitude));
            mapPlaces.put(name, place);
        }
        Map<String, String> mapCoord = new HashMap<String, String>();
        JSONObject map = area.getJSONObject(PARAM_map);
        float NWlatitude = (float) map.getJSONObject(PARAM_NW).getDouble(PARAM_latitude);
        float NWlongitude = (float) map.getJSONObject(PARAM_NW).getDouble(PARAM_longitude);

        float SElatitude = (float) map.getJSONObject(PARAM_SE).getDouble(PARAM_latitude);
        float SElongitude = (float) map.getJSONObject(PARAM_SE).getDouble(PARAM_longitude);

        mapCoord.put(PARAM_NWLATITUDE, Float.toString(NWlatitude));
        mapCoord.put(PARAM_NWLONGITUDE, Float.toString(NWlongitude));
        mapCoord.put(PARAM_SELATITUDE, Float.toString(SElatitude));
        mapCoord.put(PARAM_SELONGITUDE, Float.toString(SElongitude));
        mapPlaces.put(MAP_COORD, mapCoord);

        Map<String, String> mapLimits = new HashMap<String, String>();
        JSONObject limits = area.getJSONObject(PARAM_limits);
        int min = limits.getInt(PARAM_min);
        int max = limits.getInt(PARAM_max);

        mapLimits.put(PARAM_min, Integer.toString(min));
        mapLimits.put(PARAM_max, Integer.toString(max));
        mapPlaces.put(MAP_LIMITS, mapLimits);
        return mapPlaces;
    }

    public static String parseJsonRejectMeet(JSONObject json) {
        return null;
    }

    public static List<Map<String, String>> parseAppointments(JSONObject jsonAppointments) throws JSONException {
        List<Map<String, String>> appointmentsList = new ArrayList<Map<String, String>>();
        JSONArray appointments = jsonAppointments.getJSONArray(PARAM_appointments);
        for (int i = 0; i < appointments.length(); i++) {
            Map<String, String> map = parseApointmentJson(appointments.getJSONObject(i));
            appointmentsList.add(map);
        }
        return appointmentsList;
    }

    public static Map<String, String> parseApointmentJson(JSONObject appointments) throws JSONException {
        Map<String, String> map = new HashMap<String, String>();
        map.put(PARAM_appointmentId, appointments.getString(PARAM_appointmentId));
        map.put(PARAM_status, appointments.getString(PARAM_status));
        map.put(PARAM_userId, appointments.getString(PARAM_userId));
        map.put(PARAM_placeId, appointments.getString(PARAM_placeId));
        Log.d("API111", "parseAppointments ==============================");
        Log.d("API111", "parseAppointments placeId = " + appointments.getString(PARAM_placeId));
        Log.d("API111", "parseAppointments PARAM_status = " + appointments.getString(PARAM_status));
        Log.d("API111", "parseAppointments PARAM_userId = " + appointments.getString(PARAM_userId));
        return map;
    }

    public static Map<String, String> parsePushMap(String jsonMessage) throws JSONException {
        Map<String, String> apsMap = new HashMap<String, String>();

        JSONObject push = new JSONObject(jsonMessage);
        JSONObject aps = push.getJSONObject("aps");
        JSONObject alert = aps.getJSONObject("alert");
        apsMap.put("title", alert.getString("title"));
        apsMap.put("body", alert.getString("body"));

        return apsMap;
    }

    public static Map<String, String> parsePushAppointment(String jsonMessage) throws JSONException {
        Map<String, String> appointmentMap = new HashMap<String, String>();
        JSONObject push = new JSONObject(jsonMessage);
        JSONObject appointment = push.getJSONObject("appointment");
        appointmentMap.put("appointmentId", appointment.getString("appointmentId"));
        appointmentMap.put("status", appointment.getString("status"));
        appointmentMap.put("placeId", appointment.getString("placeId"));

        return appointmentMap;
    }

    public static Nearby parsePushTarget(String jsonMessage) throws JSONException {
        JSONObject push = new JSONObject(jsonMessage);
        JSONObject targetJson = push.getJSONObject("target");
        Nearby targetNearby = parseUserNearby(targetJson);
        return targetNearby;
    }

    public static Map<String, String> parsePushPlace(String jsonMessage) throws JSONException {
        Map<String, String> placeMap = new HashMap<String, String>();
        JSONObject push = new JSONObject(jsonMessage);
        JSONObject place = push.getJSONObject("place");
        placeMap.put("placeId", place.getString("placeId"));
        placeMap.put("name", place.getString("name"));
        placeMap.put("latitude", place.getJSONObject("location").getString("latitude"));
        placeMap.put("longitude", place.getJSONObject("location").getString("longitude"));
        return placeMap;
    }

    public static void parseSheAgreedToMeet(String jsonMessage) {

    }


    public static void parseSheRejectedMeeting(String jsonMessage) {

    }


    public static void parseSheRejectedInvitation(String jsonMessage) {

    }


    public static void parseTimeoutOfInviting(String jsonMessage) {

    }


    public static void parseSheInviteYou(String jsonMessage) {

    }
}
