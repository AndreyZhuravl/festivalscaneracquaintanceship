package com.festivalscaner.festivalscaner.network;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by andrey on 28.05.2015.
 */
public class Nearby implements Parcelable {
    public static final String PHOTO_PREVIEW_KEY = "preview";//URL квадратного фото размером 120х120
    public static final String PHOTO_PREVIEW_MEDIUM_KEY = "previewMedium";//URL квадратного фото размером 180х180
    public static final String PHOTO_PREVIEW_LARGE_KEY = "previewLarge";//URL квадратного фото размером 240х240

    public static final String LOCATION_LATITUDE_KEY = "latitude";//Широта
    public static final String LOCATION_LONGITUDE_KEY = "longitude";//Долгота

    public ParcelUuid getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getBirthday() {
        return birthday;
    }

    public HashMap<String, String> getPhoto() {
        return photo;
    }

    public HashMap<String, Float> getLocation() {
        return location;
    }

    public String getStatus() {
        return status;
    }

    public ParcelUuid getAppointmentId() {
        return appointmentId;
    }

    ParcelUuid userId;
    String name;//Имя пользователя
    String birthday;// День рождения пользователя в формате RFC 3339 ISO 8601
    HashMap<String, String> photo;//Фото пользователя с разными размерами PHOTO_PREVIEW_KEY, PHOTO_PREVIEW_MEDIUM_KEY, PHOTO_PREVIEW_LARGE_KEY
    HashMap<String, Float> location;//Месторасположение пользователя - LOCATION_LATITUDE_KEY, LOCATION_LONGITUDE_KEY
    String status;//Имя пользователя
    ParcelUuid appointmentId;

    public Nearby(ParcelUuid userId, String name, String birthday, HashMap<String, String> photo, HashMap<String, Float> location, String status, ParcelUuid appointmentId) {
        this.userId = userId;
        this.name = name;
        this.birthday = birthday;
        this.photo = photo;
        this.location = location;
        this.status = status;
        if(appointmentId == null)
            appointmentId = ParcelUuid.fromString("00000000-0000-0000-0000-000000000000");
        this.appointmentId=appointmentId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(userId, flags);
        dest.writeString(name);
        dest.writeString(birthday);
        dest.writeMap(photo);
        dest.writeMap(location);
        dest.writeString(status);
        dest.writeParcelable(appointmentId, flags);
    }

    public static final Parcelable.Creator<Nearby> CREATOR
            = new Parcelable.Creator<Nearby>() {
        public Nearby createFromParcel(Parcel source) {
            ParcelUuid userId = source.readParcelable(ParcelUuid.class.getClassLoader());
            String name= source.readString();
            String birthday= source.readString();
            HashMap<String, String> photo = new HashMap<>();
            source.readMap(photo, HashMap.class.getClassLoader());
            HashMap<String, Float> location= new HashMap<>();
            source.readMap(location, HashMap.class.getClassLoader());
            String status= source.readString();
            ParcelUuid appointmentId= source.readParcelable(ParcelUuid.class.getClassLoader());
            Nearby param = new Nearby(userId, name, birthday, photo, location, status, appointmentId);
            return param;
        }

        public Nearby[] newArray(int size) {
            return new Nearby[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Nearby)) return false;

        Nearby nearby = (Nearby) o;

        if (!birthday.equals(nearby.birthday)) return false;
        if (!location.equals(nearby.location)) return false;
        if (!name.equals(nearby.name)) return false;
        if (!photo.equals(nearby.photo)) return false;
        if (!userId.equals(nearby.userId)) return false;
        if (!status.equals(nearby.status)) return false;
        if (!appointmentId.equals(nearby.appointmentId)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + birthday.hashCode();
        result = 31 * result + photo.hashCode();
        result = 31 * result + location.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + appointmentId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Nearby{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", birthday='" + birthday + '\'' +
                ", photo=" + photo +
                ", location=" + location +
                ", status='" + status + '\'' +
                ", appointmentId=" + appointmentId +
                '}';
    }
}
