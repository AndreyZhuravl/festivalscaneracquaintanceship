package com.festivalscaner.festivalscaner.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.festivalscaner.festivalscaner.network.exception.ErrorCode;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by andrey on 28.05.2015.
 */
public class NetworkManagerDefault implements INetworkManager {
    List<List<Nearby>> lastQueryResultNearbyUserList;
    IOnNearbyUsersListener mNetManagerListener;
    Map<String, Map<String, String>> mPlacesIdMap;
    private ExecutorService mExecutor;
    private Handler mHandler;
    private String mRejectMeetingResponse;
    private List<Map<String, String>> mAppointments;

    public NetworkManagerDefault() {
        lastQueryResultNearbyUserList = null;
        mExecutor = Executors.newFixedThreadPool(4);
        mHandler = new Handler(Looper.getMainLooper());
    }

    private void getNearbyUsers(UUID sessionId, int usersInBatch, int batches, Map<String, Float> currentLocation) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;
        lastQueryResultNearbyUserList = new ArrayList<List<Nearby>>();

        String strUrl = "https://dev.privetapp.ru/ar/v1/targets" + "?usersInBatch=" + usersInBatch + "&batches=" + batches + "&" + "currentLatitude=" + currentLocation.get("currentLatitude") + "&currentLongitude=" + currentLocation.get("currentLongitude") + "&sessionId=" + sessionId.toString();

        Log.d("NET123", "strUrl = " + strUrl);
        JSONObject json;
        InputStream instream = null;
        try {

            URL url1 = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();

            instream = new BufferedInputStream(conn.getInputStream());
            json = convertStreamToString(instream);

            JsonParser.parseJsonNearby(json, lastQueryResultNearbyUserList);
        } catch (Exception e) {
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (commandResponse.errorCode == ErrorCode.OK) {
            handleNearbyUsersSuccessInUiThread();
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleNearbyUsersErrorInUiThread(errorInfo);
        }
    }

    private static JSONObject convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        JSONObject json = null;
        try {
            json = new JSONObject(reader.readLine());
        } catch (Exception e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return json;
    }

    @Override
    public void getNearbyUsersAsync(final UUID sessionId, final int usersInBatch, final int batches, final Map<String, Float> currentLocation) {

        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleNearbyUsersStartInUiThread();
                getNearbyUsers(sessionId, usersInBatch, batches, currentLocation);
            }
        });
    }

    private void handleNearbyUsersStartInUiThread() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onNearbyUsersStart();
            }
        });
    }

    private void handleNearbyUsersSuccessInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onNearbyUsersSuccess(lastQueryResultNearbyUserList);
            }
        });
    }

    private void handleNearbyUsersErrorInUiThread(final CommandErrorInfo errorInfo) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onNearbyUsersError(errorInfo);
            }
        });
    }

    @Override
    public void setNearbyUsersListener(IOnNearbyUsersListener listener) {
        mNetManagerListener = listener;
    }
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    Map<String, String> mAppointmentMeetingresult;

    private void onLetMeetUsers(UUID sessionId, UUID userId, UUID meetingPlaceID) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;

        String strUrl = "https://dev.privetapp.ru/ar/v1/appointments";
        String body = "?" + "sessionId=" + sessionId.toString() + "&" + "userId=" + userId.toString() + "&" + "placeId=" + meetingPlaceID.toString();
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(strUrl + body);


        JSONObject json;
        InputStream instream = null;
        try {
            HttpResponse response = httpclient.execute(httppost);
            Log.d("API111", "" + response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            instream = entity.getContent();
            json = convertStreamToString(instream);

            if (response.getStatusLine().getStatusCode() == 201) {

                mAppointmentMeetingresult = JsonParser.parseJsonLetMeet(json);

            } else if (response.getStatusLine().getStatusCode() == 403 ||
                    response.getStatusLine().getStatusCode() == 409) {
                commandResponse.errorCode = ErrorCode.FORBIDDEN_LOGIC;
                commandResponse.errorMessage = JsonParser.parseJsonLetMeetError(json);
            }

        } catch (Exception e) {
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (commandResponse.errorCode == ErrorCode.OK) {
            handleLetMeetSuccessInUiThread();
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleLetMeetErrorInUiThread(errorInfo);
        }
    }

    @Override
    public void onLetMeetAsync(final UUID sessionId, final UUID userId, final UUID meetingPlaceID) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleLetMeetStartInUiThread();
                onLetMeetUsers(sessionId, userId, meetingPlaceID);
            }
        });
    }

    private void handleLetMeetStartInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onLetMeetStart();
            }
        });
    }

    private void handleLetMeetSuccessInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onLetMeetSuccess(mAppointmentMeetingresult);
            }
        });
    }

    private void handleLetMeetErrorInUiThread(final CommandErrorInfo errorInfo) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onLetMeetError(errorInfo);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    private void onRejectMeeting(UUID sessionId, UUID userId, boolean isMeetingNotInvitation) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;
        String strUrlMeeting = "https://dev.privetapp.ru/ar/v1/appointments";

        getAppointmentForUser(sessionId);
        UUID apointmentIdForUser = findApointmentForUser(mAppointments, userId);
        if (apointmentIdForUser != null) {
            String strUrlRejectMeeting = "https://dev.privetapp.ru/ar/v1/appointments/" + apointmentIdForUser.toString() + "?" + "sessionId=" + sessionId.toString();
            JSONObject json;
            InputStream instream = null;


            try {

                Log.d("API111", "try1 url=" + strUrlRejectMeeting);
                URL url = new URL(strUrlRejectMeeting);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                httpConnection.setRequestMethod("DELETE");
                Log.d("API111", "try3");
                httpConnection.connect();
                Log.d("API111", "try4");

                int code = httpConnection.getResponseCode();
                Log.d("API111", "onRejectMeeting responseCode = " + code);
                if (code == 204) {


                } else if (code == 400 || code == 401 || code == 403) {
                    commandResponse.errorCode = ErrorCode.FORBIDDEN_LOGIC;
                }

            } catch (Exception e) {
                commandResponse.errorCode = ErrorCode.FATAL_ERROR;
                Log.d("API111", "onRejectMeeting e =" + e.toString());
                Log.d("API111", "onRejectMeeting e =" + e.getMessage());
            } finally {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
        }
        if (commandResponse.errorCode == ErrorCode.OK) {
            handleRejectMeetingSuccessInUiThread(userId, isMeetingNotInvitation);
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleRejectMeetingErrorInUiThread(errorInfo);
        }
    }

    private RadarCommandResponse getAppointmentForUser(UUID sessionId) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;
        String strUrlAppointments = "https://dev.privetapp.ru/ar/v1/appointments?" + "sessionId=" + sessionId.toString();
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet getAppointments = new HttpGet(strUrlAppointments);
        JSONObject jsonAppointments;
        InputStream instream = null;
        UUID appointmentId = null;
        try {
            HttpResponse response = httpclient.execute(getAppointments);
            HttpEntity entity = response.getEntity();
            instream = entity.getContent();
            jsonAppointments = convertStreamToString(instream);

            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {

                mAppointments = JsonParser.parseAppointments(jsonAppointments);

            } else if (code == 204 || code == 400 || code == 401 || code == 500) {
                commandResponse.errorCode = ErrorCode.FORBIDDEN_LOGIC;
                commandResponse.errorMessage = JsonParser.parseJsonLetMeetError(jsonAppointments);
            }

        } catch (Exception e) {
            Log.d("API111", "getAppointmentForUser e=" + e.getMessage());
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return commandResponse;
    }

    private UUID findApointmentForUser(List<Map<String, String>> appointments, UUID userId) {
        UUID appointmentId = null;
        if(appointments != null)
        for (Map<String, String> appointment : appointments) {
            String appUserId = appointment.get(JsonParser.PARAM_userId);
            if (appUserId.equals(userId.toString())) {
                appointmentId = UUID.fromString(appointment.get(JsonParser.PARAM_appointmentId));
                break;
            }
        }
        return appointmentId;
    }

    @Override
    public void onRejectMeetingAsync(final UUID ssesionId, final UUID userId, final boolean isMeetingNotInvitation) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleRejectMeetingStartInUiThread();
                onRejectMeeting(ssesionId, userId, isMeetingNotInvitation);
            }
        });
    }

    private void handleRejectMeetingStartInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onRejectMeetingStart();
            }
        });
    }

    private void handleRejectMeetingSuccessInUiThread(final UUID userId, final boolean isMeetingNotInvitation) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                if (isMeetingNotInvitation) {
                    mNetManagerListener.onRejectMeetingSuccess(userId);
                } else {
                    Log.d("API111", "onRejectInvitationSuccess");
                    mNetManagerListener.onRejectInvitationSuccess(userId);
                }
            }
        });
    }

    private void handleRejectMeetingErrorInUiThread(final CommandErrorInfo errorInfo) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onRejectMeetingError(errorInfo);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////
    private void onYouAgreedToMeet(UUID sessionId, UUID userId, UUID meetingPlaceID) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;

        String strUrl = "https://dev.privetapp.ru/ar/v1/appointments";
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(strUrl);
        JSONObject json;
        InputStream instream = null;
        try {


        } catch (Exception e) {
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
            Log.d("127", "e =" + e.toString());
            Log.d("127", "e =" + e.getMessage());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (commandResponse.errorCode == ErrorCode.OK) {
            handleYouAgreedToMeetSuccessInUiThread();
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleYouAgreedToMeetErrorInUiThread(errorInfo);
        }
    }

    @Override
    public void onYouAgreedToMeetAsync(final UUID sessionId, final UUID userId, final UUID meetingPlaceID) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleYouAgreedToMeetStartInUiThread();
                onYouAgreedToMeet(sessionId, userId, meetingPlaceID);
            }
        });
    }

    private void handleYouAgreedToMeetStartInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onYouAgreedToMeetStart();
            }
        });
    }

    private void handleYouAgreedToMeetSuccessInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onYouAgreedToMeetSuccess();
            }
        });
    }

    private void handleYouAgreedToMeetErrorInUiThread(final CommandErrorInfo errorInfo) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onYouAgreedToMeetError(errorInfo);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////
    private void onGetPlacesId(UUID sessionId) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;
        String strUrl = "https://dev.privetapp.ru/ar/v1/area";
        String body = "?" + "sessionId=" + sessionId.toString();
        Log.d("127", "strUrl + body =" + strUrl + body);
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(strUrl + body);

        InputStream instream = null;
        try {

            URL url1 = new URL(strUrl + body);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            instream = new BufferedInputStream(conn.getInputStream());

            JSONObject json = convertStreamToString(instream);
            mPlacesIdMap = JsonParser.parseJsonPlacesId(json);
            Log.d("127", "jsonMap =" + json);

        } catch (Exception e) {
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
            Log.d("127", "e1 =" + e.getMessage());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (commandResponse.errorCode == ErrorCode.OK) {
            handleGetPlacesIdSuccessInUiThread();
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleGetPlacesIdErrorInUiThread(errorInfo);
        }
    }

    @Override
    public void onGetPlacesIdAsync(final UUID sessionId) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleGetPlacesIdStartInUiThread();
                onGetPlacesId(sessionId);
            }
        });
    }

    @Override
    public void onPause() {
    }

    private void handleGetPlacesIdStartInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onGetPlacesIdStart();
            }
        });
    }

    private void handleGetPlacesIdSuccessInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onGetPlacesIdSuccess(mPlacesIdMap);
            }
        });
    }

    private void handleGetPlacesIdErrorInUiThread(final CommandErrorInfo errorInfo) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onGetPlacesIdError(errorInfo);
            }
        });
    }
    ///////////////////////////////////////////////////////////////////////////////
    private void onChangeAppointment(final UUID sessionId, final UUID appointmentId, final String status) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse.errorCode = ErrorCode.OK;

        String strUrl = "https://dev.privetapp.ru/ar/v1/appointment/" + appointmentId.toString() + "?" + "sessionId=" + sessionId.toString() + "status=" + status;
        InputStream instream = null;
        Map<String, String> appointment = null;
        try {

            Log.d("API111", "onChangeAppointment1 url=" + strUrl);
            URL url = new URL(strUrl);
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod("PUT");
            Log.d("API111", "onChangeAppointment3");
            httpConnection.connect();
            Log.d("API111", "onChangeAppointment4");
            instream = httpConnection.getInputStream();
            Log.d("API111", "onChangeAppointment5");

            int code = httpConnection.getResponseCode();
            Log.d("API111", "onChangeAppointment responseCode = " + code);
            if (code == 200) {
                JSONObject json = convertStreamToString(instream);
                appointment = JsonParser.parseApointmentJson(json);

            } else if (code == 400 || code == 401 || code == 403|| code == 404 || code == 409 || code == 500) {
                commandResponse.errorCode = ErrorCode.FORBIDDEN_LOGIC;
            } else{
                commandResponse.errorCode = ErrorCode.FATAL_ERROR;
            }
        } catch (Exception e) {
            commandResponse.errorCode = ErrorCode.FATAL_ERROR;
            Log.d("127", "e =" + e.toString());
            Log.d("127", "e =" + e.getMessage());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (commandResponse.errorCode == ErrorCode.OK) {
            handleChangeAppointmentSuccessInUiThread(appointment);
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleChangeAppointmentErrorInUiThread(errorInfo);
        }
    }

    @Override
    public void onChangeAppointmentAsync(final UUID sessionId, final UUID appointmentId, final String status) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleChangeAppointmentStartInUiThread();
                onChangeAppointment(sessionId, appointmentId, status);
            }
        });
    }

    private void handleChangeAppointmentStartInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onChangeAppointmentStart();
            }
        });
    }

    private void handleChangeAppointmentSuccessInUiThread(final Map<String, String> appointment) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onChangeAppointmentSuccess(appointment);
            }
        });
    }

    private void handleChangeAppointmentErrorInUiThread(final CommandErrorInfo errorInfo) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onChangeAppointmentError(errorInfo);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////
    private void onAppointments(UUID sessionId, UUID userId, UUID meetingPlaceID) {
        RadarCommandResponse commandResponse = new RadarCommandResponse();
        commandResponse = getAppointmentForUser(sessionId);


        if (commandResponse.errorCode == ErrorCode.OK) {
            handleAppointmentsSuccessInUiThread(mAppointments);
        } else {
            CommandErrorInfo errorInfo = new CommandErrorInfo(commandResponse.errorCode, commandResponse.errorMessage);
            handleAppointmentsErrorInUiThread(errorInfo);
        }
    }

    @Override
    public void onAppointmentsAsync(final UUID sessionId, final UUID userId, final UUID meetingPlaceID) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                handleAppointmentsStartInUiThread();
                onAppointments(sessionId, userId, meetingPlaceID);
            }
        });
    }

    private void handleAppointmentsStartInUiThread() {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onAppointmentsStart();
            }
        });
    }

    private void handleAppointmentsSuccessInUiThread(final List<Map<String, String>> appointments) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onAppointmentsSuccess(appointments);
            }
        });
    }

    private void handleAppointmentsErrorInUiThread(final CommandErrorInfo errorInfo) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mNetManagerListener.onAppointmentsError(errorInfo);
            }
        });
    }


    @Override
    public int getState() {
        return 0;
    }

    @Override
    public void prepareOnWorkerThreadIfNeeded(OnPrepareProgressListener listener) {

    }
}
