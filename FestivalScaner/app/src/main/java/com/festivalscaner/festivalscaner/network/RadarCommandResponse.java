package com.festivalscaner.festivalscaner.network;

import org.json.JSONObject;

public class RadarCommandResponse {

	public int errorCode;
	public JSONObject jsonObject;
	public String errorMessage;

	public RadarCommandResponse() {
		errorCode = 0;
		errorMessage = "";
		jsonObject = null;

	}

	@Override
	public String toString() {
		return "RadarCommandResponse [errorCode=" + errorCode + ", jsonObject=" + jsonObject + ", errorMessage="
				+ errorMessage + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + errorCode;
		result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result + ((jsonObject == null) ? 0 : jsonObject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RadarCommandResponse other = (RadarCommandResponse) obj;
		if (errorCode != other.errorCode)
			return false;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		if (jsonObject == null) {
			if (other.jsonObject != null)
				return false;
		} else if (!jsonObject.equals(other.jsonObject))
			return false;
		return true;
	}
}
