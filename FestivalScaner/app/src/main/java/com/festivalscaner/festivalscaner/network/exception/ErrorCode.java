package com.festivalscaner.festivalscaner.network.exception;

public class ErrorCode {

	public static final int OK = 0;
	public static final int NO_NETWORK = 1;
	public static final int LOW_CONNECTION = 2;
	public static final int SERVICE_UNAVAILABLE = 3;
	public static final int UNKNOWN = 4;
	public static final int TEXT_MESSAGE = 5;
	public static final int DISK_READ = 6;
	public static final int DISK_WRITE = 7;
	public static final int DISK_ERROR = 8;
	public static final int FILE_NOT_FOUND = 9;
	public static final int INVALID_FILE = 10;
	public static final int FATAL_ERROR = 11;

	public static final int NO_NETWORK_SYNC = 12;
	public static final int LOW_CONNECTION_SYNC = 13;
	public static final int SERVICE_UNAVAILABLE_SYNC = 14;

	public static final int SD_CARD_BUSY = 20;
	public static final int JSON_ERROR = 21;
	public static final int MALFORMED_URL = 22;
	public static final int BAD_URL = 23;
    public static final int ACTIVE_APPOINTMENT = 24;
    public static final int FORBIDDEN_LOGIC = 25;

}
