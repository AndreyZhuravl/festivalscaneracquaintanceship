package com.festivalscaner.festivalscaner.network.exception;

public final class LowConnectionException extends Exception {

	private static final long serialVersionUID = -6903926471245254946L;

	public LowConnectionException() {
		super();
	}

	public LowConnectionException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public LowConnectionException(String detailMessage) {
		super(detailMessage);
	}

	public LowConnectionException(Throwable throwable) {
		super(throwable);
	}

}
