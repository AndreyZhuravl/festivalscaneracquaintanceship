package com.festivalscaner.festivalscaner.network.exception;

public final class NetworkUnavailableException extends Exception {

	private static final long serialVersionUID = 7335676285478364583L;

	public NetworkUnavailableException() {
		super();
	}

	public NetworkUnavailableException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public NetworkUnavailableException(String detailMessage) {
		super(detailMessage);
	}

	public NetworkUnavailableException(Throwable throwable) {
		super(throwable);
	}

}
