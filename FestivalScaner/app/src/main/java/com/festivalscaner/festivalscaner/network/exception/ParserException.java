package com.festivalscaner.festivalscaner.network.exception;

public final class ParserException extends Exception {
	
	private static final long serialVersionUID = -3889331785790283139L;

	public ParserException() {
		super();
	}

	public ParserException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public ParserException(String detailMessage) {
		super(detailMessage);
	}

	public ParserException(Throwable throwable) {
		super(throwable);
	}

}
