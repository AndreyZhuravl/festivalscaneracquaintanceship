package com.festivalscaner.festivalscaner.network.exception;

public final class ServiceUnavailableException extends Exception {

	private static final long serialVersionUID = -2813258420881294468L;

	public ServiceUnavailableException() {
		super();
	}

	public ServiceUnavailableException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public ServiceUnavailableException(String detailMessage) {
		super(detailMessage);
	}

	public ServiceUnavailableException(Throwable throwable) {
		super(throwable);
	}

}
