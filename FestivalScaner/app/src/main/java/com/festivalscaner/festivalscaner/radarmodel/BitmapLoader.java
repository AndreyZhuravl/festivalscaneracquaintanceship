package com.festivalscaner.festivalscaner.radarmodel;

import android.graphics.Bitmap;
import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class BitmapLoader implements IBitmapLoader {
    private ExecutorService mExecutor;
    HashMap<String, Bitmap> mBitMapUrlSet;

    public BitmapLoader() {

        mExecutor = Executors.newFixedThreadPool(4);
        mBitMapUrlSet = new HashMap<String, Bitmap>();
    }


    @Override
    public void addImageUrlToLoading(final String imageUrl, final UserData userData, final Context context) {
        if (!mBitMapUrlSet.containsKey(imageUrl)) {
            mExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    Bitmap b = ModelUtils.getBitmapFromURL(imageUrl);
                    if (b != null) {
                        userData.setUserIcon(b);
                        userData.prepareBitmapForRadar(context);
                        put(imageUrl, b);
                        Log.d("LOAD111","userData2 - " + userData.getID().toString());
                    }
                }
            });
        } else {
            mExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    userData.setUserIcon(get(imageUrl));
                    userData.prepareBitmapForRadar(context);
                    Log.d("LOAD111","userData1 - " + userData.getID().toString());
                }
            });
        }
    }

    public synchronized void put(String key, Bitmap value) {
        mBitMapUrlSet.put(key, value);
    }

    public synchronized Bitmap get(String key) {
        return mBitMapUrlSet.get(key);
    }

    @Override
    public void clearCache() {
        mBitMapUrlSet = new HashMap<String, Bitmap>();
    }

    @Override
    public Bitmap tryCache(String imageUrl) {
        return get(imageUrl);
    }
}
