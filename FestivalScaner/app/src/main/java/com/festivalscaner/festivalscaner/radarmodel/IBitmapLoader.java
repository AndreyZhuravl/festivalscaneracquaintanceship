package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by andrey on 29.05.2015.
 */
public interface IBitmapLoader {
    public void addImageUrlToLoading(String imageUrl, UserData userData, Context context);
    public void clearCache();
    public Bitmap tryCache(String imageUrl);
}
