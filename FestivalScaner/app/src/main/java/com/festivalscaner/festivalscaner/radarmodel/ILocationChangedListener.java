package com.festivalscaner.festivalscaner.radarmodel;

import android.location.Location;

/**
 * Created by andrey on 07.05.2015.
 */
public interface ILocationChangedListener {

    void update(Location location);
}
