package com.festivalscaner.festivalscaner.radarmodel;

import android.graphics.Canvas;
import android.location.Location;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.festivalscaner.festivalscaner.RadarSurfaceView;
import com.festivalscaner.festivalscaner.fsm.RadarFSMContext;
import com.festivalscaner.festivalscaner.network.INetworkManager;
import com.festivalscaner.festivalscaner.viewcontroller.RadarActivity;
import com.festivalscaner.festivalscaner.viewcontroller.RadarView;
import com.festivalscaner.festivalscaner.viewcontroller.openglcam.CameraGLSurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by andrey on 08.05.2015.
 */
public interface IRadarModel extends INetworkManager.IOnNearbyUsersListener {
    float getX1();

    float getY1();

    float getZ1();

    ArrayList<UserData> getUserPolarCoordinates();

    public void setUserPolarCoordinates(ArrayList<UserData> userPolarCoordinates);

    public void registerSensorListeners();

    public void unregisterSensorListeners();

    public void onDrawRadar(Canvas canvas);

    public void setWidthHeight(int mWidth, int mHeight);

    public List<UserData> getVisibleUsersList();

    public boolean onTouch(MotionEvent event);

    public void updateRadar();

    public void updateLastLocation(Location lastLocation);

    public void requestNextNearby();

    public void setOreintation(int degrees);

    public void onDraw(Canvas canvas);

    public void setCameraView(CameraGLSurfaceView mCameraView);

    public boolean isCurrentMeeting();

    public void updateFSM();

    public void switchResumePauseStateFSMR(boolean isResume);

    public void onLetMeet(UserData mCurrentUser, String a);

    public void youRejectInvitation(UUID id);

    public RadarFSMContext getRadarFSMContext();

    public void setRadarFSMContext(RadarFSMContext radarFSMContext);

    public void youRejectMeeting(UUID id);

    public void setView(RadarView radarView);

    public void setSurfaceView(RadarSurfaceView radarView);

    public void toTheCurrentMeeting();

    public void onSheAgreedToMeet(String jsonMessage);

    public void onSheRejectedMeeting(String jsonMessage);

    public void onSheRejectedInvitation(String jsonMessage);

    public void onTimeoutOfInviting(String jsonMessage);

    public void onSheInviteYou(String jsonMessage);

    public void nextStackAction();

    void setCameraLayout(RadarActivity activity, FrameLayout preview);
}
