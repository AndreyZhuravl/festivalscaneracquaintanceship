package com.festivalscaner.festivalscaner.radarmodel;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.MotionEvent;

/**
 * Created by andrey on 10.06.2015.
 */
public interface IScreenHolder {
    String getAction(MotionEvent event);

    void drawHolder(Canvas canvas);
}
