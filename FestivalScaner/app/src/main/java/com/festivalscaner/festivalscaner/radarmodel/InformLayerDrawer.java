package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Vibrator;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.MotionEvent;

import com.festivalscaner.festivalscaner.R;
import com.festivalscaner.festivalscaner.fsm.RadarFSMContext;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSAnimationLodingState;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSEmptyRadarState;
import com.festivalscaner.festivalscaner.viewcontroller.RadarView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class InformLayerDrawer {

    private final float mLetMeetStrokeWidth;
    private final Bitmap mLikePinBitmap;
    private final Bitmap mWaitePinBitmap;
    private final Bitmap mRejectedPinBitmap;
    private UserData mCurrentUser;
    private final Context mContext;
    private int mWidth;
    private int mHeight;
    private Paint paintBackground;
    private Paint mPaintForeground;
    public float mUserIconSize;
    public float mPudding;
    private RectF mCloseButtonRect;
    private RectF mUserLayerRect;
    private RectF mInformationButtonRect;
    private Paint mPaintName;
    private Paint mPaintAge;
    private float mButtonHeight;
    private Paint mPaintWite;
    private RectF mLetMeetButtonRect;
    private Paint mPaintCloseButton;
    private Paint mPaintClosetText;
    private Paint mPaintWiteStripe;
    private float mStripeWidth;
    private Paint mPaintSimpleButton;
    private Paint mPaintSimpleText;
    private Paint mPaintTittle;
    private Paint mPaintSubTittle;
    private Paint mPaintName2;

    private ScreenHolder mLetMeetScreenHolder;
    private ScreenHolder mYouCantAppointScreenHolder;
    private ScreenHolder mSheAgreedScreenHolder;
    //private ScreenHolder mYouAgreedMeetScreenHolder;
    private RoundScreenHolder mSheDisapointMeetScreenHolder;
    private RoundScreenHolder mYouDisapointMeetScreenHolder;
    private RoundScreenHolder mYouRejectInvitaionScreenHolder;
    private ScreenHolder mWaitAnswerScreenHolder;
    private ScreenHolder mYouAreInvitedScreenHolder;
    private ScreenHolder mWeMeetScreenHolder;
    private ScreenHolder mProfileHolder;

    private String mMeetingPlaceString;
    private TextPaint mSpecialText1Paint;
    private TextPaint mSpecialText2Paint;
    private StaticLayout mSpecialText1Layout;
    private StaticLayout mSpecialText2Layout;
    private IRadarModel mRadarModel;
    private RadarView mRadarView;


    public InformLayerDrawer(Context context, IRadarModel radarModel) {
        mContext = context;
        mUserIconSize = mContext.getResources().getDimension(R.dimen.user_layer_height);
        mPudding = mContext.getResources().getDimension(R.dimen.user_top_bar_pudding);
        mLetMeetStrokeWidth = mContext.getResources().getDimension(R.dimen.user_top_bar_let_meet_stroke_width);
        mCurrentUser = null;
        mLikePinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_like_pin), mUserIconSize / 4, mUserIconSize / 4, mContext);
        mWaitePinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_wating_pin), mUserIconSize / 4, mUserIconSize / 4, mContext);
        mRejectedPinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_rejected_pin), mUserIconSize / 4, mUserIconSize / 4, mContext);
        mMeetingPlaceString = "Перейдите на место встречи B";
        mRadarModel = radarModel;
        createPaintCloseButton();
        createPaintSimpleButton();
        createPaintBackground();
        createPaintNameAge();
    }

    public UserData getCurrentUser() {
        return mCurrentUser;
    }

    public UUID getCurrentUserID() {
        UUID id = UUID.randomUUID();
        if (mCurrentUser != null)
            id = mCurrentUser.getID();
        return id;
    }

    public RadarFSMContext getRadarFSMContext() {
        return mRadarModel.getRadarFSMContext();
    }

    public void setView(RadarView radarView) {
        mRadarView = radarView;
    }

    public class ScreenButton {
        public RectF buttonRect;
        public Paint paintButtonText;
        public RectF buttonTextRect;
        public String text;
        public String action;

        public ScreenButton() {
        }

        public void drawButton(Canvas canvas) {
            Bitmap bitmap = ModelUtils.textAsBitmap(text, paintButtonText);
            canvas.drawBitmap(bitmap,buttonTextRect.left, buttonTextRect.top- bitmap.getHeight(), null);
        }
    }

    public class ScreenHolder {
        public RectF userLayerRect;
        public List<ScreenButton> screenButtonList;
        public String name;
        public RectF title1;
        public RectF title2;
        public String title1Text;
        public String title2Text;
        private RectF mWhiteButtonRect;
        private float deltaTitle;
        private String title3Text;
        private RectF title3;
        private boolean mIsSpecialText;
        private float mBaseTopLayer;


        public ScreenHolder() {
            userLayerRect = new RectF(0, mHeight - 3 * mPudding - mUserIconSize - mStripeWidth, mWidth, mHeight);
            mBaseTopLayer = mHeight - 3 * mPudding - mUserIconSize - mStripeWidth;
            screenButtonList = new ArrayList<ScreenButton>();
            title1Text = "";
            title2Text = "";
            title3Text = "";
            deltaTitle = 0;
            mIsSpecialText = false;
        }

        public String getAction(MotionEvent event) {
            String act = "";
            for (ScreenButton button : screenButtonList) {
                if (button.buttonRect.contains(event.getX(), event.getY())) {
                    act = button.action;
                    break;
                }
            }
            return act;
        }

        public void setTitle(String title1Str, String title2Str) {
            title1Text = title1Str;
            title2Text = title2Str;
            float deltaY2 = 1.2f * mPaintTittle.getTextSize();
            float yTitle1 = userLayerRect.top + mPudding + mUserIconSize / 2;
            title1 = new RectF(mPudding * 2 + mUserIconSize, yTitle1, 0, 0);
            title2 = new RectF(mPudding * 2 + mUserIconSize, yTitle1 + deltaY2, 0, 0);
            deltaTitle = deltaY2;
            userLayerRect.top -= deltaTitle;
        }

        public void setTitle(String title1Str, String title2Str, String title3Str) {
            title1Text = title1Str;
            title2Text = title2Str;
            title3Text = title3Str;
            float deltaY2 = mPaintTittle.getTextSize();
            float deltaY3 = 1.4f * mPaintSubTittle.getTextSize();
            float yTitle1 = userLayerRect.top + mPudding + mUserIconSize / 3;
            title1 = new RectF(mPudding * 2 + mUserIconSize, yTitle1, 0, 0);
            title2 = new RectF(mPudding * 2 + mUserIconSize, yTitle1 + deltaY2, 0, 0);
            title3 = new RectF(mPudding * 2 + mUserIconSize, yTitle1 + deltaY2 + deltaY3, 0, 0);
            deltaTitle = deltaY2 + deltaY3;
            userLayerRect.top -= deltaTitle;
        }

        public void setSpecialTitle(String textAbove, String textBelow) {
            mIsSpecialText = true;
            mSpecialText1Paint = new TextPaint();
            mSpecialText1Paint.setTextSize(mContext.getResources().getDimension(R.dimen.user_cant_apoint_meeting_text1_size));
            mSpecialText1Paint.setColor(Color.argb(0xff, 0x10, 0x10, 0x10));
            mSpecialText1Paint.setFlags(Paint.ANTI_ALIAS_FLAG);
            mSpecialText2Paint = new TextPaint();
            mSpecialText2Paint.setTextSize(mContext.getResources().getDimension(R.dimen.user_cant_apoint_meeting_text2_size));
            mSpecialText2Paint.setColor(Color.argb(0xff, 0xA0, 0xA0, 0xA0));
            mSpecialText2Paint.setFlags(Paint.ANTI_ALIAS_FLAG);
            if (textAbove.equals(""))
                textAbove = mContext.getString(R.string.you_cant_apoint_meeting_text1);
            if (textBelow.equals(""))
                textBelow = mContext.getString(R.string.in_order_to_apoint_meeting_text);
            mSpecialText1Layout = new StaticLayout(textAbove, mSpecialText1Paint, (int) (mWidth - mPudding * 3.0f - mUserIconSize), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
            mSpecialText2Layout = new StaticLayout(textBelow, mSpecialText2Paint, (int) (mWidth - mPudding * 3.0f - mUserIconSize), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);

            float deltaY1 = mSpecialText1Layout.getHeight();
            float deltaY2 = mSpecialText2Layout.getHeight();
            deltaTitle = deltaY1 * 1.05f + deltaY2;
            userLayerRect.top = mBaseTopLayer - deltaTitle;
            float yTitle1 = userLayerRect.top + mPudding + mUserIconSize;
            title1 = new RectF(mPudding * 2 + mUserIconSize, yTitle1, 0, 0);
            title2 = new RectF(mPudding * 2 + mUserIconSize, yTitle1 + deltaY1 * 1.05f, 0, 0);
        }

        public void drawHolder(Canvas canvas) {
            drawBackground(canvas);
            for (ScreenButton button : screenButtonList) {
                button.drawButton(canvas);
            }
            drawUserIcon(canvas, screenButtonList.size(), deltaTitle);
            if (mIsSpecialText) {
                drawUserSpecialText(canvas);
            } else {
                if (!title1Text.equals("")) {
                    drawUserAlias1String(canvas);
                } else {
                    drawUserAlias2String(canvas);
                }
            }
        }

        private void drawUserSpecialText(Canvas canvas) {
            String userName = mCurrentUser.getAlias();
            String userAge = mCurrentUser.getUserAge() + ModelUtils.getAgeWord(mCurrentUser.getUserAge());
            float deltaYName = 1.5f * mPaintName.getTextSize();
            float deltaYAge = 1.2f * mPaintAge.getTextSize();
            float deltaXName = mPaintName.measureText(userName) / 2;
            float deltaXAge = mPaintAge.measureText(userAge) / 2;
            float Yname = userLayerRect.top + mPudding + mUserIconSize / 2;
            float Yage = userLayerRect.top + mPudding + mUserIconSize / 2 + deltaXAge;
            Bitmap bitmap = ModelUtils.textAsBitmap(userName, mPaintName);
            canvas.drawBitmap(bitmap,mPudding * 2 + mUserIconSize, Yname - bitmap.getHeight(), null);
            Bitmap bitmap1 = ModelUtils.textAsBitmap(userAge, mPaintAge);
            canvas.drawBitmap(bitmap1,mPudding * 2 + mUserIconSize, Yage - bitmap1.getHeight(), null);

            Bitmap bitmapStaticLayer1 = ModelUtils.textLayoutAsBitmap(mSpecialText1Layout);
            canvas.drawBitmap(bitmapStaticLayer1,title1.left, title1.top, null);

            Bitmap bitmapStaticLayer2 = ModelUtils.textLayoutAsBitmap(mSpecialText2Layout);
            canvas.drawBitmap(bitmapStaticLayer2,title2.left, title2.top, null);
        }


        private void drawUserAlias2String(Canvas canvas) {
            String userName = mCurrentUser.getAlias();
            String userAge = mCurrentUser.getUserAge() + ModelUtils.getAgeWord(mCurrentUser.getUserAge());
            float deltaYName = 1.5f * mPaintName.getTextSize();
            float deltaYAge = 1.2f * mPaintAge.getTextSize();
            float deltaXName = mPaintName.measureText(userName) / 2;
            float deltaXAge = mPaintAge.measureText(userAge) / 2;
            float Yname = userLayerRect.top + mPudding + mUserIconSize / 2;
            float Yage = userLayerRect.top + mPudding + mUserIconSize / 2 + deltaXAge;
            Bitmap bitmap = ModelUtils.textAsBitmap(userName, mPaintName);
            canvas.drawBitmap(bitmap,mPudding * 2 + mUserIconSize, Yname - bitmap.getHeight(), null);
            Bitmap bitmap1 = ModelUtils.textAsBitmap(userAge, mPaintAge);
            canvas.drawBitmap(bitmap1,mPudding * 2 + mUserIconSize, Yage - bitmap1.getHeight(), null);
        }

        private void drawUserAlias1String(Canvas canvas) {

            String userName = mCurrentUser.getAlias();
            if (!mCurrentUser.getUserAge().equals(""))
                userName += ", " + mCurrentUser.getUserAge();
            float deltaYName = 0.5f * mPaintName2.getTextSize();
            float Yname = userLayerRect.top + mPudding + mUserIconSize / 2 - deltaYName;
            Bitmap bitmap = ModelUtils.textAsBitmap(userName, mPaintName2);
            canvas.drawBitmap(bitmap,mPudding * 2 + mUserIconSize, Yname - bitmap.getHeight(), null);
            drawTitile(canvas, Yname);
        }

        private void drawTitile(Canvas canvas, float Yname) {
            Bitmap bitmap = ModelUtils.textAsBitmap(title1Text, mPaintTittle);
            canvas.drawBitmap(bitmap,title1.left, title1.top - bitmap.getHeight(), null);
            if (!title2Text.equals("")) {
                Bitmap bitmap1 = ModelUtils.textAsBitmap(title2Text, mPaintTittle);
                canvas.drawBitmap(bitmap1,title2.left, title2.top - bitmap1.getHeight(), null);
            }
            if (!title3Text.equals("")) {
                Bitmap bitmap1 = ModelUtils.textAsBitmap(title3Text, mPaintSubTittle);
                canvas.drawBitmap(bitmap1,title3.left, title3.top - bitmap1.getHeight(), null);
            }


        }


        public RectF getButtonsRect() {
            RectF buttonRect = new RectF(2 * mPudding + mUserIconSize, mHeight - mPudding, mWidth, mHeight - mPudding);
            buttonRect.top -= mButtonHeight * screenButtonList.size();
            return buttonRect;
        }

        public void addButton(String text, String action, Paint paintButtonText) {
            ScreenButton button = new ScreenButton();
            button.text = text;
            button.action = action;
            button.paintButtonText = paintButtonText;
            RectF buttonsRect = getButtonsRect();
            button.buttonRect = new RectF(buttonsRect.left, buttonsRect.top - mButtonHeight - deltaTitle, buttonsRect.right, buttonsRect.top);
            float deltaYClose = button.paintButtonText.getTextSize();
            button.buttonTextRect = new RectF(buttonsRect.left, button.buttonRect.centerY() + deltaYClose / 4, 0, 0);
            screenButtonList.add(button);
            userLayerRect.top -= button.buttonRect.height();
            mBaseTopLayer -= button.buttonRect.height();
            mWhiteButtonRect = new RectF(2 * mPudding + mUserIconSize, mHeight - mPudding - screenButtonList.size() * mButtonHeight - mStripeWidth, mWidth, mHeight - mPudding - screenButtonList.size() * mButtonHeight);
        }

        private void drawBackground(Canvas canvas) {
            canvas.drawRect(0, 0, mWidth, mHeight, paintBackground);
            canvas.drawRect(userLayerRect, mPaintSimpleButton);
            canvas.drawRect(mWhiteButtonRect, mPaintWiteStripe);
        }

        private void drawUserIcon(Canvas canvas, int buttonCount, float deltaTitile) {
            Bitmap icon = ModelUtils.getResizedBitmap(mCurrentUser.getUserIcon(), mUserIconSize, mUserIconSize, mContext);
            canvas.drawCircle(mPudding + mUserIconSize / 2, mHeight - 2 * mPudding - buttonCount * mButtonHeight - mUserIconSize / 2 - deltaTitile, mUserIconSize / 2 + mLetMeetStrokeWidth / 2, mPaintWite);
            canvas.drawBitmap(ModelUtils.getRoundedBitmap(icon), mPudding, mHeight - 2 * mPudding - buttonCount * mButtonHeight - mUserIconSize - deltaTitile, mPaintForeground);

        }


    }

    public class RoundScreenHolder implements IScreenHolder {
        private final Paint mPaintText2;
        public RectF userLayerRect;
        private final float mUserIconSize;
        private final float mPudding;
        private final float mLetMeetStrokeWidth;
        private final Paint mPaintWite;
        private final Paint mPaintText1;
        private final Paint paintStrokeNet;
        private Paint mPaintForeground;
        private Paint mPaintName;

        float mCurrentX;
        float mCurrentY;
        private RectF mCloseButtonRect;
        private float mButtonSize;
        private String text1;
        private String text2;
        private String text3;

        public RoundScreenHolder(String text1, String text2, String text3) {
            createPaintBackground();
            setText(text1, text2, text3);
            mCurrentX = 0;
            mCurrentY = 0;
            mUserIconSize = mContext.getResources().getDimension(R.dimen.user_map_icon_height) * 0.8f;
            mPudding = mContext.getResources().getDimension(R.dimen.user_top_bar_pudding);

            mPaintWite = new Paint();
            mPaintWite.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
            mPaintWite.setFlags(Paint.ANTI_ALIAS_FLAG);

            mPaintName = new Paint();
            mPaintName.setColor(Color.argb(0xff, 0xf0, 0xf0, 0xf0));
            mPaintName.setTextSize(mContext.getResources().getDimension(R.dimen.user_name_map_text_size));
            mPaintName.setFlags(Paint.ANTI_ALIAS_FLAG);

            mPaintText1 = new Paint();
            mPaintText1.setColor(Color.argb(0xff, 0xf0, 0xf0, 0xf0));
            mPaintText1.setTextSize(mContext.getResources().getDimension(R.dimen.user_name_text_size));
            mPaintText1.setFlags(Paint.ANTI_ALIAS_FLAG);

            mPaintText2 = new Paint();
            mPaintText2.setColor(Color.argb(0xff, 0xf0, 0xf0, 0xf0));
            mPaintText2.setTextSize(mContext.getResources().getDimension(R.dimen.user_message2_text_size));
            mPaintText2.setFlags(Paint.ANTI_ALIAS_FLAG);

            paintStrokeNet = new Paint();
            paintStrokeNet.setColor(Color.argb(0xff, 0x80, 0x80, 0x80));
            paintStrokeNet.setStyle(Paint.Style.FILL_AND_STROKE);
            paintStrokeNet.setAntiAlias(true);
            paintStrokeNet.setStrokeWidth(2);

            mLetMeetStrokeWidth = mContext.getResources().getDimension(R.dimen.user_top_bar_let_meet_stroke_width);
            mButtonSize = mUserIconSize / 2;
            this.mCloseButtonRect = new RectF(mWidth - mPudding - mButtonSize, mPudding, mWidth - mPudding, mButtonSize + mPudding);
        }

        private void createPaintBackground() {
            paintBackground = new Paint();
            paintBackground.setColor(Color.argb(0xA0, 0x0, 0x0, 0x0));
        }

        public void setText(String text1, String text2, String text3) {
            this.text1 = text1;
            this.text2 = text2;
            this.text3 = text3;
        }

        @Override
        public String getAction(MotionEvent event) {
            String act = "";
            if (this.mCloseButtonRect.contains(event.getX(), event.getY())) {
                act = "RadarFSMSEmptyRadarState";
            }
            return act;
        }

        @Override
        public void drawHolder(Canvas canvas) {
            drawBackground(canvas);
            drawCircle(canvas);
            drawUserIcon(canvas);
            drawUserNameAge(canvas);
            drawCloseButton(canvas);
        }

        private void drawBackground(Canvas canvas) {
            canvas.drawRect(0, 0, mWidth, mHeight, paintBackground);
        }

        private void drawCircle(Canvas canvas) {
            float r100 = mHeight * 1.4f;
            float yCenter = mHeight - 2 * mUserIconSize + r100;
            canvas.drawCircle(mWidth / 2, yCenter, r100, paintStrokeNet);
        }

        private void drawCloseButton(Canvas canvas) {
            Bitmap icon = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(
                    mContext.getResources(), R.drawable.top_bar_close_button), mButtonSize / 2, mButtonSize / 2, mContext);
            canvas.drawBitmap(icon, mWidth - mPudding - mButtonSize / 2, mPudding, mPaintForeground);
        }

        private void drawUserIcon(Canvas canvas) {
            Bitmap icon = ModelUtils.getResizedBitmap(mCurrentUser.getUserIcon(), mUserIconSize, mUserIconSize, mContext);
            canvas.drawCircle(mWidth / 2, mHeight - 2 * mUserIconSize, mUserIconSize / 2 + mLetMeetStrokeWidth / 2, mPaintWite);
            canvas.drawBitmap(ModelUtils.getRoundedBitmap(icon), mWidth / 2 - mUserIconSize / 2, mHeight - 5 * mUserIconSize / 2, mPaintForeground);
            if (mCurrentUser.getStatusUser() == UserData.StatusUser.inviter) {
                canvas.drawBitmap(mLikePinBitmap, mWidth / 2 + mUserIconSize * 0.25f, mHeight - 2 * mUserIconSize - mUserIconSize * 0.45f, mPaintForeground);
            }
            if (mCurrentUser.getStatusUser() == UserData.StatusUser.invited) {
                canvas.drawBitmap(mWaitePinBitmap, mWidth / 2 + mUserIconSize * 0.25f, mHeight - 2 * mUserIconSize - mUserIconSize * 0.45f, mPaintForeground);
            }
            if ( getRadarFSMContext().getStateCurrent().getClass().getSimpleName().equals("RadarFSMSYouRejectMeetingState") ||
                    getRadarFSMContext().getStateCurrent().getClass().getSimpleName().equals("RadarFSMSYouRejectInvitationState")) {
                canvas.drawBitmap(mRejectedPinBitmap, mWidth / 2 + mUserIconSize * 0.25f, mHeight - 2 * mUserIconSize - mUserIconSize * 0.45f, mPaintForeground);
            }
        }


        private void drawUserNameAge(Canvas canvas) {
            String userNameAge = mCurrentUser.getAlias();
            if (!mCurrentUser.getUserAge().equals(""))
                userNameAge += ", " + mCurrentUser.getUserAge();
            float deltaYName = 0.5f * mPaintName.getTextSize();
            float deltaXName = mPaintName.measureText(userNameAge) / 2;
            Bitmap bitmap = ModelUtils.textAsBitmap(userNameAge, mPaintName);
            canvas.drawBitmap(bitmap, mWidth / 2 - deltaXName, mHeight - mUserIconSize - mPudding - deltaYName - bitmap.getHeight(), null);
            float deltaYmessageSent = 0.5f * mPaintText1.getTextSize();
            float deltaXmessageSent = mPaintText1.measureText(text1) / 2;
            float deltaXtext2 = mPaintText2.measureText(text2) / 2;
            float deltaXtext3 = mPaintText2.measureText(text3) / 2;
            Bitmap bitmap1 = ModelUtils.textAsBitmap(text1, mPaintText1);
            canvas.drawBitmap(bitmap1,mWidth / 2 - deltaXmessageSent, mHeight - mUserIconSize - mPudding - deltaYName + 2 * deltaYmessageSent  - bitmap1.getHeight(),null);
            Bitmap bitmap2 = ModelUtils.textAsBitmap(text2, mPaintText2);
            canvas.drawBitmap(bitmap2,mWidth / 2 - deltaXtext2, mHeight - mUserIconSize - mPudding - deltaYName + 4 * deltaYmessageSent - bitmap2.getHeight(),  null);
            Bitmap bitmap3 = ModelUtils.textAsBitmap(text3, mPaintText2);
            canvas.drawBitmap(bitmap3, mWidth / 2 - deltaXtext3, mHeight - mUserIconSize - mPudding - deltaYName + 6 * deltaYmessageSent - bitmap3.getHeight(), null);
        }
    }

    private void createPaintNameAge() {
        int color = Color.argb(0xff, 0x90, 0x90, 0x90);
        mPaintName = new Paint();
        mPaintName.setColor(color);
        mPaintName.setTextSize(mContext.getResources().getDimension(R.dimen.user_name_text_size));
        mPaintName.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaintAge = new Paint();
        mPaintAge.setColor(color);
        mPaintAge.setTextSize(mContext.getResources().getDimension(R.dimen.user_age_text_size));
        mPaintAge.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaintName2 = new Paint();
        mPaintName2.setColor(color);
        mPaintName2.setTextSize(mContext.getResources().getDimension(R.dimen.user_name_text_size));
        mPaintName2.setFlags(Paint.ANTI_ALIAS_FLAG);


        mPaintTittle = new Paint();
        mPaintTittle.setColor(color);
        mPaintTittle.setTextSize(mContext.getResources().getDimension(R.dimen.user_title_text_size));
        mPaintTittle.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaintSubTittle = new Paint();
        mPaintSubTittle.setColor(color);
        mPaintSubTittle.setTextSize(mContext.getResources().getDimension(R.dimen.user_sub_title_text_size));
        mPaintSubTittle.setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    private void createPaintCloseButton() {
        mPaintCloseButton = new Paint();
        mPaintCloseButton.setColor(Color.argb(0xff, 0xff, 0x0, 0x0));
        mPaintCloseButton.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintClosetText = new Paint();
        mPaintClosetText.setColor(Color.argb(0xff, 0x10, 0x10, 0x10));
        mPaintClosetText.setTextSize(mContext.getResources().getDimension(R.dimen.let_meet_text_size));
        mPaintClosetText.setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    private void createPaintSimpleButton() {
        mPaintSimpleButton = new Paint();
        mPaintSimpleButton.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
        mPaintSimpleButton.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintWite = new Paint();
        mPaintWite.setColor(Color.argb(0xff, 0xA0, 0xA0, 0xA0));
        mPaintWite.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintWiteStripe = new Paint();
        mPaintWiteStripe.setColor(Color.argb(0xff, 0xA0, 0xA0, 0xA0));
        mPaintWiteStripe.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintSimpleText = new Paint();
        mPaintSimpleText.setColor(Color.argb(0xff, 0xff, 0x0, 0x0));
        mPaintSimpleText.setTextSize(mContext.getResources().getDimension(R.dimen.let_meet_text_size));
        mPaintSimpleText.setFlags(Paint.ANTI_ALIAS_FLAG);
    }


    public void setMeetingPlace(String meetingPlace) {
        mMeetingPlaceString = meetingPlace;
    }

    private void createPaintBackground() {
        paintBackground = new Paint();
        paintBackground.setColor(Color.argb(0x80, 0x0, 0x0, 0x0));
    }

    public void setCurrentUserID(UserData currentUser) {
        this.mCurrentUser = currentUser;
    }

    public void setWidthHeight(int width, int height) {
        mStripeWidth = 2.0f;
        this.mHeight = height;
        this.mWidth = width;
        mButtonHeight = mContext.getResources().getDimension(R.dimen.user_button_size);
        mUserLayerRect = new RectF(0, 0, mWidth, mHeight);
        mCloseButtonRect = new RectF(2 * mPudding + mUserIconSize, mHeight - mPudding - mButtonHeight, mWidth, mHeight - mPudding);

        mInformationButtonRect = new RectF(2 * mPudding + mUserIconSize, mHeight - 2 * mButtonHeight - mPudding, mWidth, mHeight - mButtonHeight);
        mLetMeetButtonRect = new RectF(2 * mPudding + mUserIconSize, mHeight - 3 * mButtonHeight - mPudding, mWidth, mHeight - 2 * mButtonHeight);
        createScreenHolders();
    }

    public void setSpecialTextOnForbidenMeeting(String textAbove, String textBelow) {
        mYouCantAppointScreenHolder.setSpecialTitle(textAbove, textBelow);
    }

    private void createScreenHolders() {
        mLetMeetScreenHolder = new ScreenHolder();
        mLetMeetScreenHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mLetMeetScreenHolder.addButton("Профиль", "RadarFSMSProfileState", mPaintSimpleText);
        mLetMeetScreenHolder.addButton("Давай встретимся", "RadarFSMSChoosePlaceState", mPaintSimpleText);

        mYouCantAppointScreenHolder = new ScreenHolder();
        mYouCantAppointScreenHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mYouCantAppointScreenHolder.addButton("К текущей встрече", "ToCurrentMeeting", mPaintSimpleText);
        mYouCantAppointScreenHolder.setSpecialTitle("", "");

        mSheAgreedScreenHolder = new ScreenHolder();
        mSheAgreedScreenHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mSheAgreedScreenHolder.addButton("Профиль", "RadarFSMSProfileState", mPaintSimpleText);
        mSheAgreedScreenHolder.addButton("Мы встретились", "RadarFSMSWeMeetState", mPaintSimpleText);
        mSheAgreedScreenHolder.addButton("Отменить встречу", "RadarFSMSYouRejectMeetingState", mPaintSimpleText);
        mSheAgreedScreenHolder.addButton("Начать переписку", "RadarFSMSEmptyRadarState", mPaintSimpleText);
        mSheAgreedScreenHolder.setTitle("Согласилась", "встретиться");

        mSheDisapointMeetScreenHolder = new RoundScreenHolder("Пользователь отменил встречу", "Пользователь больше не будет виден", "вам на радаре");
        mYouDisapointMeetScreenHolder = new RoundScreenHolder("Вы отменили встречу", "Пользователь больше не будет виден", "вам на радаре");
        mYouRejectInvitaionScreenHolder = new RoundScreenHolder("Вы отменили приглашение", "Пользователь больше не будет виден", "вам на радаре");

        mYouAreInvitedScreenHolder = new ScreenHolder();
        mYouAreInvitedScreenHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mYouAreInvitedScreenHolder.addButton("Профиль", "RadarFSMSProfileState", mPaintSimpleText);
        mYouAreInvitedScreenHolder.addButton("Отклонить приглашение", "RadarFSMSYouRejectInvitationState", mPaintSimpleText);
        mYouAreInvitedScreenHolder.addButton("Давай встретимся", "RadarFSMSYouAgreedMeetingState", mPaintSimpleText);
        mYouAreInvitedScreenHolder.setTitle("Приглашает вас", "встретиться");

        mWaitAnswerScreenHolder = new ScreenHolder();
        mWaitAnswerScreenHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mWaitAnswerScreenHolder.addButton("Отменить приглашение", "RadarFSMSYouRejectInvitationState", mPaintSimpleText);
        mWaitAnswerScreenHolder.setTitle("Приглашение", "отправлено");

        mWeMeetScreenHolder = new ScreenHolder();
        mWeMeetScreenHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mWeMeetScreenHolder.setTitle("У нас есть подарок", "для вас");

        mProfileHolder = new ScreenHolder();
        mProfileHolder.addButton("Закрыть", "RadarFSMSEmptyRadarState", mPaintClosetText);
        mProfileHolder.setTitle("Профиль", "");

    }

    public void draw(Canvas canvas) {
        switch (getRadarFSMContext().getStateCurrent().getClass().getSimpleName()) {
            case "RadarFSMSEmptyRadarState":
                break;
            case"RadarFSMSProfileState" :{
                mProfileHolder.drawHolder(canvas);
                break;}
            case "RadarFSMSForbidTwoMeetingState": {
                mYouCantAppointScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSLetMeetState": {
                mLetMeetScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSWaitAnswerState": {
                mWaitAnswerScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSSheAgreedMeetState": {
                mSheAgreedScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSWeMeetState": {
                mWeMeetScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSYouAreInvitedState": {
                mYouAreInvitedScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSYouRejectInvitationState": {
                mYouRejectInvitaionScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSYouRejectMeetingState": {
                mYouDisapointMeetScreenHolder.drawHolder(canvas);
                break;
            }
            case "RadarFSMSSheRejectMeetingState": {
                mSheDisapointMeetScreenHolder.drawHolder(canvas);
                break;
            }
            default: {
                break;
            }
        }

    }

    public boolean onTouch(MotionEvent event) {
        boolean result = false;
        Vibrator vibro = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        if (!(getRadarFSMContext().getStateCurrent() instanceof RadarFSMSEmptyRadarState) && !(getRadarFSMContext().getStateCurrent() instanceof RadarFSMSAnimationLodingState)) {
            switch (getRadarFSMContext().getStateCurrent().getClass().getSimpleName()) {
                case "RadarFSMSEmptyRadarState":
                    break;
                case "RadarFSMSProfileState" : {
                    result = true;
                    switch (mProfileHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSForbidTwoMeetingState": {
                    result = true;
                    switch (mYouCantAppointScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        case "ToCurrentMeeting":
                            Log.d("API111", "ToCurrentMeeting1");
                            mRadarModel.toTheCurrentMeeting();
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        case "RadarFSMSProfileState":
                            getRadarFSMContext().TrESProfile();
                            break;

                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSLetMeetState": {
                    result = true;
                    switch (mLetMeetScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        case "RadarFSMSChoosePlaceState":
                            //mRadarModel.isCurrentMeeting();
                            if (mCurrentUser.getmStatusPin() == UserData.StatusPin.MEETING) {
                                getRadarFSMContext().TrEToForbidTwoMeeting();
                            } else {
                                getRadarFSMContext().TrELetMeet();
                            }
                            break;
                        case "RadarFSMSProfileState":
                            getRadarFSMContext().TrESProfile();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSWaitAnswerState": {
                    result = true;
                    switch (mWaitAnswerScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState": {
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        }
                        case "RadarFSMSYouRejectInvitationState": {
                            mRadarModel.youRejectInvitation(mCurrentUser.getID());
                            getRadarFSMContext().TrESEmptyRadar();

                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSSheAgreedMeetState": {
                    result = true;
                    switch (mSheAgreedScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();

                            break;
                        case "RadarFSMSYouRejectMeetingState":
                            mRadarModel.youRejectMeeting(mCurrentUser.getID());
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        case "RadarFSMSWeMeetState":
                            getRadarFSMContext().TrEToWeMeet();
                            break;
                        case "RadarFSMSProfileState":
                            getRadarFSMContext().TrESProfile();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSWeMeetState": {
                    result = true;
                    switch (mWeMeetScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSYouAreInvitedState": {
                    result = true;
                    switch (mYouAreInvitedScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        case "RadarFSMSYouAgreedMeetingState":
                            getRadarFSMContext().TrEToYouAgreedMeet();
                            break;
                        case "RadarFSMSYouRejectInvitationState":
                            mRadarModel.youRejectInvitation(mCurrentUser.getID());
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        case "RadarFSMSProfileState":
                            getRadarFSMContext().TrESProfile();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSYouRejectInvitationState": {
                    result = true;
                    switch (mYouRejectInvitaionScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSYouRejectMeetingState": {
                    result = true;
                    switch (mYouDisapointMeetScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                case "RadarFSMSSheRejectMeetingState": {
                    result = true;
                    switch (mSheDisapointMeetScreenHolder.getAction(event)) {
                        case "RadarFSMSEmptyRadarState":
                            getRadarFSMContext().TrESEmptyRadar();
                            break;
                        default: {
                            break;
                        }
                    }
                    break;
                }
                default: {
                    break;
                }
            }

        }

        return result;
    }
}
