package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;

import com.festivalscaner.festivalscaner.R;
import com.festivalscaner.festivalscaner.network.JsonParser;
import com.festivalscaner.festivalscaner.viewcontroller.RadarView;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrey on 13.05.2015.
 */
public class MeetingPlaces {
    private final Context mContext;
    private final float mUserIconSize;
    private final float mPudding;
    private final float mLetMeetStrokeWidth;
    private final Paint mPaintWite;
    private final Paint mPaintChoosePlace;
    private int mWidth;
    private int mHeight;
    private Paint mPaintBackground;
    private Paint mPaintForeground;

    private Paint mPaintPlaceBackground;
    private Paint mPainPlaceForeground;
    private Paint mPaintName;
    Bitmap mMap;
    float mCurrentX;
    float mCurrentY;
    Drawable mMapDrawable;

    int mapWidth = 1896;
    int mapHeight = 1468;

    float x1 = 0;
    float y1 = 0;

    float x2 = 0;
    float y2 = 0;

    int mRectSize = 60;
    Map<String, Map<String, Rect>> mRectPlaces;
    Rect mRectA, mRectB, mRectC, mRectD, mRectO;
    int placeAX = 503 + 297;
    int placeAY = 153 + 537;

    int placeCX = 511 + 657;
    int placeCY = 153 + 384;

    int placeBX = 1091 + 243;
    int placeBY = 490;

    int placeDX = 1038 + 420;
    int placeDY = 592;

    int placeOX = 313;
    int placeOY = 977;
    private UserData mCurrentUser;
    private float mButtonSize;
    private RectF mCloseButtonRect;
    private Map<String, Map<String, String>> mMeetingPlacesMap;
    IRadarModel mRadarModel;
    private RadarView mRadarView;

    public MeetingPlaces(Context context, IRadarModel radarModel) {
        mContext = context;
        mRadarModel = radarModel;

        InputStream mapInput = mContext.getResources().openRawResource(R.raw.map3);
        if (mContext.getResources().getBoolean(R.bool.is_hdpi)) {
            mapInput = mContext.getResources().openRawResource(R.raw.map4);
            mapWidth = 2652;
            mapHeight = 2062;
        }
        if (mContext.getResources().getBoolean(R.bool.is_xhdpi) && mContext.getResources().getBoolean(R.bool.is_xxhdpi)) {
            mapInput = mContext.getResources().openRawResource(R.raw.map5);
            mapWidth = 3789;
            mapHeight = 2946;
        }
        mMapDrawable = Drawable.createFromStream(mapInput, "transit_map");
        mMeetingPlacesMap = null;
        mMapDrawable.setBounds(0, 0, mapWidth, mapHeight);
        mCurrentX = 0;
        mCurrentY = 0;
        mUserIconSize = mContext.getResources().getDimension(R.dimen.user_map_icon_height) / 2;
        mPudding = mContext.getResources().getDimension(R.dimen.user_top_bar_pudding);

        mPaintWite = new Paint();
        mPaintWite.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
        mPaintWite.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaintName = new Paint();
        mPaintName.setColor(Color.argb(0xff, 0xf0, 0xf0, 0xf0));
        mPaintName.setTextSize(mContext.getResources().getDimension(R.dimen.user_name_map_text_size));
        mPaintName.setFlags(Paint.ANTI_ALIAS_FLAG);

        mPaintChoosePlace = new Paint();
        mPaintChoosePlace.setColor(Color.argb(0xff, 0xf0, 0xf0, 0xf0));
        mPaintChoosePlace.setTextSize(mContext.getResources().getDimension(R.dimen.user_name_text_size));
        mPaintChoosePlace.setFlags(Paint.ANTI_ALIAS_FLAG);

        mLetMeetStrokeWidth = mContext.getResources().getDimension(R.dimen.user_top_bar_let_meet_stroke_width);
        mButtonSize = mUserIconSize / 2;

        mRectA = new Rect(placeAX - mRectSize, placeAY - mRectSize, placeAX + mRectSize, placeAY + mRectSize);
        mRectB = new Rect(placeBX - mRectSize, placeBY - mRectSize, placeBX + mRectSize, placeBY + mRectSize);
        mRectC = new Rect(placeCX - mRectSize, placeCY - mRectSize, placeCX + mRectSize, placeCY + mRectSize);
        mRectD = new Rect(placeDX - mRectSize, placeDY - mRectSize, placeDX + mRectSize, placeDY + mRectSize);
        mRectO = new Rect(placeOX - mRectSize, placeOY - mRectSize, placeOX + mRectSize, placeOY + mRectSize);
        createPaintPlaceBackground();
        createPaintPlaceForeground();
    }


    public void setWidthHeight(int width, int height) {
        this.mHeight = height;
        this.mWidth = width;
        mCloseButtonRect = new RectF(mWidth - mPudding - mButtonSize, mPudding, mWidth - mPudding, mButtonSize + mPudding);
    }

    private void createPaintPlaceBackground() {
        mPaintPlaceBackground = new Paint();
        mPaintPlaceBackground.setColor(Color.argb(0xff, 0xff, 0x0, 0x0));
        mPaintPlaceBackground.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaintPlaceBackground.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    private void createPaintPlaceForeground() {
        mPainPlaceForeground = new Paint();
        mPainPlaceForeground.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
        mPainPlaceForeground.setTextSize(24);
        mPainPlaceForeground.setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    public void draw(Canvas canvas) {

        float zoom = 1.0f;

        int left = (int) (mCurrentX - x2 + x1);
        left = left < 0 ? 0 : left;
        left = left > mapWidth - mWidth ? mapWidth - mWidth : left;
        left = -left;
        int top = (int) (mCurrentY - y2 + y1);
        top = top < 0 ? 0 : top;
        top = top > mapHeight - mHeight ? mapHeight - mHeight : top;
        top = -top;
        int right = (int) (zoom * mapWidth + left);
        int bottom = (int) (zoom * mapHeight + top);

        mMapDrawable.setBounds(left, top, right, bottom);
        mMapDrawable.draw(canvas);
        drawUserIcon(canvas);
        drawUserNameAge(canvas);
        drawCloseButton(canvas);
        drawPlaces(canvas);
    }

    private void drawPlaces(Canvas canvas) {
        int left = (int) (mCurrentX - x2 + x1);
        left = left < 0 ? 0 : left;
        left = left > mapWidth - mWidth ? mapWidth - mWidth : left;

        int top = (int) (mCurrentY - y2 + y1);
        top = top < 0 ? 0 : top;
        top = top > mapHeight - mHeight ? mapHeight - mHeight : top;

        for (String place : mRectPlaces.keySet()) {
            float deltaXName = 0.5f * mPaintName.measureText(place);
            float deltaYName = 0.5f * mPaintName.getTextSize();
            Rect rect = mRectPlaces.get(place).get(mRectPlaces.get(place).keySet().toArray()[0]);
            RectF rec = new RectF(rect.left - left, rect.top - top, rect.right - left, rect.bottom - top);
            canvas.drawRoundRect(rec, mRectSize / 4, mRectSize / 4, mPaintPlaceBackground);
            canvas.drawBitmap(ModelUtils.textAsBitmap(place, mPaintName), rect.centerX() - left - deltaXName, rect.centerY() - top - deltaYName, null);
            //canvas.drawText(place, rect.centerX()-deltaXName- left, rect.centerY()-deltaYName- top, mPaintName);
        }
    }


    private void drawCloseButton(Canvas canvas) {
        Bitmap icon = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(
                mContext.getResources(), R.drawable.top_bar_close_button), mButtonSize, mButtonSize, mContext);
        canvas.drawBitmap(icon, mWidth - mPudding - mButtonSize, mPudding, mPaintForeground);
    }

    private void drawUserIcon(Canvas canvas) {
        Bitmap icon = ModelUtils.getResizedBitmap(mCurrentUser.getUserIcon(), mUserIconSize, mUserIconSize, mContext);
        canvas.drawCircle(mUserIconSize / 2 + mPudding, mUserIconSize / 2 + mPudding, mUserIconSize / 2 + mLetMeetStrokeWidth / 2, mPaintWite);
        canvas.drawBitmap(ModelUtils.getRoundedBitmap(icon), mPudding, mPudding, mPaintForeground);
    }


    private void drawUserNameAge(Canvas canvas) {
        String userNameAge = mCurrentUser.getAlias();
        if (!mCurrentUser.getUserAge().equals(""))
            userNameAge += ", " + mCurrentUser.getUserAge();
        float deltaYName = 0.5f * mPaintName.getTextSize();
        Bitmap bitmapAge = ModelUtils.textAsBitmap(userNameAge, mPaintName);
        canvas.drawBitmap(bitmapAge, 2 * mPudding + mUserIconSize, 2 * mPudding + deltaYName - bitmapAge.getHeight(), null);
        //canvas.drawText(userNameAge, 2 * mPudding + mUserIconSize, 2 * mPudding + deltaYName, mPaintName);
        float deltaYChoosePlace = 0.5f * mPaintChoosePlace.getTextSize();
        Bitmap bitmap = ModelUtils.textAsBitmap(mContext.getString(R.string.choose_place_text), mPaintChoosePlace);
        canvas.drawBitmap(bitmap, 2 * mPudding + mUserIconSize, (float) (2 * mPudding + deltaYName + 2.4 * deltaYChoosePlace) - bitmap.getHeight(), null);
        Bitmap bitmap1 = ModelUtils.textAsBitmap(mContext.getString(R.string.meeting_text), mPaintChoosePlace);
        canvas.drawBitmap(bitmap1, 2 * mPudding + mUserIconSize, (float) (2 * mPudding + deltaYName + 4.2 * deltaYChoosePlace) - bitmap.getHeight(), null);
//        canvas.drawText(mContext.getString(R.string.choose_place_text), 2 * mPudding + mUserIconSize, (float) (2 * mPudding + deltaYName + 2.4 * deltaYChoosePlace), mPaintChoosePlace);
//        canvas.drawText(mContext.getString(R.string.meeting_text), 2 * mPudding + mUserIconSize, (float) (2 * mPudding + deltaYName + 4.2 * deltaYChoosePlace), mPaintChoosePlace);
    }

    public void setCurrentUserID(UserData currentTopBarUser) {
        this.mCurrentUser = currentTopBarUser;
    }

    public boolean onTouch(MotionEvent event) {
        boolean result = true;


        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // gets called
            {
                if (mRectPlaces != null) {
                    int left = (int) (mCurrentX - x2 + x1);
                    left = left < 0 ? 0 : left;
                    left = left > mapWidth - mWidth ? mapWidth - mWidth : left;

                    int top = (int) (mCurrentY - y2 + y1);
                    top = top < 0 ? 0 : top;
                    top = top > mapHeight - mHeight ? mapHeight - mHeight : top;
                    Log.d("TOUCH129", "event.getX() = " + event.getX());
                    Log.d("TOUCH129", "event.getY() = " + event.getY());
                    Log.d("TOUCH129", "mCurrentX = " + mCurrentX);
                    Log.d("TOUCH129", "mCurrentY = " + mCurrentY);
                    Log.d("TOUCH129", "mCurrentX + event.getX() = " + (mCurrentX + event.getX()));
                    Log.d("TOUCH129", "mCurrentY + event.getY() = " + (mCurrentY + event.getY()));

                    Log.d("TOUCH129", "mCurrentX/density + event.getX() = " + (mCurrentX / mContext.getResources().getDisplayMetrics().density + event.getX()));
                    Log.d("TOUCH129", "mCurrentY/density + event.getY() = " + (mCurrentY / mContext.getResources().getDisplayMetrics().density + event.getY()));


                    Log.d("TOUCH129", "mapWidth = " + mapWidth);
                    Log.d("TOUCH129", "mapHeight = " + mapHeight);
                    for (String place : mRectPlaces.keySet()) {
//                        Rect rect1 = mRectPlaces.get(place).get(mRectPlaces.get(place).keySet().toArray()[0]);
//                        Log.d("TOUCH129", "rect.centerX() = " + rect1.centerX());
//                        Log.d("TOUCH129", "rect.centerY() = " + rect1.centerY());
//                        Log.d("TOUCH129", "density = " + mContext.getResources().getDisplayMetrics().density);

                        Rect rect = mRectPlaces.get(place).get(mRectPlaces.get(place).keySet().toArray()[0]);
                        RectF rec = new RectF(rect.left - left, rect.top - top, rect.right - left, rect.bottom - top);

                        if (rec.contains((int) (event.getX()), event.getY())) {
                            String placeId = (String) mRectPlaces.get(place).keySet().toArray()[0];
                            mRadarModel.onLetMeet(mCurrentUser, placeId);
                            mRadarModel.getRadarFSMContext().TrESEmptyRadar();
                            Vibrator vibro = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
                            vibro.vibrate(RadarModel.FEED_BACK_VIBRATE_MILLISECONDS);
                            result = false;
                            break;
                        }
                    }
                } else {

                }
                x1 = event.getX();
                y1 = event.getY();
                x2 = event.getX();
                y2 = event.getY();
                break;
            }
            case MotionEvent.ACTION_MOVE: // doesnt seem to do anything
            {
                x2 = event.getX();
                y2 = event.getY();
                break;
            }
            case MotionEvent.ACTION_UP: // doesnt seem to do anything
            {

                mCurrentX += x1 - event.getX();
                mCurrentY += y1 - event.getY();

                guardEdgeMap();

                x1 = 0;
                x2 = 0;
                y1 = 0;
                y2 = 0;
                result = false;
                break;
            }
        }
        if (mCloseButtonRect.contains(event.getX(), event.getY())) {
            mRadarModel.getRadarFSMContext().TrESEmptyRadar();
            Vibrator vibro = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            vibro.vibrate(RadarModel.FEED_BACK_VIBRATE_MILLISECONDS);
            result = false;

        }

        return result;
    }

    private void guardEdgeMap() {
        if (mCurrentX < 0) mCurrentX = 0;
        if (mCurrentY < 0) mCurrentY = 0;

        if (mCurrentX > mapWidth) mCurrentX = mapWidth;
        if (mCurrentY > mapHeight) mCurrentY = mapHeight;
    }

    public void setMeetingPlaces(Map<String, Map<String, String>> meetingPlacesMap) {
        mMeetingPlacesMap = meetingPlacesMap;
        Map<String, String> mapCoord = mMeetingPlacesMap.get(JsonParser.MAP_COORD);
        float nwLat = Float.parseFloat(mapCoord.get(JsonParser.PARAM_NWLATITUDE));
        float nwLog = Float.parseFloat(mapCoord.get(JsonParser.PARAM_NWLONGITUDE));
        float seLat = Float.parseFloat(mapCoord.get(JsonParser.PARAM_SELATITUDE));
        float seLog = Float.parseFloat(mapCoord.get(JsonParser.PARAM_SELONGITUDE));

        float mapWidthGeo = Math.abs(nwLat - seLat);
        float mapHeightGeo = Math.abs(nwLog - seLog);
        mRectPlaces = new HashMap<String, Map<String, Rect>>();
        for (String keyPlaceName : mMeetingPlacesMap.keySet()) {
            if (keyPlaceName != JsonParser.MAP_COORD && keyPlaceName != JsonParser.MAP_LIMITS) {
                Map<String, String> place = mMeetingPlacesMap.get(keyPlaceName);
                Rect rectPlace = new Rect();
                float deltaXName = 0.5f * mPaintName.measureText(keyPlaceName);
                float deltaYName = 0.5f * mPaintName.getTextSize();
                rectPlace.top = (int) ((int) (((-Float.parseFloat(place.get(JsonParser.PARAM_latitude)) + nwLat) / mapWidthGeo) * mapWidth) - deltaYName - mRectSize / 2);
                rectPlace.left = (int) ((int) (((Float.parseFloat(place.get(JsonParser.PARAM_longitude)) - nwLog) / mapHeightGeo) * mapHeight) - deltaXName - mRectSize / 2);
                rectPlace.bottom = (int) ((int) (((-Float.parseFloat(place.get(JsonParser.PARAM_latitude)) + nwLat) / mapWidthGeo) * mapWidth) + deltaYName + mRectSize / 2);
                rectPlace.right = (int) ((int) (((Float.parseFloat(place.get(JsonParser.PARAM_longitude)) - nwLog) / mapHeightGeo) * mapHeight) + deltaXName + mRectSize / 2);
                Map<String, Rect> placeIdRect = new HashMap<String, Rect>();
                placeIdRect.put(place.get(JsonParser.PARAM_placeId), rectPlace);
                mRectPlaces.put(keyPlaceName, placeIdRect);
            }
        }
    }

    public boolean isMapLoaded() {
        return mMeetingPlacesMap != null;
    }

    public void setView(RadarView radarView) {
        mRadarView = radarView;
    }
}
