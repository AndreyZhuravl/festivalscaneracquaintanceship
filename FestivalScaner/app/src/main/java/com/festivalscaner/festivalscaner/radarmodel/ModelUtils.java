package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Location;
import android.text.StaticLayout;
import android.util.Log;

import com.festivalscaner.festivalscaner.R;
import com.festivalscaner.festivalscaner.network.Nearby;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.festivalscaner.festivalscaner.network.Nearby.PHOTO_PREVIEW_KEY;

/**
 * Created by andrey on 08.05.2015.
 */
public class ModelUtils {

    public static Bitmap getResizedBitmap(Bitmap bm, float newWidth, float newHeight, Context context) {
        if (bm != null) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = newWidth / width;
            float scaleHeight = newHeight / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                    matrix, false);
            return resizedBitmap;
        } else {
            return null;
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        Bitmap myBitmap = null;
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(input);
        } catch (Exception e) {

        }
        return myBitmap;
    }

    public static Bitmap getRoundedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);


        return output;
    }

    public static void sortPolarByAngle(ArrayList<UserData> polarCoordinates) {
        Collections.sort(polarCoordinates, new Comparator<UserData>() {

            @Override
            public int compare(UserData lhs, UserData rhs) {
                int result = 0;
                if (lhs.mPoint.y < rhs.mPoint.y) {
                    result = 1;
                } else if (lhs.mPoint.y > rhs.mPoint.y) {
                    result = -1;
                }
                return result;
            }
        });

    }
    public static void sortPolarByDistance(ArrayList<UserData> polarCoordinates) {
        Collections.sort(polarCoordinates, new Comparator<UserData>() {

            @Override
            public int compare(UserData lhs, UserData rhs) {
                int result = 0;
                if (lhs.mPoint.x < rhs.mPoint.x) {
                    result = 1;
                } else if (lhs.mPoint.x > rhs.mPoint.x) {
                    result = -1;
                }
                return result;
            }
        });

    }

    public static ArrayList<UserData> convertCoordinatesToPolar(CopyOnWriteArrayList<UserData> coordinates, Context context) {
        ArrayList<UserData> listPolarCoordinates = (new ArrayList<UserData>());
        Log.d("123","+++++++++++++++++++++++++++++++++++++++++");
        for (UserData point : coordinates) {
            Log.d("123","**********************************");
            Log.d("123","point.x = " + point.getPoint().x);
            Log.d("123","point.t = " + point.getPoint().y);
            UserData polarPoint = coordinateToPolarPoint2D(point, context);
            listPolarCoordinates.add(polarPoint);
            Log.d("123","polarPoint.x = " + polarPoint.getPoint().x);
            Log.d("123","polarPoint.y = " + polarPoint.getPoint().y);
        }
        return listPolarCoordinates;
    }

    public static UserData coordinateToPolarPoint2D(UserData gostData, Context context) {
        double R = Math.sqrt(gostData.getPoint().x * gostData.getPoint().x
                + gostData.getPoint().y * gostData.getPoint().y);
        double MINUS_BECAUSE_WE_SEE_FROM_X = -1.0;
        double angle = Math.atan2(gostData.getPoint().y,
                MINUS_BECAUSE_WE_SEE_FROM_X * gostData.getPoint().x);
        if (gostData.getPoint().y > 0) {
        } else {
            angle += 2 * Math.PI;
        }
        angle = angle * 180 / Math.PI;
        gostData.setPoint(new Point2D((float) R, (float) angle));
        gostData.prepareBitmapForRadar(context);
        return gostData;
    }

    public static CopyOnWriteArrayList<UserData> convertCoordinatesToLocal(List<Nearby> mNearbyList, Location mLastLocation, IBitmapLoader bitmapLoader, Bitmap defaultBitmap, Context context) {
        CopyOnWriteArrayList<UserData> list = new CopyOnWriteArrayList<UserData>();
        for (Nearby nearbyUser : mNearbyList) {
            UserData polarPoint = convertNearbyToUserData(nearbyUser, mLastLocation, defaultBitmap);
            bitmapLoader.addImageUrlToLoading(nearbyUser.getPhoto().get(PHOTO_PREVIEW_KEY), polarPoint, context);
            list.add(polarPoint);
        }
        return list;
    }

    private static UserData convertNearbyToUserData(Nearby nearbyUser, Location mLastLocation, Bitmap defaultBitmap) {
        double metersInDergeeLong = 111111.11f;
        double latCoeff = Math.cos(nearbyUser.getLocation().get(Nearby.LOCATION_LONGITUDE_KEY)/180.0*Math.PI);
        double metersInDergeeLat = metersInDergeeLong*latCoeff;

        double X = (nearbyUser.getLocation().get(Nearby.LOCATION_LATITUDE_KEY) - mLastLocation.getLatitude())*metersInDergeeLat;
        double Y = (nearbyUser.getLocation().get(Nearby.LOCATION_LONGITUDE_KEY) - mLastLocation.getLongitude())*metersInDergeeLong;
        UserData.StatusUser status = translateUserStaus(nearbyUser.getStatus());
        UserData polarPoint = new UserData(
                new Point2D((float)X, (float)Y), nearbyUser.getUserId().getUuid(),
                nearbyUser.getName(), nearbyUser.getBirthday(), defaultBitmap, status);
        return polarPoint;
    }

    public static Bitmap textAsBitmap(String text, Paint paint) {
        if(text.equals("")) text = " ";
        int width = (int) (paint.measureText(text) + 0.5f); // round
        float baseline = (int) (-paint.ascent() + 0.5f); // ascent() is negative
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    public static Bitmap textLayoutAsBitmap(StaticLayout textLayout) {
        int width = textLayout.getWidth();
        int height = textLayout.getHeight();
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        textLayout.draw(canvas);
        return image;
    }
    public static UserData.StatusUser translateUserStaus(String status) {
        UserData.StatusUser st = UserData.StatusUser.normal;
        switch (status) {
            case "made":
                st = UserData.StatusUser.invited;
                break;
            case "invited":
                st = UserData.StatusUser.invited;
                break;
            case "inviter":
                st = UserData.StatusUser.inviter;
                break;
            case "appointment":
                st = UserData.StatusUser.appointment;
                break;
            default:
                break;
        }
        return st;
    }

    public static String getAgeWord(String userAge) {
        String wordAge = " лет";
        switch (userAge) {
            case "21":
            case "31":
            case "41":
            case "51":
                wordAge = " год";
                break;
            case "22":
            case "23":
            case "24":
            case "32":
            case "33":
            case "34":
            case "42":
            case "43":
            case "44":
            case "52":
            case "53":
            case "54":
                wordAge = " года";
                break;
            case "":
                wordAge = "";
                break;
            default:
                wordAge = " лет";
                break;
        }
        return wordAge;
    }
}
