package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.MotionEvent;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by andrey on 09.05.2015.
 */
public class Navigator implements SensorEventListener {
    private static final int SMOOTING_WINDOW_LENTH = 8;
    private static final float DVDT_KOEFF = 0.5f;
    public static int CAMERA_ANGLE_OF_VISION = 20;
    private static boolean mIsRotationAndCompas;
    boolean hasMagmeticSensor = false;
    boolean hasRotationSensor = false;
    boolean hasAcselerometrSensor = false;
    private static float mDragX1;
    private static float mDragX2;
    private static int mCurrentDragX;
    private float dZ;

    public static void setWidth(float mWidth) {
        Navigator.mWidth = mWidth;
    }

    private static float mWidth;
    public float[] xArrWindows;
    public float[] yArrWindows;
    public float[] zArrWindows;
    public float xCurrSummWindows;
    public float yCurrSummWindows;
    public float zCurrSummWindows;
    public float zCurrSumm360_ZeroWindows;
    public float zCurrSumm360_ZeroWindowsMedean;
    public float x = 0;
    public float y = 0;
    public float z = 0;
    protected SensorManager mSensorManager;
    protected Sensor mAccelerometer;
    protected Sensor mMagneticSens;
    protected Sensor mRotationSens;
    protected float[] mMagnetic;
    protected float[] mGravity;
    int xCounterWindow = 0;
    private int yCounterWindow;
    private int zCounterWindow;
    private float[] z360_ZeroArrWindows;
    protected Context mContext;
    boolean mIsStart = true;
    Executor mExecutor;
    private float[] mRotation = new float[16];

    private float[] mOrientation = new float[9];
    public int angleOrientation = 90;

    public Navigator(Context context) {
        mContext = context;
        initSensors();
        initSmoothingWindows();
        mExecutor = new ThreadPoolExecutor(0, 1, 1, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(1));
        dZ = 0;
    }

    private void initSensors() {
        mSensorManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagneticSens = mSensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mRotationSens = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        detectSensorType();
    }

    private void detectSensorType() {
        List<Sensor> deviceSensors = mSensorManager
                .getSensorList(Sensor.TYPE_ALL);

        for (Sensor sens : deviceSensors) {
            if (sens.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                hasMagmeticSensor = true;
            if (sens.getType() == Sensor.TYPE_ACCELEROMETER)
                hasAcselerometrSensor = true;
            if (sens.getType() == Sensor.TYPE_ROTATION_VECTOR)
                hasRotationSensor = true;
        }
        mIsRotationAndCompas = hasRotationSensor || (hasMagmeticSensor && hasAcselerometrSensor);
    }

    public void registerSensorListeners() {
        if (hasRotationSensor) {
            mSensorManager.registerListener(this, mRotationSens, SensorManager.SENSOR_DELAY_GAME);
        } else {
            if (hasMagmeticSensor && hasAcselerometrSensor) {
                mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
                mSensorManager.registerListener(this, mMagneticSens, SensorManager.SENSOR_DELAY_GAME);
            }
        }
    }


    public void unregisterSensorListeners() {
        mSensorManager.unregisterListener(this);
    }

    protected void initSmoothingWindows() {
        xArrWindows = new float[SMOOTING_WINDOW_LENTH];
        yArrWindows = new float[SMOOTING_WINDOW_LENTH];
        zArrWindows = new float[SMOOTING_WINDOW_LENTH];
        xCurrSummWindows = 0;
        yCurrSummWindows = 0;
        zCurrSummWindows = 0;
        zCurrSumm360_ZeroWindows = 0;
        z360_ZeroArrWindows = new float[SMOOTING_WINDOW_LENTH];
        for (int i = 0; i < SMOOTING_WINDOW_LENTH; i++) {
            xArrWindows[i] = 0;
            yArrWindows[i] = 0;
            zArrWindows[i] = 0;
            z360_ZeroArrWindows[i] = 0;
        }
    }

    private float nextStepByWindowX(float value) {
        float result = value;
        xCounterWindow++;
        int lastValueIndex = xCounterWindow - 1;
        if (xCounterWindow > SMOOTING_WINDOW_LENTH - 1) {
            xCounterWindow = 0;
            lastValueIndex = SMOOTING_WINDOW_LENTH - 1;
        }
        float dvdt = xArrWindows[lastValueIndex];
        float temp = xArrWindows[xCounterWindow];
        xArrWindows[xCounterWindow] = value;
        xCurrSummWindows -= temp;
        xCurrSummWindows += value;
        result = xCurrSummWindows / (float) SMOOTING_WINDOW_LENTH;
        return result;
    }

    private float nextStepByWindowY(float value) {
        float result = value;
        yCounterWindow++;
        if (yCounterWindow > SMOOTING_WINDOW_LENTH - 1) {
            yCounterWindow = 0;

        }

        float temp = yArrWindows[yCounterWindow];
        yArrWindows[yCounterWindow] = value;
        yCurrSummWindows -= temp;
        yCurrSummWindows += value;
        result = yCurrSummWindows / (float) SMOOTING_WINDOW_LENTH;
        return result;
    }

    private float nextStepByWindowZ(float value) {
        float result = value;
        float result360_Zero = value;
        if (((zCurrSummWindows / (float) SMOOTING_WINDOW_LENTH - value) < 0.5f) && ((zCurrSummWindows / (float) SMOOTING_WINDOW_LENTH - value) >= -0.5f)) {
            return zCurrSummWindows / (float) SMOOTING_WINDOW_LENTH;
        }

        zCounterWindow++;
        int lastValueIndex = zCounterWindow - 1;
        if (zCounterWindow > SMOOTING_WINDOW_LENTH - 1) {
            zCounterWindow = 0;
            lastValueIndex = SMOOTING_WINDOW_LENTH - 1;
        }
        float temp = zArrWindows[zCounterWindow];
        float dvdt = zArrWindows[lastValueIndex];
        float dvdt360_Zero = zArrWindows[lastValueIndex];
        float temp360_Zero = z360_ZeroArrWindows[zCounterWindow];
        zArrWindows[zCounterWindow] = value;
        if (value < CAMERA_ANGLE_OF_VISION) {
            z360_ZeroArrWindows[zCounterWindow] = value + 360;
        } else {
            z360_ZeroArrWindows[zCounterWindow] = value;
        }
        zCurrSummWindows -= temp;
        zCurrSummWindows += value;
        zCurrSumm360_ZeroWindows -= temp360_Zero;
        zCurrSumm360_ZeroWindows += z360_ZeroArrWindows[zCounterWindow];

        result = zCurrSummWindows / (float) SMOOTING_WINDOW_LENTH;
        result360_Zero = zCurrSumm360_ZeroWindows / (float) SMOOTING_WINDOW_LENTH;
        if (result360_Zero >= 359.9999f) {
            result360_Zero = result360_Zero - 359.9999f;
        }
        zCurrSumm360_ZeroWindowsMedean = result360_Zero;
        if ((value < CAMERA_ANGLE_OF_VISION / 4) || (value > (360 - CAMERA_ANGLE_OF_VISION / 4))) {
            result = result360_Zero;
        }
        return result;
    }

    protected void getDirection() {

        float[] temp = new float[9];
        float[] R = new float[9];
        // Load rotation matrix into R
        SensorManager.getRotationMatrix(temp, null, mGravity, mMagnetic);

        // Remap to camera's point-of-view
        SensorManager.remapCoordinateSystem(temp, SensorManager.AXIS_X,
                SensorManager.AXIS_Z, R);

        // Return the orientation values
        float[] values = new float[3];
        SensorManager.getOrientation(R, values);

        // Convert to degrees
        for (int i = 0; i < values.length; i++) {
            Double degrees = (values[i] * 180) / Math.PI;
            values[i] = degrees.floatValue();
        }
        x = round(nextStepByWindowX(round(values[1])));
        y = (float) (-nextStepByWindowY((float) (round((values[2]) + angleOrientation) / 180.0f * Math.PI)));
        z = round(nextStepByWindowZ(mod(values[0] + 360.0f, 360.0f))) - dZ;

    }

    protected void getDirectionOnlyGravity() {


    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_MAGNETIC_FIELD:
                mMagnetic = event.values.clone();
                break;
            case Sensor.TYPE_ACCELEROMETER:

                mGravity = event.values.clone();
                if (mMagnetic != null && mRotation != null) {
                    getDirection();
                }
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                SensorManager.getRotationMatrixFromVector(mRotation, event.values);
                calculateRotation();
                break;

            default:
                return;
        }

    }

    private void calculateRotation() {
        SensorManager.remapCoordinateSystem(mRotation, SensorManager.AXIS_X,
                SensorManager.AXIS_Z, mRotation);
        SensorManager.getOrientation(mRotation, mOrientation);
        x = round(nextStepByWindowX(round((float) Math.toDegrees(mOrientation[1]))));
        y = (float) (-nextStepByWindowY((float) (round(((float) Math.toDegrees(mOrientation[2])) + angleOrientation) / 180.0f * Math.PI)));
        z = round(nextStepByWindowZ(mod((float) Math.toDegrees(mOrientation[0]) + 360.0f, 360.0f))) - dZ;
        Log.d("NAV111", "z= " + z);
        //z = mod((float) Math.toDegrees(mOrientation[0]) + 360.0f, 360.0f) - dZ;
    }

    public static float mod(float a, float b) {
        return (a % b + b) % b;
    }

    public static float round(float x) {
        float a = x;
        double temp = Math.pow(10, 4);
        a *= temp;
        a = Math.round(a);
        return (a / (float) temp);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d("NAV111A", "accuracy = " + accuracy);
    }

    public static boolean isCompass() {
        return mIsRotationAndCompas;
    }

    public static boolean dragRadarPins(MotionEvent event) {
        boolean result = true;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // gets called
            {
                mDragX1 = event.getX();
                mDragX2 = event.getX();
                break;
            }
            case MotionEvent.ACTION_MOVE: // doesnt seem to do anything
            {
                mDragX2 = event.getX();

                break;
            }
            case MotionEvent.ACTION_UP: // doesnt seem to do anything
            {
                mCurrentDragX += (-mDragX2 + mDragX1) / mWidth * Navigator.CAMERA_ANGLE_OF_VISION;
                if (mCurrentDragX >= 360) mCurrentDragX -= 360;
                if (mCurrentDragX < 0) mCurrentDragX += 360;
                mDragX1 = 0;
                mDragX2 = 0;
                result = false;
                break;
            }
        }
        return result;
    }

    public static float getCurrentDragX() {
        return mCurrentDragX + (-mDragX2 + mDragX1) / mWidth * Navigator.CAMERA_ANGLE_OF_VISION;
    }
}
