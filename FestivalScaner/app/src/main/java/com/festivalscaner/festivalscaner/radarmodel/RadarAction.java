package com.festivalscaner.festivalscaner.radarmodel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrey on 20.06.2015.
 */
public class RadarAction implements Serializable {

    public enum RadarActionType {
        onError, onSheAgreedToMeet, onSheRejectedMeeting, onSheRejectedInvitation, onTimeoutOfInviting, onSheInviteYou,
        onNearbyUsersSuccess, onLetMeetSuccess, onRejectMeetingSuccess, onRejectInvitationSuccess, onYouAgreedToMeetSuccess, onChangeAppointmentSuccess
    }

    public RadarActionType actionType;
    public Object dataObject;
    public final static Map<RadarActionType, Integer> PTOIORITET_MAP;

    static {
        PTOIORITET_MAP = createPROIORITET_MAP();
    }

    public RadarAction(RadarActionType type, Object dObject){
        actionType = type;
        dataObject = dObject;
    }

    private static Map<RadarActionType, Integer> createPROIORITET_MAP() {
        Map<RadarActionType, Integer> map = new HashMap<RadarActionType, Integer>();
        map.put(RadarActionType.onNearbyUsersSuccess, -1);
        map.put(RadarActionType.onError, 0);

        map.put(RadarActionType.onLetMeetSuccess, 1);

        map.put(RadarActionType.onYouAgreedToMeetSuccess, 2);
        map.put(RadarActionType.onChangeAppointmentSuccess, 0);

        map.put(RadarActionType.onSheInviteYou, 10);
        map.put(RadarActionType.onSheAgreedToMeet, 20);

        map.put(RadarActionType.onSheRejectedMeeting, 30);
        map.put(RadarActionType.onSheRejectedInvitation, 40);
        map.put(RadarActionType.onTimeoutOfInviting, 50);

        map.put(RadarActionType.onRejectMeetingSuccess, 60);
        map.put(RadarActionType.onRejectInvitationSuccess, 70);

        return map;
    }

    static public boolean isSecondPriorLess(RadarActionType first, RadarActionType second){
        return PTOIORITET_MAP.get(first) > PTOIORITET_MAP.get(second);
    }
}
