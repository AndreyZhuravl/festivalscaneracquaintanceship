package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.location.Location;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.festivalscaner.festivalscaner.ManagerPool;
import com.festivalscaner.festivalscaner.R;
import com.festivalscaner.festivalscaner.RadarSurfaceView;
import com.festivalscaner.festivalscaner.fsm.BusinessObject;
import com.festivalscaner.festivalscaner.fsm.RadarFSMContext;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSAnimationLodingState;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSChoosePlaceState;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSEmptyRadarState;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSRadarState;
import com.festivalscaner.festivalscaner.fsm.RadarFSMSSobelEffectState;
import com.festivalscaner.festivalscaner.network.CommandErrorInfo;
import com.festivalscaner.festivalscaner.network.INetworkManager;
import com.festivalscaner.festivalscaner.network.JsonParser;
import com.festivalscaner.festivalscaner.network.Nearby;
import com.festivalscaner.festivalscaner.viewcontroller.RadarActivity;
import com.festivalscaner.festivalscaner.viewcontroller.RadarView;
import com.festivalscaner.festivalscaner.viewcontroller.openglcam.CameraGLSurfaceView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RadarModel implements IRadarModel {
    public static final int FEED_BACK_VIBRATE_MILLISECONDS = 30;
    public static final String TIME_LAST_REQUEST_NEARBY = "TIME_LAST_REQUEST_NEARBY";
    public static final int PERIOD_UPDATE_NEARBY_MILLIS = 1000 * 1 * 1;
    private static final String TIME_LAST_RECALCULATE = "TIME_LAST_REQUEST_LOCATION";
    public static final int PERIOD_UPDATE_RECALCULATEMILLIS = 1000 * 1 * 1;
    public static int MAX_RADIUS_INT = 52500;
    private final Context mContext;
    private final Bitmap mIsMeetingPinBitmap;
    private final Bitmap mRejectedPinBitmap;
    //public static int ANGLE_OF_VISION = 70;
    public Paint paint;
    public Paint paintShadow;
    public Paint paintStroke;
    private Paint paintStrokeNet;
    public Paint paintWhiteShadow;
    public Paint paintPincShadow;
    public Paint paintAnimation;

    //  CopyOnWriteArrayList<UserData> mGostCoordinates;
    private ArrayList<UserData> mUserPolarCoordinates;
    private ArrayList<UserData> mIcStatusUserPolarCoordinates;

    //public static int CAMERA_ANGLE_OF_VISION = 70;
    private int mWidth;
    private int mHeight;
    private CopyOnWriteArrayList<UserData> mVisibleUsersList;
    private Navigator mNavigator;

    private float mUpdateButtonSize;
    private RectF mUpdateButtonRect;
    private float yAnimating;
    private boolean isAnimationLoadingRun = false;
    private MeetingPlaces mMeetingPlaces;
    private Paint paintBlackShadow;

    private float mCurrentZ, mCurrentY, mCurrentX;
    private float mDeltaAngle;

    private List<List<Nearby>> mNearbyList;
    private Location mLastLocation;
    private float mAlfaPainter = 0f;
    private int mCurrentButch = 0;
    private Bitmap mDeafaultBitmap;
    private IBitmapLoader mBitmapLoader;
    private RadarFSMContext mRadarFSMContext;
    private CameraGLSurfaceView mCameraGLSurfaceView;
    private Bitmap mUpdateButtonIconBitmap;
    private float yAnimatingAngle;
    private Bitmap mLikePinBitmap;
    private Bitmap mWaitePinBitmap;
    private InformLayerDrawer mInformLayerDrawer;
    private int mMinRadarNet;
    private int mMaxRadarNet;
    private RectF mBackButtonRect;
    private Bitmap mBackButtonIconBitmap;
    private float mBackButtonSize;
    private RadarView mRadarView;
    private List<Map<String, String>> mAppointments;
    List<RadarAction> mActionStack;
    private RectF mSheInviteYouButtonRect;
    private RectF mSheAgreedToMeetButtonRect;
    private RectF mSheRejectInvitationButtonRect;
    private RectF mSheRejectedMeetingButtonRect;
    private RectF mTimeoutOfInvitingButtonRect;
    private FrameLayout preview;
    private RadarActivity activity;

    public RadarModel(Context context) {
        mContext = context;
        initNearbyListener();
        mNavigator = new Navigator(mContext);
        initPaints();
        mVisibleUsersList = new CopyOnWriteArrayList<>();
        initStateMachine();
        mUpdateButtonSize = mContext.getResources().getDimension(R.dimen.radar_update_button_size);
        mBackButtonSize = mContext.getResources().getDimension(R.dimen.radar_update_button_size) / 2;
        mCurrentZ = 0;
        mCurrentX = 0;
        mCurrentY = 0;
        mDeltaAngle = 5.0f;
        mDeafaultBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ghost_icon);
        mLikePinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_like_pin), mUpdateButtonSize / 2, mUpdateButtonSize / 2, mContext);
        mWaitePinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_wating_pin), mUpdateButtonSize / 2, mUpdateButtonSize / 2, mContext);
        mIsMeetingPinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_like_pin_blue), mUpdateButtonSize / 2, mUpdateButtonSize / 2, mContext);
        mRejectedPinBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.status_like_pin_green), mUpdateButtonSize / 2, mUpdateButtonSize / 2, mContext);
        mBitmapLoader = new BitmapLoader();
        mIcStatusUserPolarCoordinates = new ArrayList<UserData>();
        mActionStack = new ArrayList<RadarAction>();
    }

    private void initNearbyListener() {
        ManagerPool managerPool = ManagerPool.getInstance();
        INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
        networkManager.setNearbyUsersListener(this);
    }

    public void initStateMachine() {
        setRadarFSMContext(new RadarFSMContext(new BusinessObject(this)));
        mMeetingPlaces = new MeetingPlaces(mContext, this);
        mInformLayerDrawer = new InformLayerDrawer(mContext, this);
    }

    private void initPaints() {
        paint = new Paint();
        paint.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
        paint.setAntiAlias(true);
        paint.setTextSize(24);

        paintShadow = new Paint();
        paintShadow.setColor(Color.argb(0x80, 0xff, 0xff, 0xff));
        paintShadow.setAntiAlias(true);
        paintStroke = new Paint();
        paintStroke.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setAntiAlias(true);
        paintStroke.setStrokeWidth(4);

        paintStrokeNet = new Paint();
        paintStrokeNet.setColor(Color.argb(0x80, 0xf0, 0xf0, 0xf0));
        paintStrokeNet.setStyle(Paint.Style.STROKE);
        paintStrokeNet.setAntiAlias(true);
        paintStrokeNet.setStrokeWidth(2);

        paintWhiteShadow = new Paint();
        paintShadow.setColor(Color.argb(0x30, 0xff, 0xff, 0xff));

        paintPincShadow = new Paint();
        paintPincShadow.setColor(Color.argb(0x20, 0xff, 0x0, 0x0));

        paintBlackShadow = new Paint();
        paintBlackShadow.setColor(Color.argb(0xff, 0x0, 0x0, 0x0));

        paintAnimation = new Paint();
        paintAnimation.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
    }


    @Override
    public void registerSensorListeners() {
        mNavigator.registerSensorListeners();
    }

    @Override
    public void unregisterSensorListeners() {
        mNavigator.unregisterSensorListeners();
    }

    public boolean isNeededClearScreen = true;

    @Override
    public void onDraw(Canvas canvas) {
        if (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSRadarState) {
            onDrawRadar(canvas);
        } else if (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSChoosePlaceState) {
            mMeetingPlaces.draw(canvas);
        }
        //mRadarView.postInvalidate();
    }

    @Override
    public void setCameraView(CameraGLSurfaceView cameraView) {
        mCameraGLSurfaceView = cameraView;
    }

    @Override
    public boolean isCurrentMeeting() {
        return mInformLayerDrawer.getCurrentUser().getmStatusPin() == UserData.StatusPin.MEETING;
    }

    @Override
    public void updateFSM() {
        mCurrentButch = 0;
    }

    @Override
    public void switchResumePauseStateFSMR(boolean isResume) {
        Log.d("444", "switchResumePauseStateFSMR getRadarFSMContext() = " + getRadarFSMContext().getStateCurrent().getName());
        if (isResume) {
            setRadarFSMContext(new RadarFSMContext(new BusinessObject(this)));
            getRadarFSMContext().EStart();
            getRadarFSMContext().TrESActiveSRadar();
            getRadarFSMContext().TrESEmptyRadar();
            //getRadarFSMContext().TrESActiveSRadar();
            //getRadarFSMContext().TrEEndSobel()
            Log.d("444", "switchResumePauseStateFSMR getRadarFSMContext() TrESActiveSRadar = " + getRadarFSMContext().getStateCurrent().getName());
            // getRadarFSMContext().TrEEndSobel();
            Log.d("444", "switchResumePauseStateFSMR getRadarFSMContext() TrEEndSobel = " + getRadarFSMContext().getStateCurrent().getName());
        } else {
            getRadarFSMContext().EExit();
        }
    }

    @Override
    public void onDrawRadar(Canvas canvas) {
        //shadowDraw(canvas);
        // clearIfNeeded(canvas);

        updateRadar();
        drawNet(canvas);
        mVisibleUsersList.clear();

        mCurrentZ = getZ1() + Navigator.CAMERA_ANGLE_OF_VISION;
        if (mCurrentZ >= 360) mCurrentZ -= 360;

        if (!(getRadarFSMContext().getStateCurrent() instanceof RadarFSMSSobelEffectState)) {
            synchronized (mUserPolarCoordinates) {
                synchronized (mIcStatusUserPolarCoordinates) {
                    for (UserData point : getUserPolarCoordinates()) {
                        if (point.getStatusUser() != UserData.StatusUser.invisible) {
                            drawPoint2D(canvas, point);
                        }
                    }
                    for (UserData point : getIcStatusUserPolarCoordinates()) {
                        if (point.getStatusUser() != UserData.StatusUser.invisible) {
                            drawPoint2D(canvas, point);
                        }
                    }
                }
            }
        }
        if (!(getRadarFSMContext().getStateCurrent() instanceof RadarFSMSEmptyRadarState) && !(getRadarFSMContext().getStateCurrent() instanceof RadarFSMSAnimationLodingState)) {
            mInformLayerDrawer.draw(canvas);
        } else {
            drawUpdateButton(canvas);
        }
        drawBackButton(canvas);
        drawTestButton(canvas);
    }

    private void drawNet(Canvas canvas) {
        float r100 = mHeight * 1.3f;
        float r300 = mHeight * 1.7f;
        float r500 = mHeight * 2.0f;
        float yCenter = mHeight * 2.1f;

        //canvas.drawText("100m", 10, yCenter - r100, paint);
        //Рисуем также, только конвертируем в битмап
        Bitmap bitmap = ModelUtils.textAsBitmap("" + mMinRadarNet + "m", paint);
        canvas.drawBitmap(bitmap, 10, yCenter - r100 - bitmap.getHeight(), null);
        Bitmap bitmap1 = ModelUtils.textAsBitmap("" + (mMinRadarNet + mMaxRadarNet) / 2 + "m", paint);
        canvas.drawBitmap(bitmap1, 10, yCenter - r300 - bitmap1.getHeight(), null);
        Bitmap bitmap2 = ModelUtils.textAsBitmap("" + mMaxRadarNet + "m", paint);
        canvas.drawBitmap(bitmap2, 10, yCenter - r500 - bitmap2.getHeight(), null);
        canvas.drawCircle(mWidth / 2, yCenter, r100, paintStrokeNet);
        canvas.drawCircle(mWidth / 2, yCenter, r300, paintStrokeNet);
        canvas.drawCircle(mWidth / 2, yCenter, r500, paintStrokeNet);
    }

    private void drawUpdateButton(Canvas canvas) {
        if (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSAnimationLodingState) {

            Matrix matrix = new Matrix();
            matrix.postRotate((float) (yAnimatingAngle * 360));

            Bitmap icon = Bitmap.createBitmap(mUpdateButtonIconBitmap, 0, 0, mUpdateButtonIconBitmap.getWidth(), mUpdateButtonIconBitmap.getWidth(), matrix, true);
            canvas.drawBitmap(icon, mWidth / 2 - icon.getWidth() / 2, mHeight - mInformLayerDrawer.mPudding - icon.getWidth() / 2 - mUpdateButtonIconBitmap.getWidth() / 2, paint);
        } else {
            canvas.drawBitmap(mUpdateButtonIconBitmap, mUpdateButtonRect.left, mUpdateButtonRect.top, paint);
        }
    }

    private void drawBackButton(Canvas canvas) {
        canvas.drawBitmap(mBackButtonIconBitmap, mBackButtonRect.left, mBackButtonRect.top, paint);
    }

    private void drawTestButton(Canvas canvas) {

        canvas.drawBitmap(mBackButtonIconBitmap, mSheInviteYouButtonRect.left, mSheInviteYouButtonRect.top, paint);
        Bitmap bitmap = ModelUtils.textAsBitmap("SheInviteYou", paint);
        canvas.drawBitmap(bitmap, mSheInviteYouButtonRect.left + mBackButtonIconBitmap.getWidth(), mSheInviteYouButtonRect.top , null);

        canvas.drawBitmap(mBackButtonIconBitmap, mSheAgreedToMeetButtonRect.left, mSheAgreedToMeetButtonRect.top, paint);
        bitmap = ModelUtils.textAsBitmap("SheAgreedToMeet", paint);
        canvas.drawBitmap(bitmap, mSheAgreedToMeetButtonRect.left + mBackButtonIconBitmap.getWidth(), mSheAgreedToMeetButtonRect.top , null);

        canvas.drawBitmap(mBackButtonIconBitmap, mSheRejectInvitationButtonRect.left, mSheRejectInvitationButtonRect.top, paint);
        bitmap = ModelUtils.textAsBitmap("SheRejectInvitation", paint);
        canvas.drawBitmap(bitmap, mSheRejectInvitationButtonRect.left + mBackButtonIconBitmap.getWidth(), mSheRejectInvitationButtonRect.top , null);

        canvas.drawBitmap(mBackButtonIconBitmap, mSheRejectedMeetingButtonRect.left, mSheRejectedMeetingButtonRect.top, paint);
        bitmap = ModelUtils.textAsBitmap("heRejectedMeeting", paint);
        canvas.drawBitmap(bitmap, mSheRejectedMeetingButtonRect.left + mBackButtonIconBitmap.getWidth(), mSheRejectedMeetingButtonRect.top , null);

        canvas.drawBitmap(mBackButtonIconBitmap, mTimeoutOfInvitingButtonRect.left, mTimeoutOfInvitingButtonRect.top, paint);
        bitmap = ModelUtils.textAsBitmap("TimeoutOfInviting", paint);
        canvas.drawBitmap(bitmap, mTimeoutOfInvitingButtonRect.left + mBackButtonIconBitmap.getWidth(), mTimeoutOfInvitingButtonRect.top , null);
    }


    public void drawPoint2D(Canvas canvas, UserData point) {
        float pointX = point.getPoint().x;
        float pointY = point.getPoint().y;

        float delta = Math.abs(pointY - getZ1());
        if (!Navigator.isCompass()) {
            float tempAngel = Navigator.getCurrentDragX() - Navigator.CAMERA_ANGLE_OF_VISION;
            if (tempAngel >= 360) tempAngel -= 360;
            if (tempAngel < 0) tempAngel += 360;
            delta = Math.abs(tempAngel - pointY);
        }
        if ((delta < Navigator.CAMERA_ANGLE_OF_VISION / 2 + mDeltaAngle) || (delta > (360 - Navigator.CAMERA_ANGLE_OF_VISION / 2 - mDeltaAngle))) {
            float z = mCurrentZ;


            int xIcon = 0;
            int yIcon = 0;

            float r = 1 + pointX / mMaxRadarNet;
//            float sizeIcMax = (mContext.getResources().getIntArray(R.array.base_max_pin_icon_size)[0]);
//            float sizeIcMin = (mContext.getResources().getIntArray(R.array.base_min_pin_icon_size)[0]);
//            point.iconSize = (int) (sizeIcMin + (mMaxRadarNet - pointX) * (sizeIcMax - sizeIcMin) / (mMaxRadarNet - mMinRadarNet));
            float transleteZAngleToWidth = 0;

            float tempPointY = pointY + Navigator.CAMERA_ANGLE_OF_VISION;
            if (tempPointY >= 360) tempPointY -= 360;

            float dAngle = 0;
            dAngle = z - tempPointY;
            if (!Navigator.isCompass()) {
                dAngle = Navigator.getCurrentDragX() - tempPointY;
            }
            if (dAngle > Navigator.CAMERA_ANGLE_OF_VISION / 2) {
                dAngle -= 360;
            } else if (dAngle < -Navigator.CAMERA_ANGLE_OF_VISION / 2) {
                dAngle += 360;
            }
            transleteZAngleToWidth = (dAngle)
                    / Navigator.CAMERA_ANGLE_OF_VISION * mWidth;

            xIcon = (int) (mWidth / 2 - transleteZAngleToWidth);

            float transleteXAngleToHeight = (getX1()) * mHeight / 100;
            float levelYOfDistance = (pointX - (mMaxRadarNet + mMinRadarNet) / 2) / mMaxRadarNet * mHeight * 0.3f;
            float yIconTemp = mHeight * 2 / 3 - transleteXAngleToHeight - levelYOfDistance;
            if (!Navigator.isCompass()) {
                yIconTemp = mHeight / 2 - levelYOfDistance;
            }
            if (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSAnimationLodingState) {

                yIcon = (int) (yIconTemp * yAnimating - point.iconSize * (1 - yAnimating) + yIconTemp * (1 - r) * (1 - yAnimating) / 2);
                int alfa = (int) (255.0f * mAlfaPainter);
                alfa = alfa > 255 ? 255 : alfa;
                paint.setAlpha(alfa);
                paintShadow.setAlpha(alfa);
                paintStroke.setAlpha(alfa);
            } else {
                yIcon = (int) (yIconTemp);
            }

            Bitmap bitmapUserIcon = ModelUtils.getResizedBitmap(point.getUserIcon(), point.iconSize, point.iconSize, mContext);

            float semiWidthX = xIcon - mWidth / 2;
            float semiHeightY = yIcon - mHeight / 2;
            float X = (float) ((float) semiWidthX * Math.cos(getY1()) - (float) semiHeightY * Math.sin(getY1()));
            float Y = (float) ((float) semiWidthX * Math.sin(getY1()) + (float) semiHeightY * Math.cos(getY1()));
            xIcon = (int) X + mWidth / 2;
            yIcon = (int) Y + mHeight / 2;

            point.setDrawPinXYSize(xIcon, yIcon, point.iconSize);

            if (mInformLayerDrawer.getCurrentUserID() == point.getID()) {
                canvas.drawCircle(xIcon, yIcon, point.iconSize / 2, paint);
                canvas.drawBitmap(point.bitmapResize, xIcon - point.iconSize / 2, yIcon - point.iconSize / 2, paintShadow);
            } else {
                canvas.drawBitmap(point.bitmapResize, xIcon - point.iconSize / 2, yIcon - point.iconSize / 2, paint);
            }
            canvas.drawCircle(xIcon, yIcon, point.iconSize / 2, paintStroke);
            if ((point.getStatusUser() == UserData.StatusUser.inviter) || (point.getStatusUser() == UserData.StatusUser.invited)) {
                canvas.drawBitmap(mWaitePinBitmap, xIcon + point.iconSize * 0.25f, yIcon - point.iconSize * 0.45f, paintStroke);
            }
            if (point.getStatusUser() == UserData.StatusUser.appointment) {
                canvas.drawBitmap(mLikePinBitmap, xIcon + point.iconSize * 0.25f, yIcon - point.iconSize * 0.45f, paintStroke);
            }

            mVisibleUsersList.add(point);
        }
    }

    @Override
    public float getX1() {
        return mNavigator.x;
    }

    @Override
    public float getY1() {
        return mNavigator.y;
    }

    @Override
    public float getZ1() {
        return mNavigator.z;
    }

    @Override
    public ArrayList<UserData> getUserPolarCoordinates() {
        return mUserPolarCoordinates;
    }

    public ArrayList<UserData> getIcStatusUserPolarCoordinates() {
        return mIcStatusUserPolarCoordinates;
    }

    @Override
    public void setUserPolarCoordinates(ArrayList<UserData> userPolarCoordinates) {
        this.mUserPolarCoordinates = userPolarCoordinates;
    }

    @Override
    public void setWidthHeight(int mWidth, int mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        mMeetingPlaces.setWidthHeight(mWidth, mHeight);
        mInformLayerDrawer.setWidthHeight(mWidth, mHeight);
        mUpdateButtonRect = new RectF(mWidth / 2 - mUpdateButtonSize / 2, mHeight - mInformLayerDrawer.mPudding - mUpdateButtonSize,
                mWidth / 2 + mUpdateButtonSize / 2, mHeight - mInformLayerDrawer.mPudding);
        mBackButtonRect = new RectF(mInformLayerDrawer.mPudding, mInformLayerDrawer.mPudding,
                mInformLayerDrawer.mPudding + mBackButtonSize, mInformLayerDrawer.mPudding + mBackButtonSize);
        mBackButtonIconBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(
                mContext.getResources(), R.drawable.top_bar_back_button), mBackButtonSize, mBackButtonSize, mContext);


        float dalta = 60;
        float dalta1 = 60;
        mSheInviteYouButtonRect = new RectF(mInformLayerDrawer.mPudding, mInformLayerDrawer.mPudding + dalta,
                mInformLayerDrawer.mPudding + mBackButtonSize, mInformLayerDrawer.mPudding + mBackButtonSize + dalta);
        dalta += dalta1;
        mSheAgreedToMeetButtonRect = new RectF(mInformLayerDrawer.mPudding, mInformLayerDrawer.mPudding + dalta,
                mInformLayerDrawer.mPudding + mBackButtonSize, mInformLayerDrawer.mPudding + mBackButtonSize + dalta);
        dalta += dalta1;
        mSheRejectInvitationButtonRect = new RectF(mInformLayerDrawer.mPudding, mInformLayerDrawer.mPudding + dalta,
                mInformLayerDrawer.mPudding + mBackButtonSize, mInformLayerDrawer.mPudding + mBackButtonSize + dalta);
        dalta += dalta1;
        mSheRejectedMeetingButtonRect = new RectF(mInformLayerDrawer.mPudding, mInformLayerDrawer.mPudding + dalta,
                mInformLayerDrawer.mPudding + mBackButtonSize, mInformLayerDrawer.mPudding + mBackButtonSize + dalta);
        dalta += dalta1;
        mTimeoutOfInvitingButtonRect = new RectF(mInformLayerDrawer.mPudding, mInformLayerDrawer.mPudding + dalta,
                mInformLayerDrawer.mPudding + mBackButtonSize, mInformLayerDrawer.mPudding + mBackButtonSize + dalta);


        mUpdateButtonIconBitmap = ModelUtils.getResizedBitmap(BitmapFactory.decodeResource(
                mContext.getResources(), R.drawable.radar_update_button), mUpdateButtonSize, mUpdateButtonSize, mContext);
        mNavigator.setWidth(mWidth);
    }


    @Override
    public List<UserData> getVisibleUsersList() {
        return mVisibleUsersList;
    }


    @Override
    public boolean onTouch(MotionEvent event) {
        boolean result = true;
        Vibrator vibro = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);

        if (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSChoosePlaceState) {
            result = mMeetingPlaces.onTouch(event);
        } else if (mInformLayerDrawer.onTouch(event)) {
            result = false;
        } else {
            if (mUpdateButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                if (!(getRadarFSMContext().getStateCurrent() instanceof RadarFSMSAnimationLodingState) && !isAnimationLoadingRun) {
                    mNavigator.unregisterSensorListeners();
                    getRadarFSMContext().TrEUpdateUsersShowList();
                    requestNextNearby();
                    result = false;
                }
            } else if (mSheAgreedToMeetButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                onSheAgreedToMeet("");
                result = false;
            }
            else if (mSheRejectInvitationButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                onSheRejectedInvitation("");
                result = false;
            }
            else if (mSheRejectedMeetingButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                onSheRejectedMeeting("");
                result = false;
            }
            else if (mTimeoutOfInvitingButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                onTimeoutOfInviting("");
                result = false;
            }
            else if (mSheInviteYouButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                onSheInviteYou("");
                result = false;
            }
            else if (mBackButtonRect.contains(event.getX(), event.getY())) {
                vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                ((RadarActivity) mContext).finish();
                result = false;
            }else {
                for (UserData point : getVisibleUsersList()) {
                    RectF rectOfPin = new RectF(point.getDrawPoint().x - point.getIconSize() / 2, point.getDrawPoint().y - point.getIconSize() / 2,
                            point.getDrawPoint().x + point.getIconSize() / 2, point.getDrawPoint().y + point.getIconSize() / 2);

                    if (rectOfPin.contains(event.getX(), event.getY())) {
                        vibro.vibrate(FEED_BACK_VIBRATE_MILLISECONDS);
                        mInformLayerDrawer.setCurrentUserID(point);
                        mMeetingPlaces.setCurrentUserID(point);
                        switch (point.getStatusUser()) {
                            case appointment:
                                getRadarFSMContext().TrEDetailedUser();
                                break;
                            case normal:
                                getRadarFSMContext().TrEDetailedUser();
                                break;
                            case invited:
                                getRadarFSMContext().TrESWaitAnswer();
                                break;
                            case inviter:
                                getRadarFSMContext().TrESheAgreedMeet();
                                break;
                        }
                        result = false;
                        break;
                    }
                }
                if (result && !Navigator.isCompass()) {
                    result = Navigator.dragRadarPins(event);
                }
            }
        }
        return result;
    }

    @Override
    public void updateRadar() {
        if ((getRadarFSMContext().getStateCurrent() instanceof RadarFSMSAnimationLodingState) && !isAnimationLoadingRun) {

            isAnimationLoadingRun = true;
            Executor executor = Executors.newFixedThreadPool(1);
            executor.execute(new Runnable() {
                float timeAnimationMillisec = 1000.0f;
                float periodAnimationMillisec = 40.0f;

                @Override
                public void run() {
                    if (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSSobelEffectState) {
                        mCameraGLSurfaceView.effectSetOnOff(1);
                        try {
                            while (getRadarFSMContext().getStateCurrent() instanceof RadarFSMSSobelEffectState)
                                Thread.sleep((long) 1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                       mCameraGLSurfaceView.effectSetOnOff(-1);
                    }
                    yAnimating = 0f;
                    yAnimatingAngle = 0f;
                    mAlfaPainter = 0f;
                    float countFreq = timeAnimationMillisec / periodAnimationMillisec;
                    float dTime = (float) (3 * Math.PI / countFreq);
                    float currentTime = 0;
                    while (currentTime < 3 * Math.PI) {
                        yAnimating = 1 - (float) (Math.exp(-currentTime / 5) * (Math.cos(currentTime)));
                        yAnimatingAngle = (float) (currentTime / (3.0f * Math.PI));
                        mAlfaPainter = (float) (currentTime / (3.0f * Math.PI) + 2 / countFreq);
                        currentTime += dTime;
                        try {
                            Thread.sleep((long) periodAnimationMillisec);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    mNavigator.registerSensorListeners();
                    getRadarFSMContext().TrE1500MillisecEnd();
                    isAnimationLoadingRun = false;
                }
            });
        }

    }

    @Override
    public void requestNextNearby() {
        if ((mNearbyList != null) && (mNearbyList.size() > mCurrentButch + 1)) {
            Log.d("444", "RadarModelStart setVisibleNextBatch");
            setVisibleNextBatch();
        } else {
            Log.d("444", "RadarModelStart requestNearby");
            requestNearby();
        }
    }

    private void requestNearby() {
        mCurrentButch = 0;
        if (mLastLocation != null) {
            if (isTimeRequestNearby()) {
                UUID uuid = UUID.fromString("a49306d7-ff7c-402c-b7e4-de769e3659ce");
                HashMap<String, Float> location = new HashMap<>();
                location.put("currentLatitude", new Float(mLastLocation.getLatitude()));
                location.put("currentLongitude", new Float(mLastLocation.getLongitude()));
                ManagerPool managerPool = ManagerPool.getInstance();
                INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
                networkManager.getNearbyUsersAsync(uuid, 20, 3, location);
            } else {
                getRadarFSMContext().TrEEndSobel();
            }
        } else {
            getRadarFSMContext().TrEEndSobel();
        }
    }

    private void setVisibleNextBatch() {
        mCurrentButch++;
        recalculateXYPoints();
        reputStatusPinPolarPoints();
        getRadarFSMContext().TrEEndSobel();
    }

    @Override
    public void setOreintation(int degrees) {
        mNavigator.angleOrientation = degrees;
    }

    private boolean isTimeRequestNearby() {
        boolean result = false;
        long time = mContext.getSharedPreferences(mContext.getApplicationInfo().name, Context.MODE_PRIVATE).getLong(TIME_LAST_REQUEST_NEARBY, 0);
        if (System.currentTimeMillis() - time > PERIOD_UPDATE_NEARBY_MILLIS) {
            result = true;
        }
        return result;
    }

    @Override
    public void updateLastLocation(Location lastLocation) {
        if (lastLocation != mLastLocation) {
            mLastLocation = lastLocation;
            tryRecalculateXYPoints(false);
        }
    }

    private void tryRecalculateXYPoints(boolean forceRecalculate) {
        long lastRecalculateXYPonts = mContext.getSharedPreferences(mContext.getApplicationInfo().name, Context.MODE_PRIVATE).getLong(TIME_LAST_RECALCULATE, 0);
        if (forceRecalculate || (System.currentTimeMillis() - lastRecalculateXYPonts > PERIOD_UPDATE_RECALCULATEMILLIS)) {
            mContext.getSharedPreferences(mContext.getApplicationInfo().name, Context.MODE_PRIVATE).edit().putLong(TIME_LAST_RECALCULATE, System.currentTimeMillis());
            recalculateXYPoints();
        }
    }

    private void recalculateXYPoints() {
        mUserPolarCoordinates = new ArrayList<UserData>();
        if (mNearbyList != null) {
            CopyOnWriteArrayList<UserData> mGostCoordinates = ModelUtils.convertCoordinatesToLocal(mNearbyList.get(mCurrentButch), mLastLocation, mBitmapLoader, mDeafaultBitmap, mContext);
            mUserPolarCoordinates = ModelUtils.convertCoordinatesToPolar(mGostCoordinates, mContext);
            ModelUtils.sortPolarByAngle(mUserPolarCoordinates);
        }
    }

    private void reputStatusPinPolarPoints() {
        Log.d("Reput444", "ReputStatusPinPolarPoints1 mUserPolarCoordinates.size         = " + mUserPolarCoordinates.size());
        Log.d("Reput444", "ReputStatusPinPolarPoints1 mIcStatusUserPolarCoordinates.size = " + mIcStatusUserPolarCoordinates.size());
        synchronized (mUserPolarCoordinates) {
            synchronized (mIcStatusUserPolarCoordinates) {
                Iterator<UserData> userIterator = mUserPolarCoordinates.iterator();
                while (userIterator.hasNext()) {
                    UserData user = userIterator.next();
                    if (user.getStatusUser() != UserData.StatusUser.normal) {
                        if (!containsUser(mIcStatusUserPolarCoordinates, user)) {
                            mIcStatusUserPolarCoordinates.add(user);
                            user.prepareBitmapForRadar(mContext);
                        }
                        userIterator.remove();
                    }
                }
                ModelUtils.sortPolarByAngle(mIcStatusUserPolarCoordinates);
            }
        }
        Log.d("Reput444", "ReputStatusPinPolarPoints2 mUserPolarCoordinates.size         = " + mUserPolarCoordinates.size());
        Log.d("Reput444", "ReputStatusPinPolarPoints2 mIcStatusUserPolarCoordinates.size = " + mIcStatusUserPolarCoordinates.size());
    }

    private boolean containsUser(ArrayList<UserData> userPolarCoordinates, UserData user) {
        boolean result = false;
        for (UserData userIco : userPolarCoordinates) {
            if (userIco.getID().toString().equals(user.getID().toString())) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public RadarFSMContext getRadarFSMContext() {
        if (mRadarFSMContext == null) {
            mRadarFSMContext = new RadarFSMContext(new BusinessObject(this));
        }
        return mRadarFSMContext;
    }

    @Override
    public void setRadarFSMContext(RadarFSMContext mRadarFSMContext) {
        this.mRadarFSMContext = mRadarFSMContext;
    }

    @Override
    public void onNearbyUsersStart() {
        Log.d("444", "RadarModelStart onNearbyUsersStart");
        // mRadarStateMashine.applyEvent(mRadarStateMashine.STopUserBarJustPic);
    }

    @Override
    public void onNearbyUsersSuccess(List<List<Nearby>> nearbyList) {
        onNearbyUsersSuccessAction(nearbyList);
    }

    private void onNearbyUsersSuccessAction(List<List<Nearby>> nearbyList) {
        Log.d("444", "RadarModelStart onNearbyUsersSuccess");
        mCurrentButch = 0;
        mContext.getSharedPreferences(mContext.getApplicationInfo().name, Context.MODE_PRIVATE).edit().putLong(TIME_LAST_REQUEST_NEARBY, System.currentTimeMillis());
        mNearbyList = nearbyList;
        mBitmapLoader.clearCache();
        Log.d("444", "RadarModelStart mNearbyList =" + mNearbyList.size());
        tryRecalculateXYPoints(true);
        reputStatusPinPolarPoints();
        if (!mMeetingPlaces.isMapLoaded()) {
            UUID uuid = UUID.fromString("a49306d7-ff7c-402c-b7e4-de769e3659ce");
            ManagerPool managerPool = ManagerPool.getInstance();
            INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
            networkManager.onGetPlacesIdAsync(uuid);
        } else {
            getRadarFSMContext().TrEEndSobel();
        }
    }

    @Override
    public void onNearbyUsersError(CommandErrorInfo errorInfo) {
        onNearbyUsersErrorAction(errorInfo);
    }

    private void onNearbyUsersErrorAction(CommandErrorInfo errorInfo) {
        getRadarFSMContext().TrEEndSobel();
        getRadarFSMContext().TrEToForbidTwoMeeting();
        mInformLayerDrawer.setSpecialTextOnForbidenMeeting("Ошибка Сервера", errorInfo.errorInfo);
    }

    @Override
    public void onLetMeet(UserData mCurrentUser, String placeUUID) {
        onLetMeetAction(mCurrentUser, placeUUID);
    }

    private void onLetMeetAction(UserData mCurrentUser, String placeUUID) {
        UUID uuid = UUID.fromString("a49306d7-ff7c-402c-b7e4-de769e3659ce");
        ManagerPool managerPool = ManagerPool.getInstance();
        INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
        Log.d("444", "onLetMeet uuid = " + uuid.toString());
        Log.d("444", "onLetMeet placeUUID = " + placeUUID);
        Log.d("444", "onLetMeet mCurrentUser.getID() = " + mCurrentUser.getID().toString());
        networkManager.onLetMeetAsync(uuid, mCurrentUser.getID(), UUID.fromString(placeUUID));
    }

    @Override
    public void youRejectInvitation(UUID idUser) {
        UUID sessionId = UUID.fromString("a49306d7-ff7c-402c-b7e4-de769e3659ce");
        ManagerPool managerPool = ManagerPool.getInstance();
        INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
        networkManager.onRejectMeetingAsync(sessionId, idUser, false);
    }


    @Override
    public void onLetMeetStart() {

    }

    @Override
    public void onLetMeetSuccess(Map<String, String> appointmentResult) {

        putActionInStack(new RadarAction(RadarAction.RadarActionType.onLetMeetSuccess, appointmentResult));
    }

    private void onLetMeetSuccessAction(Object appointmentResultObject) {
        Map<String, String> appointmentResult = (Map<String, String>) appointmentResultObject;
        synchronized (mUserPolarCoordinates) {
            synchronized (mIcStatusUserPolarCoordinates) {
                Iterator<UserData> iteratorUser = mUserPolarCoordinates.iterator();
                while (iteratorUser.hasNext()) {
                    UserData user = iteratorUser.next();
                    String appointUserId = appointmentResult.get(JsonParser.PARAM_userId);
                    if (user.getID().toString().equals(appointUserId)) {
                        user.setStatusUser(ModelUtils.translateUserStaus( appointmentResult.get(JsonParser.PARAM_status)));
                        addUserIfNeeded(mIcStatusUserPolarCoordinates, user);
                        iteratorUser.remove();

                        for (Nearby ner:mNearbyList.get(mCurrentButch)){
                            if (ner.getUserId().toString().equals(user.getID().toString())) {
                                mNearbyList.get(mCurrentButch).remove(ner);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        reputStatusPinPolarPoints();
        getRadarFSMContext().TrESWaitAnswer();
    }

    private void addUserIfNeeded(ArrayList<UserData> mIcStatusUserPolarCoordinates, UserData userForAdd) {
        boolean hasUser = false;
        Iterator<UserData> iteratorUser = mIcStatusUserPolarCoordinates.iterator();
        while (iteratorUser.hasNext()) {
            UserData user = iteratorUser.next();
            if (user.getID().toString().equals(userForAdd.getID().toString())) {
                hasUser = true;
                break;
            }
        }
        if (!hasUser) {
            mIcStatusUserPolarCoordinates.add(userForAdd);
        }
    }

    @Override
    public void onLetMeetError(CommandErrorInfo errorInfo) {
        onLetMeetErrorAction(errorInfo);
    }

    private void onLetMeetErrorAction(CommandErrorInfo errorInfo) {
        mInformLayerDrawer.setSpecialTextOnForbidenMeeting(errorInfo.errorInfo, "");
        getRadarFSMContext().TrEToForbidTwoMeeting();
    }

    @Override
    public void onRejectMeetingStart() {

    }

    @Override
    public void onRejectMeetingSuccess(UUID userIdRejected) {
        onRejectMeetingSuccessAction(userIdRejected);
    }

    private void onRejectMeetingSuccessAction(UUID userIdRejected) {
        Log.d("API111", "onRejectMeetingSuccess state = " + getRadarFSMContext().getStateCurrent().getName());
        UserData userCurren = null;
        for (UserData user : mUserPolarCoordinates) {

            if (user.getID().equals(userIdRejected)) {
                user.setStatusUser(UserData.StatusUser.invisible);
                mInformLayerDrawer.setCurrentUserID(user);
                getRadarFSMContext().TrEToRejected();
                break;
            }

        }
        for (UserData user : mIcStatusUserPolarCoordinates) {

            if (user.getID().equals(userIdRejected)) {
                user.setStatusUser(UserData.StatusUser.invisible);
                mInformLayerDrawer.setCurrentUserID(user);
                getRadarFSMContext().TrEToRejected();
                break;
            }

        }
    }

    @Override
    public void onRejectMeetingError(CommandErrorInfo errorInfo) {

    }

    @Override
    public void onYouAgreedToMeetStart() {

    }

    @Override
    public void onYouAgreedToMeetSuccess() {

    }

    @Override
    public void onYouAgreedToMeetError(CommandErrorInfo errorInfo) {

    }

    @Override
    public void onGetPlacesIdStart() {

    }

    @Override
    public void onGetPlacesIdSuccess(Map<String, Map<String, String>> mPlacesIdMap) {
        mMeetingPlaces.setMeetingPlaces(mPlacesIdMap);
        Map<String, String> mapLimits = mPlacesIdMap.get(JsonParser.MAP_LIMITS);
        mMinRadarNet = Integer.parseInt(mapLimits.get(JsonParser.PARAM_min));
        mMaxRadarNet = Integer.parseInt(mapLimits.get(JsonParser.PARAM_max));
        getRadarFSMContext().TrEEndSobel();
    }

    private void saveMap(Map<String, String> inputMap) {
        SharedPreferences pSharedPref = mContext.getSharedPreferences(mContext.getApplicationInfo().name, Context.MODE_PRIVATE);
        if (pSharedPref != null) {
            JSONObject jsonObject = new JSONObject(inputMap);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove("PLACES_MAP").commit();
            editor.putString("PLACES_MAP", jsonString);
            editor.commit();
        }
    }

    private Map<String, String> loadMap() {
        Map<String, String> outputMap = new HashMap<String, String>();
        SharedPreferences pSharedPref = mContext.getSharedPreferences(mContext.getApplicationInfo().name, Context.MODE_PRIVATE);
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString("PLACES_MAP", (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while (keysItr.hasNext()) {
                    String key = keysItr.next();
                    String value = (String) jsonObject.get(key);
                    outputMap.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }

    @Override
    public void onGetPlacesIdError(CommandErrorInfo errorInfo) {
    }

    @Override
    public void onRejectInvitationSuccess(UUID userIdRejected) {
        onRejectInvitationSuccessAction(userIdRejected);
    }

    private void onRejectInvitationSuccessAction(UUID userIdRejected) {
        for (UserData user : mUserPolarCoordinates) {

            if (user.getID().equals(userIdRejected)) {
                user.setStatusUser(UserData.StatusUser.invisible);
                mInformLayerDrawer.setCurrentUserID(user);
                getRadarFSMContext().TrERejectInvitation();
                break;
            }

        }
        for (UserData user : mIcStatusUserPolarCoordinates) {

            if (user.getID().equals(userIdRejected)) {
                user.setStatusUser(UserData.StatusUser.invisible);
                mInformLayerDrawer.setCurrentUserID(user);
                getRadarFSMContext().TrERejectInvitation();
                break;
            }

        }
    }

    @Override
    public void onChangeAppointmentStart() {

    }

    @Override
    public void onChangeAppointmentSuccess(Map<String, String> appointment) {

    }

    @Override
    public void onChangeAppointmentError(CommandErrorInfo errorInfo) {

    }

    @Override
    public void onAppointmentsStart() {
        Log.d("API111", "onAppointmentsStart");
    }

    @Override
    public void onAppointmentsSuccess(List<Map<String, String>> appointments) {
        Log.d("API111", "onAppointmentsSuccess1");
        mAppointments = appointments;
        Iterator<Map<String, String>> appointment = appointments.iterator();
        while (appointment.hasNext()) {
            Map<String, String> app = appointment.next();
            Log.d("API111", "onAppointmentsSuccess2 - " + app.get(JsonParser.PARAM_status));
            if (app.get(JsonParser.PARAM_status).equals(JsonParser.PARAM_accepted)) {

                for (UserData user : mIcStatusUserPolarCoordinates) {
                    Log.d("API111", "onAppointmentsSuccess3 user.getID() - " + user.getID().toString());
                    if (user.getID().toString().equals(app.get(JsonParser.PARAM_userId))) {
                        mMeetingPlaces.setCurrentUserID(user);
                        getRadarFSMContext().TrEDetailedUser();
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onAppointmentsError(CommandErrorInfo errorInfo) {
        Log.d("API111", "onAppointmentsError" + errorInfo.errorCode);
    }

    @Override
    public void youRejectMeeting(UUID idUser) {
        UUID sessionId = UUID.fromString("a49306d7-ff7c-402c-b7e4-de769e3659ce");
        ManagerPool managerPool = ManagerPool.getInstance();
        INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
        networkManager.onRejectMeetingAsync(sessionId, idUser, true);
    }



    @Override
    public void toTheCurrentMeeting() {
        UUID sessionId = UUID.fromString("a49306d7-ff7c-402c-b7e4-de769e3659ce");
        ManagerPool managerPool = ManagerPool.getInstance();
        INetworkManager networkManager = managerPool.getManager(INetworkManager.ID);
        networkManager.onAppointmentsAsync(sessionId, null, null);
    }

    @Override
    public void onSheAgreedToMeet(String jsonMessage) {
        jsonMessage = " {\"aps\": {\"alert\": {\"title\": \"Сабрина отменила встречу.\",\"body\": \"Вы все еще можете видеть друг друга на радаре.\"}},\"appointment\": {\"appointmentId\": \"7f76f4d2-06a0-11e5-82a6-01e861ea09be\",\"status\": \"made\",\"userId\": \"463ec690-069b-11e5-88c0-757bb664a5dc\",\"placeId\":\"90e0e65e-069d-11e5-8882-757bb664a5dc\"},\"target\": {\"userId\": \"463ec690-069b-11e5-88c0-757bb664a5dc\",\"name\": \"Валерия\",\"birthday\": \"1990-05-12T00:00:00+0000\",\"status\": \"invited\",\"appointmentId\": \"7f76f4d2-06a0-11e5-82a6-01e861ea09be\",\"photo\": {\"preview\": \"http://preview.jpg\",\"previewMedium\": \"http://previewMedium.jpg\",\"previewLarge\": \"http://previewLarge.jpg\"},\"location\": {\"latitude\": 59.842806,\"longitude\": 30.41288}},\"place\": {\"placeId\": \"90e0e65e-069d-11e5-8882-757bb664a5dc\",\"name\": \"B\",\"location\": {\"latitude\": 59.80185,\"longitude\": 30.39274}}}";
        try {
            Map<String, String>   map = JsonParser.parsePushMap(jsonMessage);
            Log.d("API222","map = " + map.toString());
        } catch (JSONException e) {
            Log.d("API222", "e = " + e.getMessage());
        }

    }

    @Override
    public void onSheRejectedMeeting(String jsonMessage) {

    }

    @Override
    public void onSheRejectedInvitation(String jsonMessage) {

    }

    @Override
    public void onTimeoutOfInviting(String jsonMessage) {

    }

    @Override
    public void onSheInviteYou(String jsonMessage) {

    }

    @Override
    public void nextStackAction() {
        if (mActionStack.size() > 0) {
            switch (mActionStack.get(0).actionType) {
                case onSheAgreedToMeet:
                    break;
                case onSheRejectedMeeting:
                    break;
                case onSheRejectedInvitation:
                    break;
                case onTimeoutOfInviting:
                    break;
                case onSheInviteYou:
                    break;
                case onNearbyUsersSuccess:
                    break;
                case onLetMeetSuccess:
                    onLetMeetSuccessAction(mActionStack.get(0).dataObject);
                    break;
                case onRejectMeetingSuccess:
                    break;
                case onRejectInvitationSuccess:
                    break;
                case onYouAgreedToMeetSuccess:
                    break;
                case onChangeAppointmentSuccess:
                    break;
                default:
                    break;
            }
            mActionStack.remove(0);
        }
    }


    @Override
    public void setCameraLayout(RadarActivity activity, FrameLayout preview) {
        this.preview = preview;
        this.activity = activity;
    }

    public void putActionInStack(RadarAction radarAction) {
        int place = 0;
        Iterator<RadarAction> iteratorAction = mActionStack.iterator();
        while(iteratorAction.hasNext()){
            RadarAction actionStack = iteratorAction.next();

            if(RadarAction.isSecondPriorLess(actionStack.actionType, radarAction.actionType)){
                break;
            }
            place++;
        }
        mActionStack.add(place, radarAction);
    }

    @Override
    public void setView(RadarView radarView) {
        mRadarView = radarView;
        mInformLayerDrawer.setView(radarView);
    }

    @Override
    public void setSurfaceView(RadarSurfaceView radarView) {


    }
}


