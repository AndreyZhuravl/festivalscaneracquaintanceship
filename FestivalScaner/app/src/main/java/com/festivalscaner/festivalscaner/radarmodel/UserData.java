package com.festivalscaner.festivalscaner.radarmodel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;

import com.festivalscaner.festivalscaner.R;

import java.util.UUID;

public class UserData {
    public Point2D mPoint;
    private Point mDrawPoint;
    private UUID mID;
    private String mUserName;
    private String mUserAge;
	public int iconSize;
    private Bitmap mUserIcon;
    public Bitmap bitmapResize;
    public float mMaxRadarNet;
    public float mMinRadarNet;

    public void prepareBitmapForRadar(Context context) {
        float sizeIcMax = (context.getResources().getIntArray(R.array.base_max_pin_icon_size)[0]);
        float sizeIcMin = (context.getResources().getIntArray(R.array.base_min_pin_icon_size)[0]);
        iconSize = (int) (sizeIcMin + (mMaxRadarNet - mPoint.x) * (sizeIcMax - sizeIcMin) / (mMaxRadarNet - mMinRadarNet));
        Bitmap bitmap= ModelUtils.getResizedBitmap(mUserIcon, iconSize, iconSize, context);
        bitmapResize = ModelUtils.getRoundedBitmap(bitmap);
    }

    public enum   StatusPin  {
        AGREED_TO_MEET,  DISLIKE , YOU_ARE_WAITING, EMPTY, YOU_ARE_INVITED, SHE_REJECT, MEETING
    }

    public enum   StatusUser  {
        normal,  invited , inviter, appointment, invisible
    }

    public StatusPin getmStatusPin() {
        return mStatusPin;
    }

    public void setStatusPin(StatusUser mStatusPin) {
        this.mStatusUser = mStatusPin;
    }

    private StatusPin mStatusPin;

    private StatusUser mStatusUser;

    public UserData(Point2D point, UUID ID, String userName, String userAge, Bitmap userIcon, StatusUser statusUser) {
        super();
        this.mPoint = point;
        this.mID = ID;
        this.mUserName = userName;
        this.mUserAge = userAge;
        this.mUserIcon = userIcon;
        this.mStatusUser = statusUser;
        mDrawPoint = new Point();
        mStatusPin = StatusPin.EMPTY;
        mMinRadarNet = 0;
        mMaxRadarNet = 2500;

    }

	public Point2D getPoint() {
		return mPoint;
	}

	public void setPoint(Point2D point) {
		this.mPoint = point;
	}

	public UUID getID() {
		return mID;
	}

	public void setID(UUID ID) {
		this.mID = ID;
	}

	public String getAlias() {
		return mUserName;
	}

    public StatusUser getStatusUser() {
        return mStatusUser;
    }

    public void setStatusUser(StatusUser statusUser) {
        this.mStatusUser = statusUser;
    }

	public void setAlias(String alias) {
		this.mUserName = alias;
	}

	public void setDrawPinXYSize(int xIcon, int yIcon, int sizeIcon) {
		mDrawPoint.x = xIcon;
		mDrawPoint.y = yIcon;
		this.iconSize = sizeIcon;
	}
	
	public Point getDrawPoint(){
		return mDrawPoint;
	}
	
	public int getIconSize(){
		return iconSize;
	}

    public Bitmap getUserIcon() {
        return mUserIcon;
    }

    public void setUserIcon(Bitmap mUserIcon) {
        this.mUserIcon = mUserIcon;
    }

    public String getUserAge() {
        return mUserAge;
    }

    public void setUserAge(String userAge) {
        this.mUserAge = userAge;
    }
}
