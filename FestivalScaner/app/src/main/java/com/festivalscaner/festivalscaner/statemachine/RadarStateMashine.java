package com.festivalscaner.festivalscaner.statemachine;

import android.util.Log;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Entity;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.FiniteStateException;
import com.continuent.tungsten.commons.patterns.fsm.Guard;
import com.continuent.tungsten.commons.patterns.fsm.NegationGuard;
import com.continuent.tungsten.commons.patterns.fsm.State;
import com.continuent.tungsten.commons.patterns.fsm.StateChangeListener;
import com.continuent.tungsten.commons.patterns.fsm.StateMachine;
import com.continuent.tungsten.commons.patterns.fsm.StateTransitionMap;
import com.continuent.tungsten.commons.patterns.fsm.StateType;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;
import com.festivalscaner.festivalscaner.radarmodel.IRadarModel;

/**
 * Created by andrey on 13.05.2015.
 */
public class RadarStateMashine {

    public static final String S_ANIMATION_LODING = "SAnimationLoding";
    public static final String S_SOBEL = "SSobel";
    public static final String S_SLIDING = "SSliding";
    public static final String S_TOP_USER_BAR_VISIBLE = "STopUserBarVisible";
    public static final String S_TOP_USER_BAR_JUST_PIC = "STopUserBarJustPic";
    public static final String S_CHOOSE_PLACE = "SChoosePlace";
    public static final String S_ACTIVE = "SActive";
    public static final String S_END = "SEnd";
    public static final String S_START = "SStart";
    public static final String S_RADAR = "SRadar";
    StateTransitionMap map;
    StateMachine mModelStateMachine;
    public State SStart;
    public State SActive;
    public State SEnd;

    public State SRadar;
    public State SAnimationLoding;
    public State SSobel;
    public State SSliding;
    public State STopUserBarVisible;
    public State STopUserBarJustPic;

    public State SChoosePlace;
    public State SShowPlace;
    public State SMeHasBeenRejected;
    public State SMeAmInvited;


    Action mEmptyAction;
    IRadarModel mRadarModel;
    public boolean isJustPic = true;


    public RadarStateMashine(IRadarModel radarModel) {
        mRadarModel = radarModel;
        mEmptyAction = new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {

            }
        };
        initFSM();
    }

    private void initFSM() {
        try {
            map = new StateTransitionMap();
            Log.d("555", "initFSM0");
            initFSMStates();
            Log.d("555", "initFSM1");
            map.build();
            Log.d("555", "initFSM2");
            mModelStateMachine = new StateMachine(map, new Entity() {
            });
            Log.d("555", "initFSM3");
            mModelStateMachine.addListener(new StateChangeListener() {

                @Override
                public void stateChanged(Entity arg0, State st1, State st2) {

                }
            });
            mModelStateMachine.setMaxTransitions(10000000);
            Log.d("555", "initFSM");
        } catch (Exception e) {
            Log.d("555", "initFSM e" + e.toString());
        }
    }

    public void applyEvent(State stateEvent) {
        try {
            Log.d("666", "applyEvent1 =" + stateEvent.toString());
            mModelStateMachine.applyEvent(new Event(stateEvent));
            Log.d("666", "applyEvent2 =" + stateEvent.toString());
        } catch (FiniteStateException e) {
            e.printStackTrace();
            Log.d("666", "applyEvent e = " + e.toString());
        }
    }

    public boolean isState(State state) {
        Log.d("777", "mModelStateMachine isState getState =" + mModelStateMachine.getState().toString());
        Log.d("777", "isState state =" + state.toString());
        return mModelStateMachine.getState().equals(state);
    }

    public boolean isStateOrSubstateOf(State state) {
        Log.d("777", "mModelStateMachine isSubstateOf getState =" + mModelStateMachine.getState().toString());
        Log.d("777", "isSubstateOf state =" + state.toString());
        return mModelStateMachine.getState().isSubstateOf(state) || mModelStateMachine.getState().equals(state);
    }

    void initFSMStates() throws FiniteStateException {
        initUpperLevelStatesAndTransitions();
        Log.d("666", "initFSMStates1");
        initSRadarStates();
        Log.d("666", "initFSMStates2");
        initSChoosePlace();
    }
    private void initUpperLevelStatesAndTransitions() throws FiniteStateException {
        SStart = new State(S_START, StateType.START);
        SActive = new State(S_ACTIVE, StateType.ACTIVE, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("666", "doAction S_ACTIVE SRadar");
                applyEvent(SRadar);
            }
        }, mEmptyAction);
        SEnd = new State(S_END, StateType.END);
        SRadar = new State(S_RADAR, StateType.ACTIVE, SActive, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("666", "doAction S_RADAR SAnimationLoding");
                applyEvent(SAnimationLoding);
            }
        }, mEmptyAction);
        Transition Transition_SEnd = new Transition(new RadarGuard(SEnd), SActive, mEmptyAction, SEnd);
        Transition Transition_SRADARSEnd = new Transition(new RadarGuard(SEnd), SRadar, mEmptyAction, SEnd);
        map.addState(SStart);
        map.addState(SActive);
        map.addState(SEnd);
        map.addState(SRadar);
        map.addTransition(Transition_SEnd);
        map.addTransition(Transition_SRADARSEnd);
    }

    private void initSRadarStates() throws FiniteStateException {
        SAnimationLoding = new State(S_ANIMATION_LODING, StateType.ACTIVE, SRadar, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
            }
        }, mEmptyAction);
        map.addState(SAnimationLoding);


        Transition Transition_SActive = new Transition(new RadarGuard(SActive), SStart, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SActive doAction1");

            }
        }, SActive);
        map.addTransition(Transition_SActive);
        Transition Transition_SRadar = new Transition(new RadarGuard(SRadar), SActive, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SRadar doAction1");
                applyEvent(SAnimationLoding);
            }
        }, SRadar);
        map.addTransition(Transition_SRadar);
        Transition Transition_SRadarSActive = new Transition(new RadarGuard(SActive), SRadar, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SRadar doAction1");
            }
        }, SActive);
        map.addTransition(Transition_SRadarSActive);
        Log.d("555", "initSRadarStates1");
        initTransitionAnimation();
        Log.d("555", "initSRadarStates2");
        initTransitionTopBar();
    }

    private void initTransitionAnimation() throws FiniteStateException {
        SSobel = new State(S_SOBEL, StateType.ACTIVE, SAnimationLoding, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
            }
        }, mEmptyAction);
        map.addState(SSobel);
        SSliding = new State(S_SLIDING, StateType.ACTIVE, SAnimationLoding, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
            }
        }, mEmptyAction);
        map.addState(SSliding);

        Transition Transition_SAnimationLoding = new Transition(new RadarGuard(SAnimationLoding), SRadar, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SAnimationLoding doAction1");
                applyEvent(SSobel);
            }
        }, SAnimationLoding);


        Transition Transition_SSobel = new Transition(new RadarGuard(SSobel), SAnimationLoding, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SSobel doAction1");
            }
        }, SSobel);


        Transition Transition_SSliding = new Transition(new RadarGuard(SSliding), SSobel, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SSliding doAction1");
            }
        }, SSliding);


        Transition Transition_SSlidingToSAnimation = new Transition(new RadarGuard(SSliding), SSliding, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "Transition_SSliding doAction1");
            }
        }, SAnimationLoding);

        Transition Transition_AnimationLodingSEnd = new Transition(new RadarGuard(SEnd), SAnimationLoding, mEmptyAction, SEnd);
        Transition Transition_SSlidingSEnd = new Transition(new RadarGuard(SEnd), SSliding, mEmptyAction, SEnd);
        Log.d("555", "initTransitionAnimation1");
        map.addTransition(Transition_SAnimationLoding);
        Log.d("555", "initTransitionAnimation2");
        map.addTransition(Transition_SSobel);
        Log.d("555", "initTransitionAnimation3");
        map.addTransition(Transition_SSliding);
        Log.d("555", "initTransitionAnimation4");
        map.addTransition(Transition_SSlidingToSAnimation);
        map.addTransition(Transition_AnimationLodingSEnd);
        map.addTransition(Transition_SSlidingSEnd);
    }

    private void initTransitionTopBar() throws FiniteStateException {
        STopUserBarVisible = new State(S_TOP_USER_BAR_VISIBLE, StateType.ACTIVE, SRadar, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                isJustPic = false;
            }
        }, mEmptyAction);
        STopUserBarJustPic = new State(S_TOP_USER_BAR_JUST_PIC, StateType.ACTIVE, SRadar, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                isJustPic = true;
            }
        }, mEmptyAction);
        map.addState(STopUserBarJustPic);
        map.addState(STopUserBarVisible);
        Transition TrE1500MillisecEnd = new Transition(new RadarGuard(STopUserBarJustPic), SAnimationLoding, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrE1500MillisecEnd doAction1");
            }
        }, STopUserBarJustPic);
        map.addTransition(TrE1500MillisecEnd);


        Transition TrEUpdateUsersShowList = new Transition(new RadarGuard(SAnimationLoding), STopUserBarJustPic, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrEUpdateUsersShowList doAction1");
            }
        }, SAnimationLoding);
        map.addTransition(TrEUpdateUsersShowList);

        Transition TrEDetailedUser = new Transition(new RadarGuard(STopUserBarVisible), STopUserBarJustPic, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrEDetailedUser doAction1");
            }
        }, STopUserBarVisible);
        map.addTransition(TrEDetailedUser);

        Transition TrECloseUserBarVisible = new Transition(new RadarGuard(STopUserBarJustPic), STopUserBarVisible, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrECloseUserBarVisible doAction1");
            }
        }, STopUserBarJustPic);
        map.addTransition(TrECloseUserBarVisible);


        Transition TrEUpdateUsersShowListFromTopUser = new Transition(new RadarGuard(SAnimationLoding), STopUserBarVisible, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrEUpdateUsersShowListFromTopUser doAction1");
            }
        }, SAnimationLoding);
        map.addTransition(TrEUpdateUsersShowListFromTopUser);
    }


    private void initSChoosePlace() throws FiniteStateException {
        SChoosePlace = new State(S_CHOOSE_PLACE, StateType.ACTIVE, SActive, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
            }
        }, mEmptyAction);
        map.addState(SChoosePlace);
        Transition TrELetMeet = new Transition(new RadarGuard(SAnimationLoding), SRadar, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrELetMeet doAction1");
            }
        }, SChoosePlace);
        map.addTransition(TrELetMeet);

        Transition TrEBackToRadar = new Transition(new RadarGuard(SAnimationLoding), SChoosePlace, new Action() {
            @Override
            public void doAction(Event event, Entity entity, Transition transition, int i) throws TransitionRollbackException, TransitionFailureException {
                Log.d("555", "TrEBackToRadar doAction1");
            }
        }, SRadar);
        map.addTransition(TrEBackToRadar);
    }

    public String getState() {
        return mModelStateMachine.getState().toString();
    }

    class RadarGuard extends NegationGuard {

        public RadarGuard(final State stateGuard) {
            super(new Guard() {
                @Override
                public boolean accept(Event event, Entity entity, State state) {
                    return !event.getData().toString().equals(stateGuard.toString());
                }
            });
        }
    }
}
