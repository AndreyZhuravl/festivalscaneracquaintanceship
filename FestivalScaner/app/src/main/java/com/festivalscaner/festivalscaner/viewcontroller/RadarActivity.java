package com.festivalscaner.festivalscaner.viewcontroller;


import android.app.Activity;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.festivalscaner.festivalscaner.CameraPreview;
import com.festivalscaner.festivalscaner.R;
import com.festivalscaner.festivalscaner.RadarSurfaceView;
import com.festivalscaner.festivalscaner.radarmodel.ILocationChangedListener;
import com.festivalscaner.festivalscaner.radarmodel.Navigator;
import com.festivalscaner.festivalscaner.radarmodel.RadarModel;
import com.festivalscaner.festivalscaner.viewcontroller.openglcam.CameraGLSurfaceView;
import com.festivalscaner.festivalscaner.viewcontroller.openglcam.RawResourceReader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class RadarActivity extends Activity
        //implements SurfaceTexture.OnFrameAvailableListener{
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final int INTERVAL_LOCATION_REQUEST_MILLIS = 10000;
    public static final int FASTEST_INTERVAL_LOCATION_REQUEST_MILLIS = 5000;
    private static String LOCATION_KEY = RadarActivity.class.getName() + "LOCATION_KEY";
    private Camera mCamera;
    private CameraPreview mPreview;

    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    LocationRequest mLocationRequest;
    private boolean mRequestingLocationUpdates = true;
    private ILocationChangedListener mLocationChangedListener;
    private RadarSurfaceView mRadarView;
    private CameraGLSurfaceView mCameraView;
    int mCurrentOrientation;
    private RadarModel mRadarModel;
    // INetworkManager mNetworkManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateValuesFromBundle(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_cam);
        mRadarModel = new RadarModel(this);
        createLocationRequest();
        buildGoogleApiClient();
        Log.d("111", "onCreate");
    }

    public void setCameraDisplayOrientation() {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(0, info);
        int rotation = getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                getmCameraView().angle = 0;
                mRadarModel.setOreintation(0);
                Log.d("111", "setOreintation0");
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                getmCameraView().angle = 90;
                mRadarModel.setOreintation(90);
                Log.d("111", "setOreintation90");
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                getmCameraView().angle = 180;
                mRadarModel.setOreintation(180);
                Log.d("111", "setOreintation180");
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                getmCameraView().angle = 270;
                mRadarModel.setOreintation(270);
                Log.d("111", "setOreintation270");
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mCurrentOrientation = (info.orientation + degrees) % 360;
            mCurrentOrientation = (360 - mCurrentOrientation) % 360;  // compensate the mirror
        } else {  // back-facing
            mCurrentOrientation = (info.orientation - degrees + 360) % 360;
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setCameraDisplayOrientation();
    }


    private void initRadarView() {
        mRadarView = new RadarSurfaceView(this, mRadarModel);
        mRadarModel.initStateMachine();
        mRadarModel.isNeededClearScreen = true;
        FrameLayout radarLayout = (FrameLayout) findViewById(R.id.radar_layout);
        radarLayout.addView(mRadarView);
    }

    protected String getFragmentShader(final int resourceId) {
        return RawResourceReader.readTextFileFromRawResource(this, resourceId);
    }

    public void initCameraViewGL() {
        Log.d("111", "initCameraViewGL");
        //mCamera = getCameraInstance();
        setmCameraView(new CameraGLSurfaceView(this, null, getFragmentShader(R.raw.edgefs), 1));
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        //mPreview = new CameraPreview(this);
        preview.addView(getmCameraView());
        setCameraDisplayOrientation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("111", "onResume");
        mRadarModel.switchResumePauseStateFSMR(true);
        try {
            initCameraViewGL();
            initRadarView();
            getmCameraView().onSetRenderAndResume();
            mRadarModel.setCameraView(getmCameraView());
            mRadarModel.updateFSM();
            try {
                //mCamera.setDisplayOrientation(mCurrentOrientation);
            } catch (Exception e) {

            }
            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            mRadarModel.setCameraLayout(this, preview);
            preview.removeAllViews();
            preview.addView(getmCameraView());
            FrameLayout radarLayout = (FrameLayout) findViewById(R.id.radar_layout);
            radarLayout.removeAllViews();
            radarLayout.addView(mRadarView);
        } catch (Exception e) {
            Log.d("111", "onResume e=" + e.toString());
        }
        if (mRadarView != null) {
            mRadarView.registerModelSensorListener();
        }
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mRadarModel.switchResumePauseStateFSMR(false);
        try {
            Log.d("111", "onPause");
            if (getmCameraView() != null) getmCameraView().onSetRenderAndPause();
        } catch (Exception e) {
            Log.d("111", "onPause e=" + e.toString());
        }
        if (mRadarView != null) {
            mRadarView.unregisterModelSensorListeners();
        }
    }

    public static Camera getCameraInstance() {
        int cameraId = 0;
        Camera c = null;
        try {
            c = Camera.open(cameraId);
            Camera.Parameters parameters = c.getParameters();
            Navigator.CAMERA_ANGLE_OF_VISION = (int) parameters.getVerticalViewAngle();
            c.setParameters(parameters);
        } catch (Exception e) {
        }
        return c; // returns null if camera is unavailable
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d("111", "onConnected");
            mRadarModel.updateLastLocation(mLastLocation);
            mRadarModel.requestNextNearby();
        } else {
            Toast.makeText(this, R.string.check_your_location_settings, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(this, R.string.google_play_services_out_of_date, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL_LOCATION_REQUEST_MILLIS);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL_LOCATION_REQUEST_MILLIS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mRadarModel != null) {
            mRadarModel.updateLastLocation(mLastLocation);
        }
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                mLastLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }
        }
    }

    public CameraGLSurfaceView getmCameraView() {
        return mCameraView;
    }

    public void setmCameraView(CameraGLSurfaceView mCameraView) {
        this.mCameraView = mCameraView;
    }
}
