package com.festivalscaner.festivalscaner.viewcontroller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.festivalscaner.festivalscaner.radarmodel.IRadarModel;
import com.festivalscaner.festivalscaner.radarmodel.RadarModel;
import com.festivalscaner.festivalscaner.radarmodel.UserData;

public class RadarView extends View implements OnTouchListener {
    IRadarModel radarModel;
    Context mContext;
    private int mWidth;
    private int mHeight;

    public RadarView(Context context, IRadarModel radarModel) {
        super(context);
        this.radarModel = radarModel;
        this.radarModel.setView(this);
        mContext = context;
        setOnTouchListener(this);
    }

    public void registerModelSensorListener() {
        radarModel.registerSensorListeners();
    }

    public void unregisterModelSensorListeners() {
        radarModel.unregisterSensorListeners();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = -2;
        int desiredHeight = -2;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        // Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            // Must be this size
            mWidth = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            // Can't be bigger than...
            mWidth = Math.min(desiredWidth, widthSize);
        } else {
            // Be whatever you want
            mWidth = desiredWidth;
        }

        // Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            // Must be this size
            mHeight = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            // Can't be bigger than...
            mHeight = Math.min(desiredHeight, heightSize);
        } else {
            // Be whatever you want
            mHeight = desiredHeight;
        }
        radarModel.setWidthHeight(mWidth, mHeight);
        // MUST CALL THIS
        setMeasuredDimension(mWidth, mHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        radarModel.onDraw(canvas);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return radarModel.onTouch(event);
    }

}
