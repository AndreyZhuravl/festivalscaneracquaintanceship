package com.festivalscaner.festivalscaner.viewcontroller.openglcam;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.SurfaceHolder;

import com.festivalscaner.festivalscaner.R;
import com.festivalscaner.festivalscaner.radarmodel.Navigator;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by andrey on 25.05.2015.
 */
public class CameraGLSurfaceView extends GLSurfaceView {

    private static String TAG = CameraGLSurfaceView.class.getSimpleName();
    private CameraSobelRenderer mRenderer;
    private Camera mCamera;
    private Context mActivityContext;
    private int mIsEffect;
    private int mWidth;
    private int mHeight;
    public int angle = 90;

    public CameraGLSurfaceView(Context context, Camera camera, String fs, int isEffect) {
        super(context);
        Log.d("111", "MyGLSurfaceView1");
        mRenderer = new CameraSobelRenderer(this, fs);
        setEGLContextClientVersion(2);
        mCamera = getCameraInstance();
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        mActivityContext = context;
        mIsEffect = 1;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        super.surfaceCreated(holder);
        Log.d(TAG, " surfaceCreated");
    }

    public void onSetRenderAndResume() {
        onResume();
        Log.d(TAG, " onSetRenderAndResume()");
    }

    public void onSetRenderAndPause() {
        Log.d(TAG, " onSetRenderAndPause()");
        if(mRenderer != null)
            mRenderer.close();
        onPause();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        super.surfaceChanged(holder, format, w, h);
        Log.d(TAG, " surfaceChanged() " + angle);
        if(angle == 0 || angle == 180){
            Navigator.CAMERA_ANGLE_OF_VISION = (int) mCamera.getParameters().getVerticalViewAngle();
        }else if(angle == 90 || angle == 270){
            Navigator.CAMERA_ANGLE_OF_VISION = (int) mCamera.getParameters().getHorizontalViewAngle();
        }

    }

    public static Camera getCameraInstance() {
        Log.d(TAG, " getCameraInstance()");
        int cameraId = 0;
        Camera c = null;
        try {
            c = Camera.open(cameraId);
            Camera.Parameters parameters = c.getParameters();
            Navigator.CAMERA_ANGLE_OF_VISION = (int) parameters.getVerticalViewAngle();
            c.setParameters(parameters);
        } catch (Exception e) {
        }
        return c; // returns null if camera is unavailable
    }

    protected String getVertexShader() {
        return RawResourceReader.readTextFileFromRawResource(mActivityContext, R.raw.passthroughvs);
    }

    protected String getFragmentShader(final int resourceId) {
        return RawResourceReader.readTextFileFromRawResource(mActivityContext, resourceId);
    }

    public void effectSetOnOff(int isOn) {
        mIsEffect = isOn;
    }


    public class CameraSobelRenderer implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener {
        private String TAG = CameraSobelRenderer.class.getSimpleName();
        private final String vss =
                "attribute vec2 vPosition;\n" +
                        "attribute vec2 vTexCoord;\n" +
                        "varying vec2 texCoord;\n" +
                        "void main() {\n" +
                        "  texCoord = vTexCoord;\n" +
                        "  gl_Position = vec4 ( vPosition.x, vPosition.y, 0.0, 1.0 );\n" +
                        "}";


        private String fss;

        private int[] hTex;
        private FloatBuffer pVertex;
        private FloatBuffer pVertex0;
        private FloatBuffer pVertex90;
        private FloatBuffer pVertex180;
        private FloatBuffer pVertex270;
        private FloatBuffer pTexCoord;
        private String gpuModel;
        private int hProgram;


        private SurfaceTexture mSTexture;

        private boolean mUpdateST = false;

        private CameraGLSurfaceView mView;

        CameraSobelRenderer(CameraGLSurfaceView view, String fss) {
            this.fss = fss;
            mView = view;

        }

        public void close() {
            mUpdateST = false;
            mSTexture.release();
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            deleteTex();
        }

        public void onSurfaceCreated(GL10 unused, javax.microedition.khronos.egl.EGLConfig config) {
            //String extensions = GLES20.glGetString(GLES20.GL_EXTENSIONS);
            //Log.i("mr", "Gl extensions: " + extensions);
            //Assert.assertTrue(extensions.contains("OES_EGL_image_external"));
            Log.d(TAG, " onSurfaceCreated");
            Log.d(TAG, "GL_RENDERER = " + GLES20.glGetString(GLES20.GL_RENDERER));
            Log.d(TAG, "GL_VENDOR = " + GLES20.glGetString(GLES20.GL_VENDOR));
            Log.d(TAG, "GL_VERSION = " + GLES20.glGetString(GLES20.GL_VERSION));
            Log.i(TAG, "GL_EXTENSIONS = " + GLES20.glGetString(GLES20.GL_EXTENSIONS));
            gpuModel = GLES20.glGetString(GLES20.GL_RENDERER);
            initArrays(gpuModel);
            initTex();
            mSTexture = new SurfaceTexture(hTex[0]);
            mSTexture.setOnFrameAvailableListener(this);
            try {
                if(mCamera != null){
                    mCamera.setPreviewTexture(mSTexture);}
                else{
                    mCamera = getCameraInstance();
                    mCamera.setPreviewTexture(mSTexture);
                }
            } catch (IOException ioe) {
            }

            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

            hProgram = loadShader(vss, fss);

        }


        public void onDrawFrame(GL10 unused) {
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

            synchronized (this) {
                if (mUpdateST) {
                    mSTexture.updateTexImage();
                    mUpdateST = false;
                }
            }

            GLES20.glUseProgram(hProgram);

            int ph = GLES20.glGetAttribLocation(hProgram, "vPosition");
            int tch = GLES20.glGetAttribLocation(hProgram, "vTexCoord");
            int th = GLES20.glGetUniformLocation(hProgram, "sTexture");
            int timeHandle = GLES20.glGetUniformLocation(hProgram, "sTime");
            int effectHandle = GLES20.glGetUniformLocation(hProgram, "sEffect");
            int mStepUniformHandle = GLES20.glGetUniformLocation(hProgram, "u_step");

            GLES20.glUniform2f(mStepUniformHandle, 1.0f / (float) mWidth, 1.0f / (float) mHeight);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, hTex[0]);
            //GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, hTex[0]);
            GLES20.glUniform1i(th, 0);
            GLES20.glUniform1i(timeHandle, (int) System.currentTimeMillis() % 10000);
            GLES20.glUniform1i(effectHandle, mIsEffect);

            GLES20.glVertexAttribPointer(ph, 2, GLES20.GL_FLOAT, false, 4 * 2, pVertex);
            GLES20.glVertexAttribPointer(tch, 2, GLES20.GL_FLOAT, false, 4 * 2, pTexCoord);
            GLES20.glEnableVertexAttribArray(ph);
            GLES20.glEnableVertexAttribArray(tch);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
            GLES20.glFlush();
        }

        public void onSurfaceChanged(GL10 unused, int width, int height) {
            mWidth = width;
            mHeight = height;
            defineOrientationVertexes();
            GLES20.glViewport(0, 0, width, height);
            Camera.Parameters param = mCamera.getParameters();
            mCamera.setParameters(param);
            mCamera.startPreview();
        }

        private void defineOrientationVertexes() {
            if (angle == 0) {
                pVertex = pVertex0;
            } else if (angle == 270) {
                pVertex = pVertex270;
            } else if (angle == 180) {
                pVertex = pVertex180;
            }else {
                pVertex = pVertex90;
            }
        }

        private void initTex() {
            hTex = new int[1];
            GLES20.glGenTextures(1, hTex, 0);
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, hTex[0]);
            GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        }

        private void initArrays(String gpuModel) {
            float[] vtmp0;
            float[] vtmp90;
            float[] vtmp180;
            float[] vtmp270;
            if (gpuModel.contains("Adreno") || gpuModel.contains("Mali")) {
                vtmp0 = new float[]{-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f};
                vtmp90 = new float[]{1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};
                vtmp180 = new float[]{1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f};
                vtmp270 = new float[]{-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f};
            } else {
                //для планшетов (по крайней мере на галакси таб3 работает)
                vtmp270 = new float[]{-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f};
                vtmp0 = new float[]{1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};
                vtmp90 = new float[]{1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f};
                vtmp180 = new float[]{-1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f};
            }

            float[] ttmp = {1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};

            pVertex0 = ByteBuffer.allocateDirect(8 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            pVertex0.put(vtmp0);
            pVertex0.position(0);

            pVertex90 = ByteBuffer.allocateDirect(8 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            pVertex90.put(vtmp90);
            pVertex90.position(0);

            pVertex180 = ByteBuffer.allocateDirect(8 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            pVertex180.put(vtmp180);
            pVertex180.position(0);

            pVertex270 = ByteBuffer.allocateDirect(8 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            pVertex270.put(vtmp270);
            pVertex270.position(0);

            pTexCoord = ByteBuffer.allocateDirect(8 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            pTexCoord.put(ttmp);
            pTexCoord.position(0);
        }

        private void deleteTex() {
            GLES20.glDeleteTextures(1, hTex, 0);
        }

        public synchronized void onFrameAvailable(SurfaceTexture st) {
            mUpdateST = true;
            mView.requestRender();
        }

        private int loadShader(String vss, String fss) {
            int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            GLES20.glShaderSource(vshader, vss);
            GLES20.glCompileShader(vshader);
            int[] compiled = new int[1];
            GLES20.glGetShaderiv(vshader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                GLES20.glDeleteShader(vshader);
                vshader = 0;
            }

            int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            GLES20.glShaderSource(fshader, fss);
            GLES20.glCompileShader(fshader);
            GLES20.glGetShaderiv(fshader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                GLES20.glDeleteShader(fshader);
                fshader = 0;
            }

            int program = GLES20.glCreateProgram();
            GLES20.glAttachShader(program, vshader);
            GLES20.glAttachShader(program, fshader);
            GLES20.glLinkProgram(program);

            return program;
        }
    }

}