#extension GL_OES_EGL_image_external : require
precision mediump float;
uniform samplerExternalOES sTexture;
varying vec2 texCoord;
uniform int sTime;
uniform int sEffect;

uniform vec2 u_step;


const int KERNEL_WIDTH = 3; // Odd
const float offset = 1.0;
const vec3 W = vec3(0.99, 0.99, 0.99);

void main(void)
{
if(sEffect == 1){
    vec3 sobelH = vec3(0.0);
	vec3 sobelV = vec3(0.0);
	vec3 grayS =vec3(0.0);
	vec3 rgbS =vec3(0.0);
	vec3 outp;
	float T = 0.5;
	float PI = 3.14159;
	float fTime = float (sTime);
	float fase = (1.0 - texCoord.s)*2.0*PI*T + 2.0*PI*fTime*0.0006;
	float wave = 1.0;
	if(cos(fase) > 0.0){
        wave = 1.0 - (cos(fase*2.0)+ 1.0)*0.5;
    }
    rgbS = texture2D(sTexture, vec2(texCoord.s,texCoord.t)).rgb;
	for (int i = 0; i < KERNEL_WIDTH; ++i)
    	{
    		for (int j = 0; j < KERNEL_WIDTH; ++j)
    		{
    			vec2 coord = vec2(texCoord.s + ((float(i) - offset) * u_step.s), texCoord.t + ((float(j) - offset) * u_step.t));
    			grayS = texture2D(sTexture, coord).rgb;
    			float luminance = dot(grayS ,W);
    			grayS.rgb = vec3(luminance);
    			sobelH += ((float(i)- 1.0)*(4.0 - abs(float(j)-1.0)))*grayS;
    		}
    	}

    	for (int i = 0; i < KERNEL_WIDTH; ++i)
        	{
        		for (int j = 0; j < KERNEL_WIDTH; ++j)
        		{
        			vec2 coord = vec2(texCoord.s + ((float(i) - offset) * u_step.s), texCoord.t + ((float(j) - offset) * u_step.t));
        			grayS = texture2D(sTexture, coord).rgb;
        			float luminance = dot(grayS ,W);
        			grayS.rgb = vec3(luminance);
        			sobelV +=((4.0 - abs(float(i)- 1.0))*(float(j)-1.0))*grayS;
        		}
        	}

    outp.r = 0.949 * sqrt(sobelH.r * sobelH.r + sobelV.r * sobelV.r)*(1.0-wave) + rgbS.r*wave;

	outp.g =0.32549 * sqrt(sobelH.g * sobelH.g + sobelV.g * sobelV.g)*(1.0-wave)+ rgbS.g*wave;

	outp.b =0.3647 * sqrt(sobelH.b * sobelH.b + sobelV.b * sobelV.b)*(1.0-wave)+ rgbS.b*wave;
	outp = 4.0 * outp;
    gl_FragColor = vec4(outp / float(KERNEL_WIDTH * KERNEL_WIDTH), 1.0);
    }else{
        gl_FragColor = texture2D(sTexture,texCoord);
    }
}
